package cn.waleychain.exchange.server.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author chenx
 * @date 2018年4月10日 下午3:29:27
 * @version 1.0
 */
@SpringBootApplication
@EnableZuulProxy
public class GatewayApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(GatewayApplication.class, args);
	}
}
