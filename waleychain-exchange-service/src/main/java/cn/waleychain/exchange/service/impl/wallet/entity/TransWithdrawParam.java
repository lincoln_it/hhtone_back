package cn.waleychain.exchange.service.impl.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransWithdrawParam implements Serializable {

	private static final long serialVersionUID = -197826706251150800L;

	/**
	 * 用户标识ID
	 */
	private long userId;
	/**
	 * 币种标识ID
	 */
	private long coinId;
	/**
	 * 用户平台钱包账户
	 */
	private long coinWalletId;
	/**
	 * 提现申请记录标识ID
	 */
	private long withdrawId;
	/**
	 * 提现申请记录关联流水ID
	 */
	private long changeId;
	/**
	 * 提币地址
	 */
	private String addr;
	/**
	 * 提现数量
	 */
	private BigDecimal amount;
	/**
	 * 手续费
	 */
	private BigDecimal fee;
	
	private int type;
	/**
	 * 关联订单
	 */
	private long orderId;

	public TransWithdrawParam() {
	}

	public long getUserId() {
		return this.userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getCoinId() {
		return this.coinId;
	}

	public void setCoinId(long coinId) {
		this.coinId = coinId;
	}

	public String getAddr() {
		return this.addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public long getCoinWalletId() {
		return coinWalletId;
	}

	public void setCoinWalletId(long coinWalletId) {
		this.coinWalletId = coinWalletId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getWithdrawId() {
		return withdrawId;
	}

	public void setWithdrawId(long withdrawId) {
		this.withdrawId = withdrawId;
	}

	public long getChangeId() {
		return changeId;
	}

	public void setChangeId(long changeId) {
		this.changeId = changeId;
	}

}
