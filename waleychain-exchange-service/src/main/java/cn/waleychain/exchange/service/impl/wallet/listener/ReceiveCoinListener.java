/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl.wallet.listener;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azazar.bitcoin.jsonrpcclient.Bitcoin;
import com.azazar.bitcoin.jsonrpcclient.Bitcoin.Transaction;
import com.azazar.bitcoin.jsonrpcclient.ConfirmedPaymentListener;

import cn.waleychain.exchange.core.constant.DDIC;
import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.utils.SequenceUtils;
import cn.waleychain.exchange.feign.CoinServiceFeign;
import cn.waleychain.exchange.feign.UserServiceFeign;
import cn.waleychain.exchange.model.CoinInfo;
import cn.waleychain.exchange.model.CoinWallet;
import cn.waleychain.exchange.model.CoinWalletChange;
import cn.waleychain.exchange.model.UserInfo;
import cn.waleychain.exchange.model.WalletRecharge;
import cn.waleychain.exchange.service.wallet.CoinWalletService;

public class ReceiveCoinListener extends ConfirmedPaymentListener {
	
	private static final Logger mLog = LoggerFactory.getLogger(ReceiveCoinListener.class);
	
    private long coinId;
    private Bitcoin client;
    private String lastblock;
    private String coinName;
    private long lastUpdateTime;
    private UserServiceFeign userFeign;
    private CoinWalletService coinWalletService;
    private CoinServiceFeign coinFeign;

    public ReceiveCoinListener(long coinId, Bitcoin client, UserServiceFeign userFeign, CoinWalletService coinWalletService, CoinServiceFeign coinFeign) {
        this(coinId, "unknow", client, userFeign, coinWalletService, coinFeign);
    }

    public ReceiveCoinListener(long coinId, String coinName, Bitcoin client, UserServiceFeign userFeign, CoinWalletService coinWalletService, CoinServiceFeign coinFeign) {
        super(1);
        this.coinId = coinId;
        this.client = client;
        this.coinName = coinName;
        this.userFeign = userFeign;
        this.coinWalletService = coinWalletService;
        this.coinFeign = coinFeign;
        LoggerHelper.printLogInfo(mLog, coinName + " receive listener");
        try {

        } catch (Exception e) {
        	LoggerHelper.printLogErrorNotThrows(mLog, e, "[" + coinName + "]spring get bean error:" + e.getMessage());
        }

    }

    public Bitcoin getClient() {
        return this.client;
    }

    public String getLastblock() {
        return this.lastblock;
    }

    public long getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public void confirmed(Transaction transaction) {
        try {
            this.lastblock = transaction.blockHash();
            long now = System.currentTimeMillis();
            if (this.lastUpdateTime == 0L || (now - this.lastUpdateTime) / 1000L >= 60L) {
            	CoinInfo coin = new CoinInfo();
            	coin.setCoinId(coinId);
            	coin.setLastblock(this.lastblock);
                this.coinFeign.modifyCoinInfo(coin);
                this.lastUpdateTime = now;
            }

            if (!transaction.account().matches("^TRADE_\\d+")) {
                return;
            }

            long userId = Long.parseLong(transaction.account().replace("TRADE_", ""));
            UserInfo user = userFeign.fetchUserInfo(userId);
            if (user == null) {
            	LoggerHelper.printLogInfo(mLog, "user not found [userId=" + userId + "]");
                return;
            }

            if (this.coinWalletService.fetchWalletRechargeByTxIdAndAddress(transaction.txId(), transaction.address()) != null) {
            	LoggerHelper.printLogInfo(mLog, "txid is processed");
                return;
            }

            CoinWallet wallet = coinWalletService.fetchCoinWalletInfo(userId, coinId);
            if (wallet == null) {
            	LoggerHelper.printLogInfo(mLog, "wallet not found [coinWalletId=" + userId + coinId + "]");
                return;
            }
            
            BigDecimal amount = new BigDecimal(transaction.amount());
            WalletRecharge inRecord = new WalletRecharge();
            inRecord.setIdNo(SequenceUtils.generateDefaultSerialNum());
            inRecord.setCoinWalletId(wallet.getCoinWalletId());
            inRecord.setAddress(transaction.address());
            inRecord.setAmount(amount);
            inRecord.setFee(new BigDecimal("0"));
            inRecord.setRealAmount(amount);
            inRecord.setTxId(transaction.txId());
            inRecord.setStatus(0);
            
            CoinWalletChange change = new CoinWalletChange();
            change.setCoinWalletId(wallet.getCoinWalletId());
            change.setBizType(DDIC.WalletBizType.WALLET_RECHARGE.id);
            change.setBizTypeId(inRecord.getIdNo());
            change.setType(DDIC.AccountChangeType.INCOME_1.id);
            change.setStatus(DDIC.WalletChangeStatus.RECHARGE_SUSS.id);
            change.setCreateTime(new Date());
            
            if (this.coinWalletService.addWalletRecharge(inRecord, change)) {
            
            	LoggerHelper.printLogInfo(mLog, "[" + this.coinName + "]receiveConfirmed");
            }
        } catch (Exception e) {
        	LoggerHelper.printLogErrorNotThrows(mLog, e, "receive error:" + e.getMessage());
        }

    }
}
