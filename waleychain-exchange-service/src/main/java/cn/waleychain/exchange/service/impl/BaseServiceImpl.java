/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;

import cn.waleychain.exchange.core.constant.DDIC;
import cn.waleychain.exchange.core.exec.CoinWalletStatusFreezeException;
import cn.waleychain.exchange.core.result.RetResultCode;
import cn.waleychain.exchange.core.vaildate.VaildateHelper;
import cn.waleychain.exchange.dao.OperateDetailMapper;
import cn.waleychain.exchange.model.CoinWallet;
import cn.waleychain.exchange.model.OperateDetail;
import cn.waleychain.exchange.model.UserAccount;
import cn.waleychain.exchange.service.RedisService;

public class BaseServiceImpl {

	@Autowired
	protected RedisService redisService;
	
	@Autowired
	protected OperateDetailMapper operDetailMapper;
	
	/**
	 * 通用缓存过期时长
	 */
	protected static long cacheExpirationTime = 5 * 60 * 1000L;
	
	/**
	 * 存入缓存，不设置过期时长
	 * @param cacheName
	 * @param key
	 * @param value
	 */
	protected void setCacheNotExpi(String cacheName, String key, Object value) {
		redisService.set(cacheName, key, value);
	}
	
	/**
	 * 存入缓存，默认过期时长
	 * @param cacheName
	 * @param key
	 * @param value
	 */
	protected void setCacheDefaultExpi(String cacheName, String key, Object value) {
		redisService.set(cacheName, key, value, cacheExpirationTime, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * 存入缓存，自定义设置过期时长
	 * @param cacheName
	 * @param key
	 * @param value
	 * @param time
	 * @param timeUnit
	 */
	protected void setCacheSetExpi(String cacheName, String key, Object value, long time, TimeUnit timeUnit) {
		redisService.set(cacheName, key, value, time, timeUnit);
	}
	
	/**
	 * 获取缓存数据
	 * @param cacheName
	 * @param key
	 */
	protected Object get(String cacheName, String key) {
		return redisService.get(cacheName, key);
	}
	
	/**
	 * 删除缓存
	 * @param cacheName
	 * @param key
	 */
	protected void remove(String cacheName, String key) {
		redisService.delete(cacheName, key);
	}
	
	/**
	 * 插入相关业务操作流水
	 * @param userId 用户标识ID
	 * @param coinId 币种id
	 * @param unionId 账号ID
	 * @param refUnionId 关联账号ID
	 * @param orderId 订单iD
	 * @param detailType 入账为1，出账为2
	 * @param paymentType 业务类型:
								充值(recharge_into) 
								提现审核通过(withdrawals_out) 
								下单(order_create) 
								成交(order_turnover)
								成交手续费(order_turnover_poundage)  
								撤单(order_cancel)  
								注册奖励(bonus_register)
								提币冻结解冻(withdrawals)
								充人民币(recharge)
								提币手续费(withdrawals_poundage)   
								兑换(cny_btcx_exchange)
								奖励充值(bonus_into)
								奖励冻结(bonus_freeze)
	 * @param paymentAmount 流水状态：
									充值
									提现
									冻结
									解冻
									转出
									转入
	 * @param paymentRemark
	 * @throws Exception
	 */
	protected void createPaymentDetail(Long userId, Long coinId, Long unionId, Long refUnionId, Long orderId, Integer detailType, 
			String paymentType, BigDecimal paymentAmount, String paymentRemark) throws Exception {
		
		OperateDetail detail = new OperateDetail();
		detail.setUserId(userId);
		detail.setCoinId(coinId);
		detail.setUnionId(unionId);
		detail.setRefUnionId(refUnionId);
		detail.setOrderId(orderId);
		detail.setDetailType(detailType);
		detail.setPaymentType(paymentType);
		detail.setPaymentAmount(paymentAmount);
		detail.setPaymentRemark(paymentRemark);
		detail.setPaymentDate(new Date());
		
		operDetailMapper.insertSelective(detail);
	}
	
	public void vaildateUserAccount(UserAccount account, boolean isCheckStatus) throws Exception {
		
		VaildateHelper.vaildateBooleanResult(account == null, RetResultCode.E20020);
		if (isCheckStatus) {
			if (DDIC.UserAccountStatus.DISABLED.id == account.getStatus()) {
				throw new CoinWalletStatusFreezeException(RetResultCode.E20013, account.getAccountId());
			}
		}
	}
	
	public void vaildateCoinWallet(CoinWallet coinWallet, boolean isCheckStatus) throws Exception {
		
		VaildateHelper.vaildateBooleanResult(coinWallet == null, RetResultCode.E40001);
		if (isCheckStatus) {
			if (DDIC.CoinWalletStatus.FREEZE.id == coinWallet.getStatus()) {
				throw new CoinWalletStatusFreezeException(RetResultCode.E40002, coinWallet.getCoinWalletId());
			}
		}
	}
}
