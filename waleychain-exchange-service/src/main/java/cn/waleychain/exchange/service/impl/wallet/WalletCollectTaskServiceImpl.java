/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl.wallet;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jsuportframework.util.JSUtils;

import cn.waleychain.exchange.core.constant.DDIC;
import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.dao.WalletCollectTaskMapper;
import cn.waleychain.exchange.feign.CoinServiceFeign;
import cn.waleychain.exchange.model.CoinInfo;
import cn.waleychain.exchange.model.WalletCollectTask;
import cn.waleychain.exchange.service.impl.BaseServiceImpl;
import cn.waleychain.exchange.service.impl.wallet.utils.DefaultCoinClient;

@Service
public class WalletCollectTaskServiceImpl extends BaseServiceImpl {

	private static final Logger mLog = LoggerFactory.getLogger(WalletCollectTaskServiceImpl.class);
	
	@Autowired
	private WalletCollectTaskMapper walletTaskMapper;
	
	@Autowired
	private CoinServiceFeign coinFeign;
	
	@Autowired
	private DefaultCoinClient defaultCoinClient;
	
	public void collectTask() {
		List<WalletCollectTask> taskList = this.walletTaskMapper.fetchTodoTask(new Date());
		if (!JSUtils.ifCollectionEmpty(taskList)) {
			Iterator<WalletCollectTask> var2 = taskList.iterator();

			while (var2.hasNext()) {
				WalletCollectTask task = var2.next();
				CoinInfo tradeCoin = null;
				try {
					tradeCoin = this.coinFeign.fetchCoinById(task.getCoinId().longValue());
				} catch (Exception e1) {
					e1.printStackTrace();
					continue;
				}

				try {
					String txid = null;
					if (DDIC.WalletType.DEFAULT.id == tradeCoin.getWalletType()) {
						LoggerHelper.printLogInfo(mLog, "default开始执行最后归账");
						txid = this.defaultCoinClient.sendToCenter(tradeCoin, task.getFromUser(), task.getToAddr(), task.getAmount());
						LoggerHelper.printLogInfo(mLog, "collectTask");
						this.walletTaskMapper.deleteByPrimaryKey(task.getIdNo());
					} else if ("eth".equals(tradeCoin.getWalletType())) {
						// this.log.error("eth开始执行最后归账");
						// txid = this.ethCoinClient.sendToCenter(tradeCoin,
						// task.getFromUser(), task.getToAddr(),
						// task.getAmount());
					} else if ("ethToken".equals(tradeCoin.getWalletType())) {
						// this.log.error("ethToken开始执行最后归账");
						// txid =
						// this.ethTokenCoinClient.sendToCenter(tradeCoin,
						// task.getFromUser(), task.getToAddr(),
						// task.getAmount());
					}

				} catch (Exception e) {
					LoggerHelper.printLogErrorNotThrows(mLog, e, e.getMessage());
				}
			}

		}
	}
}
