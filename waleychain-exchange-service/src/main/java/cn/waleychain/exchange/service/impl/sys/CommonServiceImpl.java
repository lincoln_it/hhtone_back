/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl.sys;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.waleychain.exchange.core.SysParaKey;
import cn.waleychain.exchange.core.cache.CacheConts;
import cn.waleychain.exchange.core.constant.DDIC;
import cn.waleychain.exchange.core.exec.ClientException;
import cn.waleychain.exchange.core.result.RetResultCode;
import cn.waleychain.exchange.core.utils.SmsUtils;
import cn.waleychain.exchange.dao.SmsMapper;
import cn.waleychain.exchange.model.Sms;
import cn.waleychain.exchange.service.impl.BaseServiceImpl;
import cn.waleychain.exchange.service.sys.CommonService;
import cn.waleychain.exchange.service.sys.ConfigService;

@Service
public class CommonServiceImpl extends BaseServiceImpl implements CommonService {

	@Autowired
	private SmsMapper smsMapper;
	
	@Autowired
	private ConfigService configService;
	
	@Override
	public Sms sendSms(String mobile, String content) throws Exception {
		
		String errorMsg = null;
		boolean bool = false;
		try {
			bool = SmsUtils.sendSms(mobile, content);
		} catch (Exception e) {
			errorMsg = e.getMessage();
		}
		
		Sms sms = new Sms();
		sms.setMobile(mobile);
		sms.setContent(content);
		sms.setCreated(new Date());
		if (bool) {
			sms.setStatus(DDIC.SmsStatus.STATUS_1.id);
			sms.setRemark("发送成功");
		} else {
			sms.setStatus(DDIC.SmsStatus.STATUS_0.id);
			sms.setRemark("发送失败，" + errorMsg);
		}
		
		boolean bool1 = smsMapper.insertSelective(sms) > 0;
		if (bool && bool1) {
			return sms;
		}
		
		return null;
	}
	
	@Override
	public boolean addMobileCode(Sms sms, String code) throws Exception {

		if (sms == null) {
			return false;
		}
			
		if (sms.getStatus() == DDIC.SmsStatus.STATUS_1.id) {
			// 将验证码放入缓存
			Integer time = Integer.parseInt(configService.fetchSystemConfigValue(SysParaKey.SYS_CONFIG_PHONE_VAILDATE_CODE_EXPIRE_TIME));
			this.setCacheSetExpi(CacheConts.CACHE_NAME_PHONE_VERIFY_CODE, sms.getMobile(), code, time, TimeUnit.MINUTES);
		}
		
		return true;
	}

	@Override
	public boolean checkSmsVaildateCode(String mobile, String code) throws Exception {

		if (!this.redisService.hasKey(CacheConts.CACHE_NAME_PHONE_VERIFY_CODE, mobile)) {
			throw new ClientException(RetResultCode.E12002);
		}
		
		String c = (String) this.get(CacheConts.CACHE_NAME_PHONE_VERIFY_CODE, mobile);
		if (!code.equals(c)) {
			throw new ClientException(RetResultCode.E12001);
		}
		
		return true;
	}

}
