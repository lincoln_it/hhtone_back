/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl.trade;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.jsuportframework.util.JSUtils;
import com.jsuportframework.util.date.JSDate;

import cn.waleychain.exchange.core.constant.KLineDataType;

public class TradeUtils {

	/**
	 * 校验是否在交易时间范围 Validate can trade.
	 * 
	 * @param date
	 * @param tradeWeek
	 * @param tradeTime
	 * @return
	 */
	public static boolean validateCanTrade(Date date, String tradeWeek, String tradeTime) {
		if (date == null) {
			return false;
		}

		boolean canTrade = false;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		// 如果设置的星期为空，那么直接返回true。
		if (tradeWeek != null && tradeWeek.length() > 0) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			// 获取当前是星期几
			int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
			if (day == 0) {
				day = 7;
			}

			// 分割指定的星期设置
			String weeks[] = tradeWeek.split(",");
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			int date2 = calendar.get(Calendar.DATE);
			long mills = calendar.getTimeInMillis();

			if (weeks != null) {
				for (String w : weeks) {
					int v = Integer.valueOf(w);

					if (v == day) {
						canTrade = true;
						break;
					}
				}
			}
			if (!canTrade) {
				return false;
			}
			if (tradeTime != null && tradeTime.length() > 0) {
				try {
					String[] times = tradeTime.split(",");
					if (times != null) {
						for (String s : times) {
							if (s != null && s.length() > 0) {
								String[] times2 = s.split("-");
								
								Date start = dateFormatter
										.parse(year + "-" + month + "-" + date2 + " " + times2[0] + ":00");
								Date end = dateFormatter
										.parse(year + "-" + month + "-" + date2 + " " + times2[1] + ":00");

								if (mills >= start.getTime() && mills <= end.getTime()) {
									canTrade = true;
									break;
								} else {
									canTrade = false;
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		return canTrade;
	}
	
	public static Date getStatisticsDate(String type, Date currentTime) {
		
		if (!JSUtils.ifStringEmpty(type) && currentTime != null) {
			JSDate last = new JSDate(currentTime.getTime());
			int year = last.getYear();
			int month = last.getMonth();
			int date = last.getDate();
			int hours = last.getHours();
			int minutes = last.getMinutes();
			int seconds = last.getSeconds();
			byte var10 = -1;
			switch(type.hashCode()) {
				case -1779076931:
					if (type.equals(KLineDataType.THIRTY_MINUTES_LIST.getKey())) {
						var10 = 6;
					}
					break;
				case -1320119389:
					if (type.equals(KLineDataType.ONE_YEAR_LIST)) {
						var10 = 15;
					}
					break;
				case -1012436106:
					if (type.equals(KLineDataType.ONE_DAY_LIST)) {
						var10 = 12;
					}
					break;
				case -594571335:
					if (type.equals(KLineDataType.ONE_MINUTES_LIST)) {
						var10 = 2;
					}
					break;
				case 310623577:
					if (type.equals(KLineDataType.ONE_SECONDS_LIST)) {
						var10 = 0;
					}
					break;
				case 328597929:
					if (type.equals(KLineDataType.FOUR_HOURS_LIST)) {
						var10 = 9;
					}
					break;
				case 404330989:
					if (type.equals(KLineDataType.FIVE_MINUTES_LIST)) {
						var10 = 4;
					}
					break;
				case 481866978:
					if (type.equals(KLineDataType.FIFTEEN_MINUTES_LIST)) {
						var10 = 5;
					}
					break;
				case 793298797:
					if (type.equals(KLineDataType.SIX_HOURS_LIST)) {
						var10 = 10;
					}
					break;
				case 1309525901:
					if (type.equals(KLineDataType.FIVE_SECONDS_LIST)) {
						var10 = 1;
					}
					break;
				case 1478175855:
					if (type.equals(KLineDataType.SEVEN_DAY_LIST)) {
						var10 = 13;
					}
					break;
				case 1814682646:
					if (type.equals(KLineDataType.TWELVE_HOURS_LIST)) {
						var10 = 11;
					}
					break;
				case 1818596419:
					if (type.equals(KLineDataType.TWO_HOURS_LIST)) {
						var10 = 8;
					}
					break;
				case 2010589289:
					if (type.equals(KLineDataType.ONE_HOURS_LIST)) {
						var10 = 7;
					}
					break;
				case 2015200218:
					if (type.equals(KLineDataType.ONE_MONTH_LIST)) {
						var10 = 14;
					}
					break;
				case 2087422337:
					if (type.equals(KLineDataType.THREE_MINUTES_LIST)) {
						var10 = 3;
					}
			}

			switch(var10) {
				case 0:
					return currentTime;
				case 1:
					return new JSDate((new JSDate(year, month, date, hours, minutes, seconds / 5 * 5)).getTime() + 5000L);
				case 2:
					return new JSDate((new JSDate(year, month, date, hours, minutes, 0)).getTime() + 60000L);
				case 3:
					return new JSDate((new JSDate(year, month, date, hours, minutes / 3 * 3, 0)).getTime() + 180000L);
				case 4:
					return new JSDate((new JSDate(year, month, date, hours, minutes / 5 * 5, 0)).getTime() + 300000L);
				case 5:
					return new JSDate((new JSDate(year, month, date, hours, minutes / 15 * 15, 0)).getTime() + 900000L);
				case 6:
					return new JSDate((new JSDate(year, month, date, hours, minutes / 30 * 30, 0)).getTime() + 1800000L);
				case 7:
					return new JSDate((new JSDate(year, month, date, hours, 0, 0)).getTime() + 3600000L);
				case 8:
					return new JSDate((new JSDate(year, month, date, hours / 2 * 2, 0, 0)).getTime() + 7200000L);
				case 9:
					return new JSDate((new JSDate(year, month, date, hours / 4 * 4, 0, 0)).getTime() + 14400000L);
				case 10:
					return new JSDate((new JSDate(year, month, date, hours / 6 * 6, 0, 0)).getTime() + 21600000L);
				case 11:
					return new JSDate((new JSDate(year, month, date, hours / 12 * 12, 0, 0)).getTime() + 43200000L);
				case 12:
					return new JSDate((new JSDate(year, month, date, 0, 0, 0)).getTime() + 86400000L);
				case 13:
					return new JSDate((new JSDate(year, month, date / 7 * 7, 0, 0, 0)).getTime() + 604800000L);
				case 14:
					if (month + 1 > 12) {
						++year;
						month = 1;
					} else {
						++month;
					}

					return new JSDate((new JSDate(year, month, 1, 0, 0, 0)).getTime());
				case 15:
					return new JSDate(year + 1, 1, 1, 0, 0, 0);
				default:
					return null;
			}
		} else {
			return null;
		}
	}
	
}
