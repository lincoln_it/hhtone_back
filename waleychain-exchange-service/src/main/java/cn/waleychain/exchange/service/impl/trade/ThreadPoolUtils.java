/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl.trade;

import com.jsuportframework.core.thread.JSMultitaskThreadPool;
import com.jsuportframework.core.thread.JSMultitaskThreadPoolConfiguration;
import com.jsuportframework.core.thread.executer.JSExecuteGroup;

public class ThreadPoolUtils {

	private static final JSMultitaskThreadPool pool = JSMultitaskThreadPool.getThreadPool();
	private static JSExecuteGroup dataPutGroup = null;
	private static JSExecuteGroup dataGetGroup = null;
	
	private ThreadPoolUtils() {
	}
	private static ThreadPoolUtils instance = null;
	
	private static boolean isInit = false;
	public static ThreadPoolUtils getInstance() {
		if (!isInit) {
			instance = new ThreadPoolUtils();
			instance.init();
			
			isInit = true;
		}
		
		return instance;
	}
	
	public void init() {
		JSMultitaskThreadPoolConfiguration poolConfig = new JSMultitaskThreadPoolConfiguration();
		pool.startup(poolConfig);
		dataPutGroup = pool.getExecuteGroup(4, 20000, 60000L);
		dataGetGroup = pool.getExecuteGroup(4, 20000, 60000L);
	}
	
	public JSExecuteGroup getDataPutGroup () {
		return dataPutGroup;
	}
	
	public JSExecuteGroup getDataGetGroup () {
		return dataGetGroup;
	}
}
