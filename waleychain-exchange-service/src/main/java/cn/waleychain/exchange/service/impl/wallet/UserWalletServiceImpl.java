/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl.wallet;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.core.constant.DDIC;
import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.core.result.RetResultCode;
import cn.waleychain.exchange.core.utils.PageHelper;
import cn.waleychain.exchange.core.vaildate.VaildateHelper;
import cn.waleychain.exchange.dao.UserWalletMapper;
import cn.waleychain.exchange.feign.CoinServiceFeign;
import cn.waleychain.exchange.feign.UserServiceFeign;
import cn.waleychain.exchange.model.CoinInfo;
import cn.waleychain.exchange.model.UserInfo;
import cn.waleychain.exchange.model.UserWallet;
import cn.waleychain.exchange.service.wallet.UserWalletService;

@Service
public class UserWalletServiceImpl implements UserWalletService {

	@Autowired
	private UserServiceFeign userFeign;
	
	@Autowired
	private CoinServiceFeign coinFeign;
	
	@Autowired
	private UserWalletMapper userWalletMapper;
	
	@Override
	public boolean addUserWallet(Long userId, Long coinId, String name, String address, String payPassword) throws Exception {
		
		// 获取用户信息
		UserInfo user = userFeign.fetchUserInfo(userId);
		VaildateHelper.vaildateBooleanResult(user == null, RetResultCode.E20019, userId);
		
		// 币种信息
		CoinInfo coin = coinFeign.fetchCoinById(coinId);
		VaildateHelper.vaildateBooleanResult(coin == null, RetResultCode.E30003, coinId);
		
		// 验证支付密码
		VaildateHelper.vaildateBooleanResult(!payPassword.equals(user.getPayPassword()), RetResultCode.E20017);
		
		UserWallet userWallet = new UserWallet();
		userWallet.setUserId(userId);
		userWallet.setCoinId(coinId);
		userWallet.setName(name);
		userWallet.setAddress(address);
		userWallet.setSort(0);
		userWallet.setStatus(DDIC.UserWalletStatus.USER_WALLET_STATUS_1.id);
		userWallet.setCreateTime(new Date());
		userWallet.setModifiedTime(new Date());
		
		return userWalletMapper.insertSelective(userWallet) > 0;
	}

	@Override
	public UserWallet fetchUserWalletInfo(Long userWalletId) throws Exception {

		return userWalletMapper.fetchUserWalletInfo(userWalletId);
	}

	@Override
	public PageInfo<UserWallet> fetchUserWalletPageList(String name, Long coinId, Long userId, int showCount, int currentPage) throws Exception {

		PageHelper.startPage(currentPage, showCount);
		Page<UserWallet> page = userWalletMapper.fetchUserWalletPageList(name, coinId, userId);
		
		return new PageInfo<>(page);
	}

	@Override
	public boolean updateUserWallet(Long userWalletId, Long userId, String name, String address, String payPassword) throws Exception {

		// 获取用户信息
		UserInfo user = userFeign.fetchUserInfo(userId);
		VaildateHelper.vaildateBooleanResult(user == null, RetResultCode.E20019, userId);
		
		// 验证支付密码
		VaildateHelper.vaildateBooleanResult(!payPassword.equals(user.getPayPassword()), RetResultCode.E20017);
		
		UserWallet userWallet = new UserWallet();
		userWallet.setUserWalletId(userWalletId);
		userWallet.setUserId(userId);
		userWallet.setName(name);
		userWallet.setAddress(address);
		userWallet.setModifiedTime(new Date());
		
		return userWalletMapper.updateByPrimaryKeySelective(userWallet) > 0;
	}

	@Override
	public boolean delete(Long userWalletId, Long userId, String payPassword) throws Exception {
		// 获取用户信息
		UserInfo user = userFeign.fetchUserInfo(userId);
		VaildateHelper.vaildateBooleanResult(user == null, RetResultCode.E20019, userId);
		
		// 验证支付密码
		VaildateHelper.vaildateBooleanResult(!payPassword.equals(user.getPayPassword()), RetResultCode.E20017);
		
		return userWalletMapper.deleteByPrimaryKey(userWalletId) > 0;
	}

}
