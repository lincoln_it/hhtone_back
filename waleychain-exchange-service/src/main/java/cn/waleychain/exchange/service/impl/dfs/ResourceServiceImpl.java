/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl.dfs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.core.utils.PageHelper;
import cn.waleychain.exchange.dao.FileResourceMapper;
import cn.waleychain.exchange.model.FileResource;
import cn.waleychain.exchange.service.dfs.ResourceService;

@Service
public class ResourceServiceImpl implements ResourceService {

	@Autowired
	private FileResourceMapper fileResourceMapper;
	
	@Override
	public PageInfo<FileResource> fetchResourcesList(String name, Integer type, int showCount, int currentPage) throws Exception {
		
		PageHelper.startPage(currentPage, showCount);
		Page<FileResource> page = fileResourceMapper.fetchResourcesList(name, type);
		
		return new PageInfo<>(page);
	}

	@Override
	public boolean batchInsert(List<FileResource> list) throws Exception {

		return fileResourceMapper.insertBatch(list) > 0;
	}

}
