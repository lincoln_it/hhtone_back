/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.impl.sys;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;

import cn.waleychain.exchange.core.constant.DDIC;
import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.core.utils.PageHelper;
import cn.waleychain.exchange.core.utils.SequenceUtils;
import cn.waleychain.exchange.dao.DictionaryInfoMapper;
import cn.waleychain.exchange.model.DictionaryInfo;
import cn.waleychain.exchange.service.impl.BaseServiceImpl;
import cn.waleychain.exchange.service.sys.ConfigService;

@Service
public class ConfigServiceImpl extends BaseServiceImpl implements ConfigService {

	@Autowired
	private DictionaryInfoMapper dictionaryMapper;
	
	@Override
	public List<DictionaryInfo> fetchConfigType() throws Exception {

		return dictionaryMapper.fetchConfigType();
	}
	
	@Override
	public boolean addSystemConfig(Long type, 
			String name, String code, String value) throws Exception {

		DictionaryInfo dictionary = new DictionaryInfo();
		dictionary.setCode(code);
		dictionary.setCreateTime(new Date());
		dictionary.setModifiedTime(new Date());
		dictionary.setName(name);
		dictionary.setStatus(1);
		dictionary.setValueType(DDIC.DictionaryValueType.STRING.id);
		dictionary.setIdNo(SequenceUtils.generateDefaultSerialNum());
		if (type == null || type == 0L) {
			dictionary.setTypeId(dictionary.getIdNo());
		} else {
			dictionary.setTypeId(type);
		}
		dictionary.setTypeId(type);
		dictionary.setValue(value);
		
		return dictionaryMapper.insertSelective(dictionary) > 0;
	}

	@Override
	public boolean updateSystemConfig(Long idNo, 
			String name, 
			String value, int status, String remark) throws Exception {

		DictionaryInfo dictionary = new DictionaryInfo();
		dictionary.setIdNo(idNo);
		dictionary.setModifiedTime(new Date());
		dictionary.setName(name);
		dictionary.setValue(value);
		dictionary.setStatus(status);
		dictionary.setRemark(remark);
		
		boolean bool = dictionaryMapper.updateByPrimaryKeySelective(dictionary) > 0;
		if (bool) {
			// 清除缓存
			// 获取原本配置
			dictionary = dictionaryMapper.queryByPrimaryKey(idNo);
			if (dictionary != null) {
				this.redisService.hdel(DictionaryInfo.CACHE_NAME, dictionary.getCode());
			}
		}
		
		return bool;
	}
	
	@Override
	public String fetchSystemConfigValue(String configCode) throws Exception {

		DictionaryInfo info = this.fetchSystemConfigInfo(configCode);
		
		return info == null ? null : info.getValue();
	}
	
	/**
	 * 获取系统配置信息
	 * @param configCode
	 * @return
	 * @throws Exception
	 */
	@Override
	public DictionaryInfo fetchSystemConfigInfo(String configCode) throws Exception {
		
		String res = this.redisService.hget(DictionaryInfo.CACHE_NAME, configCode);
		DictionaryInfo info = null;
		if (res == null) {
			info = dictionaryMapper.fetchDictionaryByConfigCode(configCode);
			if (info == null) {
				return null;
			}
			
			this.redisService.hset(DictionaryInfo.CACHE_NAME, configCode, info);
		} else {
			info = JSONObject.parseObject(res, DictionaryInfo.class);
		}
		
		return info;
	}
	
	@Override
	public PageInfo<DictionaryInfo> fetchConfigList(Long type, int showCount, int currentPage) throws Exception {

		PageHelper.startPage(currentPage, showCount);
		Page<DictionaryInfo> page = dictionaryMapper.fetchConfigList(type);
		
		return new PageInfo<>(page);
	}

}
