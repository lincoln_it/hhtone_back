/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.account.provider;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.UserBank;
import cn.waleychain.exchange.model.UserInfo;
import cn.waleychain.exchange.service.account.UserService;

@RestController
@RequestMapping("/user")
public class UserServiceProvider {

	@Autowired
	private UserService userService;
	
	/**
	 * 获取系统用户userId
	 * @return
	 */
	@RequestMapping(value = "/fetchAdminUserId")
	public long fetchAdminUserId() {
		
		return userService.fetchAdminUserId();
	}
	
	@RequestMapping(value = "/addUser")
	public UserInfo addUser(@RequestBody UserInfo user) throws Exception {
		
		return userService.addUserInfo(user);
	}
	
	/**
	 * 用户邮箱注册
	 * @param email
	 * @param password
	 * @param invitionCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/emailRgstry")
	public UserInfo emailRgstry(@RequestParam String email, @RequestParam String password, @RequestParam String invitionCode) throws Exception {
		
		return userService.emailRgstry(email, password, invitionCode);
	}
	
	/**
	 * 邮箱注册验证
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/activateAccount")
	public boolean activateAccount(@RequestParam String code) throws Exception {
		
		return userService.activateAccount(code);
	}
	
	@RequestMapping(value = "/fetchUserInfo")
	public UserInfo fetchUserInfo(@RequestParam Long userId) throws Exception {
	
		return userService.fetchUserInfo(userId);
	}
	
	@RequestMapping(value = "/fetchUserPageList")
	public PageInfo<UserInfo> fetchUserPageList(@RequestParam(required = false) Long userId, @RequestParam(required = false) String phoneNum, @RequestParam(required = false) String keyword, 
			@RequestParam(required = false) Integer status, @RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return userService.fetchUserPageList(userId, phoneNum, keyword, status, showCount, currentPage);
	}
	
	/**
	 * 编辑用户信息
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/modifyUser")
	public boolean modifyUserInfo(@RequestBody UserInfo user) throws Exception {
		
		return userService.modifyUserInfo(user);
	}
	
	/**
	 * 验证手机号是否已注册
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkMobileIsExist")
	public boolean checkMobileIsExist(@RequestParam String mobile) throws Exception {
	
		return userService.checkMobileIsExist(mobile);
	}
	
	/**
	 * 验证邮箱是否已注册
	 * @param email
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkEmailIsExist")
	public boolean checkEmailIsExist(@RequestParam String email) throws Exception {
	
		return userService.checkEmailIsExist(email);
	}
	
	/**
	 * 用户登录
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login")
	public UserInfo login(@RequestParam String mobile, @RequestParam String password) throws Exception {
		
		return userService.login(mobile, password);
	}
	
	/**
	 * 通过手机号设置登录密码
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findPassword")
	public boolean modifyPassByMobile(@RequestParam String mobile, @RequestParam String password) throws Exception {
		
		return userService.modifyPassByMobile(mobile, password);
	}
	
	/**
	 * 修改密码
	 * @param idNo
	 * @param oldPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/modifyPassword")
	public boolean modifyPassword(@RequestParam Long userId, @RequestParam String password, @RequestParam String oldPassword) throws Exception {
		
		return userService.modifyPassword(userId, password, oldPassword);
	}
	
	/**
	 * 用户绑定银行卡
	 * @param userBank
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/bindBankCard")
	public boolean bindBankCard(@RequestBody UserBank userBank) throws Exception {
		
		return userService.addUserBankCard(userBank);
	}
	
	/**
	 * 判断用户是否实名认证
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkUserIsRealNameAuth")
	public boolean checkUserIsRealNameAuth(@RequestParam Long userId) throws Exception {
		
		return userService.checkUserIsRealNameAuth(userId);
	}
	
	/**
	 * 用户实名认证
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userIndentifyAuth")
	public boolean userIdentifyAuth(@RequestBody UserInfo user) throws Exception {
		
		return userService.userIdentifyAuth(user);
	}
	
	/**
	 * 获取用户的银行卡列表
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchUserBankList")
	public List<UserBank> fetchUserBankList(@RequestParam Long userId) throws Exception {
		
		return userService.fetchBankListByUserId(userId);
	}
	
	/**
	 * 验证支付密码
	 * @param userId
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkUserPayPassword")
	public boolean checkUserPayPassword(@RequestParam Long userId, @RequestParam String payPassword) throws Exception {
		
		return userService.checkUserPayPassword(userId, payPassword);
	}
	
	/**
	 * 修改支付密码
	 * @param idNo
	 * @param oldPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/modifyPayPassword")
	public boolean modifyPayPassword(@RequestParam Long userId, @RequestParam Integer isSetPayPassword, @RequestParam String password, @RequestParam(required = false) String oldPassword) throws Exception {
		
		return userService.modifyPayPassword(userId, isSetPayPassword, password, oldPassword);
	}
	
	/**
	 * 通过手机号设置资金密码
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findPayPassword")
	public boolean modifyPayPassByMobile(@RequestParam String mobile, @RequestParam String password) throws Exception {
		
		return userService.modifyPayPassByMobile(mobile, password);
	}
}
