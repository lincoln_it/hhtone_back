/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.account.provider;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.AccountRecharge;
import cn.waleychain.exchange.model.AccountWithdraw;
import cn.waleychain.exchange.model.UserAccount;
import cn.waleychain.exchange.service.account.AccountService;

@RestController
@RequestMapping("/account")
public class AccountServiceProvider {

	@Autowired
	private AccountService accountService;
	
	/**
	 * 分页获取用户资金账号列表
	 * @param coinId
	 * @param userName
	 * @param mobile
	 * @param status
	 * @param begin
	 * @param end
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchAccountPageList")
	public PageInfo<UserAccount> fetchAccountPageList(@RequestParam(required = false) String userName, 
			@RequestParam(required = false) String realName, @RequestParam(required = false) String mobile, @RequestParam(required = false) Integer status, 
			@RequestParam(required = false) BigDecimal begin, @RequestParam(required = false) BigDecimal end, @RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return accountService.fetchAccountPageList(userName, realName, mobile, status, begin, end, showCount, currentPage);
	}
	
	/**
	 * 获取资金账号信息
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchAccountInfo")
	public UserAccount fetchAccountInfo(@RequestParam Long accountId) throws Exception {
		
		return accountService.fetchAccountInfo(accountId);
	}
	
	/**
	 * 通过用户ID获取资金账号信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchAccountByUserId")
	public UserAccount fetchAccountByUserId(@RequestParam Long userId) throws Exception {
		
		return accountService.fetchAccountByUserId(userId);
	}
	
	/**
	 * 分页获取资金账号提现记录
	 * @param userName
	 * @param realName
	 * @param beginDate
	 * @param endDate
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchAccountWithdrowPageList")
	public PageInfo<AccountWithdraw> fetchAccountWithdrowPageList(@RequestParam(required = false) String userName, @RequestParam(required = false) String realName, 
			@RequestParam(required = false) Date beginDate, @RequestParam(required = false) Date endDate, @RequestParam(required = false) Integer status, 
			@RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return accountService.fetchAccountWithdrowPageList(userName, realName, beginDate, endDate, status, showCount, currentPage);
	}
	
	/**
	 * 分页获取资金账号充值记录
	 * @param userName
	 * @param realName
	 * @param beginDate
	 * @param endDate
	 * @param status
	 * @param tradeNo
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchAccountRechargePageList")
	public PageInfo<AccountRecharge> fetchAccountRechargePageList(@RequestParam(required = false) String userName, @RequestParam(required = false) String realName, 
			@RequestParam(required = false) Date beginDate, @RequestParam(required = false) Date endDate, @RequestParam(required = false) Integer status, 
			@RequestParam(required = false) String tradeNo, @RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return accountService.fetchAccountRechargePageList(userName, realName, beginDate, endDate, status, tradeNo, showCount, currentPage);
	}
	
	/**
	 * 设置提现记录状态
	 * @param idNo
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setAccountWithdrawStatus")
	public boolean setAccountWithdrawStatus(@RequestParam Long idNo, @RequestParam Integer status, @RequestParam(required = false) String remark) throws Exception {
		
		return accountService.setAccountWithdrawStatus(idNo, status, remark);
	}
	
	/**
	 * 设置充值记录状态
	 * @param idNo
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setAccountRechargeStatus")
	public boolean setAccountRechargeStatus(@RequestParam Long idNo, @RequestParam Integer status, @RequestParam(required = false) String remark) throws Exception {
		
		return accountService.setAccountRechargeStatus(idNo, status, remark);
	}
	
	/**
	 * 资金账户提现
	 * @param accountWithdraw
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addAccountWithdraw")
	public boolean addAccountWithdraw(@RequestBody AccountWithdraw accountWithdraw, @RequestParam Long userId, 
			@RequestParam String payPassword) throws Exception {
	
		return accountService.addAccountWithdraw(accountWithdraw, userId, payPassword);
	}
	
	/**
	 * 资金账户充值
	 * @param accountRecharge
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addAccountRecharge")
	public AccountRecharge addAccountRecharge(@RequestParam Long userId, @RequestParam BigDecimal amount, @RequestParam Integer payType) throws Exception {
	
		return accountService.addAccountRecharge(userId, amount, payType);
	}
	
}
