/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.account.config;

import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.utils.StringUtils;

@Configuration
public class DataSourceConfig implements EnvironmentAware {
	
	private static final Logger mLog = LoggerFactory.getLogger(DataSourceConfig.class);

    private RelaxedPropertyResolver prop;
    
	@Override
	public void setEnvironment(Environment environment) {
        this.prop = new RelaxedPropertyResolver(environment,"spring.datasource.");
	}

	/**
     * 数据库连接池配置
     * @return
     */
    @Bean(initMethod="init",destroyMethod="close",name="dataSource")
    public DataSource dataSource(){
    	LoggerHelper.printLogInfo(mLog, "数据库连接池配置中......");
        if (StringUtils.isEmpty(prop.getProperty("url"))) {
            throw new ApplicationContextException("数据库连接池url配置错误.");
        }else{
            DruidDataSource druid=new DruidDataSource();
            druid.setUrl(prop.getProperty("url"));
            druid.setUsername(prop.getProperty("username"));
            druid.setPassword(prop.getProperty("password"));
            druid.setDriverClassName(prop.getProperty("driverClassName"));
            druid.setInitialSize(Integer.valueOf(prop.getProperty("druid.initialSize")));
            druid.setMinIdle(Integer.valueOf(prop.getProperty("druid.minIdle")));
            druid.setMaxActive(Integer.valueOf(prop.getProperty("druid.maxActive")));
            druid.setMaxWait(Integer.valueOf(prop.getProperty("druid.maxWait")));
            druid.setTimeBetweenConnectErrorMillis(Long.valueOf(prop.getProperty("druid.timeBetweenEvictionRunsMillis")));
            druid.setMinEvictableIdleTimeMillis(Long.valueOf(prop.getProperty("druid.minEvictableIdleTimeMillis")));
            druid.setValidationQuery(prop.getProperty("druid.validationQuery"));
            druid.setTestWhileIdle(Boolean.parseBoolean(prop.getProperty("druid.testWhileIdle")));
            druid.setTestOnBorrow(Boolean.parseBoolean(prop.getProperty("druid.testOnBorrow")));
            druid.setTestOnReturn(Boolean.parseBoolean(prop.getProperty("druid.testOnReturn")));
            druid.setConnectionProperties(prop.getProperty("druid.connectionProperties"));
            try {
                druid.setFilters(prop.getProperty("druid.filter"));
            } catch (SQLException e) {
            	LoggerHelper.printLogErrorNotThrows(mLog, e, "druid数据库连接池初始化异常");
            }
            return druid;
        }
    }
    
    /**
     * 静态资源过滤
     * @return
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean fr = new FilterRegistrationBean();
        fr.setFilter(new WebStatFilter());
        fr.addUrlPatterns("/*");
        fr.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        return fr;
    }

    /**
     * 数据源监控
     */
    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        ServletRegistrationBean registration = new ServletRegistrationBean();
        registration.setServlet(new StatViewServlet());
        registration.setName("druidMonitor");
        registration.setUrlMappings(Lists.newArrayList("/druid/*"));
        //自定义添加初始化参数
        Map<String, String> intParams = Maps.newHashMap();
        intParams.put("loginUsername","druid");
        intParams.put("loginPassword","druid");
        registration.setName("DruidWebStatFilter");
        registration.setInitParameters(intParams);
        return registration;
    }
}
