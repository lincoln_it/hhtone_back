package cn.waleychain.exchange.dfs.core;

/**
 * Fastdfs config exception.
 * 
 */
public class FastdfsConfigException extends RuntimeException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5956076746600662398L;

	public FastdfsConfigException() {
		super();
	}

	public FastdfsConfigException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public FastdfsConfigException(String message, Throwable cause) {
		super(message, cause);
	}

	public FastdfsConfigException(String message) {
		super(message);
	}

	public FastdfsConfigException(Throwable cause) {
		super(cause);
	}
}
