package cn.waleychain.exchange.dfs;

import java.io.File;
import java.io.FileNotFoundException;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.ResourceUtils;

import cn.waleychain.exchange.dfs.core.FastdfsApplication;
import cn.waleychain.exchange.dfs.core.FastdfsException;

/**
 * 文件dfs
 * @author chenx
 * @date 2018年4月11日 上午9:58:39
 * @version 1.0
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages="cn.waleychain.exchange.dao")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"cn.waleychain.exchange.feign"})
@ComponentScan(basePackages = {"cn.waleychain.exchange.service.impl", "cn.waleychain.exchange.dfs.provider"})
@EnableTransactionManagement
public class DfsApplication implements CommandLineRunner {
	
	public static void main(String[] args) {
		
		SpringApplication.run(DfsApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		try {
			File f = ResourceUtils.getFile("classpath:fdfs_client.conf");
			FastdfsApplication.application.init(f.getAbsolutePath());

		} catch (FileNotFoundException e) {
			throw new FastdfsException("Init fastdfs failed, caused by: ", e);
		}
	}
}
