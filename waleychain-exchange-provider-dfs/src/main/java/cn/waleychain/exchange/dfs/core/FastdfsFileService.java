package cn.waleychain.exchange.dfs.core;

import java.net.URL;

import cn.waleychain.exchange.dfs.bean.FastdfsRemoteItem;
import cn.waleychain.exchange.dfs.bean.FastdfsUploadResult;

/**
 * Fastdfs file upload and download server.
 */
public interface FastdfsFileService {

	/**
	 * Upload file to fastdfs storage server with specified file data and
	 * file extend name.
	 * 
	 * @param data  The file data.
	 * 
	 * @param fileExtname
	 * 
	 * @return
	 */
	public FastdfsUploadResult upload(byte[] data, String fileExtname);

	/**
	 * Upload file to fastdfs storage server with specified URL.
	 * 
	 * @param url
	 * @return
	 */
	public FastdfsUploadResult upload(URL url);

	/**
	 * Download file.
	 * 
	 * @param item
	 * @return
	 */
	//public FastdfsDownloadResult download(FastdfsRemoteItem item);

	/**
	 * Delete file from server.
	 */
	public boolean delete(FastdfsRemoteItem result);
}
