package cn.waleychain.exchange.dfs.bean;

import java.io.Serializable;

/**
 * Fastdfs remote item.
 */
public class FastdfsRemoteItem implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3165739316124697318L;

	/**
	 * Original name.
	 */
	private String originalName;

	/**
	 * Group name.
	 */
	private String groupName;

	/**
	 * Remote file name.
	 */
	private String remoteFileName;

	/**
	 * Access url.
	 */
	private String url;

	public FastdfsRemoteItem(String originalName, String groupName, String remoteFileName, String url) {
		this.originalName = originalName;
		this.groupName = groupName;
		this.remoteFileName = remoteFileName;
		this.url = url;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getRemoteFileName() {
		return remoteFileName;
	}

	public void setRemoteFileName(String remoteFileName) {
		this.remoteFileName = remoteFileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
}
