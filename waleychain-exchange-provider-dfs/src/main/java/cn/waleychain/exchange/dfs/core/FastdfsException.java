package cn.waleychain.exchange.dfs.core;

/**
 * Fastdfs exception.
 * 
 */
public class FastdfsException extends RuntimeException{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1212676311014675600L;

	public FastdfsException() {
		super();
	}

	public FastdfsException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public FastdfsException(String message, Throwable cause) {
		super(message, cause);
	}

	public FastdfsException(String message) {
		super(message);
	}

	public FastdfsException(Throwable cause) {
		super(cause);
	}
}
