package cn.waleychain.exchange.dfs.bean;

import java.io.Serializable;

/**
 * FASTDFS item.
 */
public class FastdfsUploadResult implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5994877731919508460L;

	/**
	 * Success status.
	 */
	public final static int STATUS_SUCCESS = 0;

	/**
	 * Failed status.
	 */
	public final static int STATUS_FAILED = -1;

	/**
	 * Fastdfs remote item.
	 */
	private FastdfsRemoteItem item;

	/**
	 * Status. 0 means success, -1 means failed.
	 */
	private int status;

	/**
	 * Error description.
	 */
	private String errorDesc;

	/**
	 * Constructs fastdfs result with no status.
	 * 
	 * @param groupName
	 * @param remoteFileName
	 */
	public FastdfsUploadResult(String originalName, String groupName, String remoteFileName, String url) {
		this.status = STATUS_SUCCESS;
		this.item = new FastdfsRemoteItem(originalName, groupName, remoteFileName, url);
	}

	/**
	 * Constructs failed result with specified error desc.
	 * 
	 * @param errorDesc
	 */
	public FastdfsUploadResult(String errorDesc) {
		this.status = STATUS_FAILED;
		this.errorDesc = errorDesc;
	}

	public String getGroupName() {
		return item == null ? null : item.getGroupName();
	}

	public String getRemoteFileName() {
		return item == null ? null : item.getRemoteFileName();
	}

	public String getUrl() {
		return item == null ? null : item.getUrl();
	}

	public String getOriginalName() {
		return item == null ? null : item.getOriginalName();
	}

	public int getStatus() {
		return status;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public FastdfsRemoteItem getItem() {
		return item;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("status").append(status);
		return buf.toString();
	}
}
