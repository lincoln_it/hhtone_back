package cn.waleychain.exchange.dfs.core;

import java.io.Serializable;

import cn.waleychain.exchange.dfs.client.ClientGlobal;
import cn.waleychain.exchange.dfs.client.TrackerGroup;

/**
 * Fastdfs global configuration.
 * 
 *
 */
public class FastdfsConfiguration implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8705887420430779321L;

	/**
	 * The global configuration file name.
	 */
	private String configFileName;

	public FastdfsConfiguration(String configFileName) {
		super();
		if (configFileName == null) {
			throw new NullPointerException("Config file name can not be null!");
		}
		this.configFileName = configFileName;

		try {
			ClientGlobal.init(this.configFileName);
		} catch (Exception e) {
			throw new FastdfsConfigException("Fastdfs config error, caused by:", e);
		}
	}

	/**
	 * Return current config file name.
	 * 
	 * @return
	 */
	public String getConfigFileName() {
		return configFileName;
	}

	/**
	 * Get connet timeout.
	 * 
	 * @return
	 */
	public long getConnectTimeout() {
		return ClientGlobal.getG_connect_timeout();
	}

	/**
	 * Get network timeout.
	 * 
	 * @return
	 */
	public long getNetworkTimeout() {
		return ClientGlobal.getG_network_timeout();
	}

	/**
	 * Get track group.
	 * 
	 * @return
	 */
	public TrackerGroup getTrackerGroup() {
		return ClientGlobal.getG_tracker_group();
	}

	/**
	 * Get upload directory.
	 * 
	 * @return
	 */
	public String getDFSUploadDir() {
		return ClientGlobal.getDFS_upload_dir();
	}

	/**
	 * Get upload host.
	 * 
	 * @return
	 */
	public String getDFSUploadHost() {
		return ClientGlobal.getDFS_upload_host();
	}
	
	/**
	 * Get upload size.
	 * @return
	 */
	public int getDFSUploadSizePer(){
		return ClientGlobal.getDFS_upload_size();
	}
	
	/**
	 * Get upload number.
	 * 
	 * @return
	 */
	public int getDFSUploadNumberPer(){
		return ClientGlobal.getDFS_upload_number();
	}
}
