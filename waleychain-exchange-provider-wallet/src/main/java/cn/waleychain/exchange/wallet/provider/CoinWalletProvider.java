/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.wallet.provider;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.CoinInfo;
import cn.waleychain.exchange.model.CoinWallet;
import cn.waleychain.exchange.model.WalletRecharge;
import cn.waleychain.exchange.model.WalletWithdraw;
import cn.waleychain.exchange.service.impl.wallet.WalletServiceImpl;
import cn.waleychain.exchange.service.wallet.CoinWalletService;

/**
 * 平台钱包相关服务
 * @author chenx
 * @date 2018年4月17日 下午2:39:09
 * @version 1.0 
 */
@RestController
@RequestMapping("/coinWallet")
public class CoinWalletProvider {

	@Autowired
	private CoinWalletService coinWalletService;
	
	@Autowired
	private WalletServiceImpl walletService;
	
	/**
	 * 获取用户平台钱包列表
	 * @param userId
	 * @param keyword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchCoinWalletListByUserId")
	public PageInfo<CoinWallet> fetchCoinWalletList(
			@RequestParam(required = false) Long userId, 
			@RequestParam(required = false) Long coinId,
			@RequestParam(required = false) String mobile,
			@RequestParam(required = false) String address,
			@RequestParam(required = false) Integer status,
			@RequestParam(required = false) Integer orderColumn,
			@RequestParam(required = false) Integer orderType,
			@RequestParam(required = true) Integer showCount,
			@RequestParam(required = true) Integer currentPage) throws Exception {
		
		return coinWalletService.fetchCoinWalletList(userId, coinId, mobile, address, status, orderColumn, orderType, showCount, currentPage);
	}
	
	/**
	 * 获取用户平台钱包信息
	 * @param userId
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchCoinWalletInfo")
	public CoinWallet fetchCoinWalletInfo(@RequestParam Long userId, 
			@RequestParam Long coinId) throws Exception {
		
		return coinWalletService.fetchCoinWalletInfo(userId, coinId);
	}
	
	/**
	 * 用户钱包提现
	 * @param userId
	 * @param userWalletId
	 * @param amount
	 * @param mobileCode
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userWithdraw")
	public boolean userWithdraw(@RequestParam Long userId,
			@RequestParam Long coinWalletId,
			@RequestParam Long userWalletId,
			@RequestParam BigDecimal amount,
			@RequestParam String mobileCode,
			@RequestParam String payPassword) throws Exception {
		
		return coinWalletService.userWithdrawApply(userId, coinWalletId, userWalletId, amount, mobileCode, payPassword);
	}
	
	/**
	 * 用户钱包提现审核
	 * @param idNo
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setWithdrawStatus")
	public boolean setWithdrawStatus(
			@RequestParam Long idNo, 
			@RequestParam Integer status, 
			@RequestParam String remark) throws Exception {
		
		return coinWalletService.setWelletWithdrawStatus(idNo, status, remark);
	}
	
	/**
	 * 用户撤销提现
	 * @param idNo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userCancelWithdraw")
	public boolean userCancelWithdraw(@RequestParam Long idNo) throws Exception {
		
		return coinWalletService.userCancelWithdraw(idNo);
	}
	
	/**
	 * 给用户打币成功，更新用户提币状态
	 * @param idNo
	 * @param txId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sendCoinSuccess")
	public boolean sendCoinSuccess(@RequestParam Long idNo, @RequestParam String txId) throws Exception {
		
		return coinWalletService.sendCoinSuccess(idNo, txId);
	}
	
	/**
	 * 获取用户提币记录
	 * @param userName
	 * @param status 状态（30 待审核 31 通过 32 拒绝 33 用户撤销 34 打币中）
	 * @param coinId
	 * @param mobile
	 * @param beginCoin
	 * @param endCoin
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchWalletWithdrawPageList")
	public PageInfo<WalletWithdraw> fetchWalletWithdrawPageList(
			@RequestParam(required = false) Long coinWalletId, 
			@RequestParam(required = false) String userName, 
			@RequestParam(required = false) Integer status, 
			@RequestParam(required = false) Long coinId, 
			@RequestParam(required = false) String mobile, 
			@RequestParam(required = false) Date beginDate, 
			@RequestParam(required = false) Date endDate, 
			@RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return coinWalletService.fetchWalletWithdrawPageList(coinWalletId, userName, status, coinId, mobile, beginDate, endDate, showCount, currentPage);
	}
	
	/**
	 * 获取用户充币记录
	 * @param userName
	 * @param status 状态（40 待审核 41 通过）
	 * @param coinId
	 * @param mobile
	 * @param beginCoin
	 * @param endCoin
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchWalletRechargePageList")
	public PageInfo<WalletRecharge> fetchWalletRechargePageList(
			@RequestParam(required = false) Long coinWalletId, 
			@RequestParam(required = false) String userName, 
			@RequestParam(required = false) Integer status, 
			@RequestParam(required = false) Long coinId, 
			@RequestParam(required = false) String mobile, 
			@RequestParam(required = false) Date beginDate, 
			@RequestParam(required = false) Date endDate, 
			@RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return coinWalletService.fetchWalletRechargePageList(coinWalletId, userName, status, coinId, mobile, beginDate, endDate, showCount, currentPage);
	}
	
	/**
	 * 冻结资金
	 * @param userId
	 * @param coinId
	 * @param freezeAmount
	 * @param paymentType
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/walletFreezeAmount")
	public boolean walletFreezeAmount(@RequestParam Long userId, @RequestParam Long coinId, @RequestParam BigDecimal freezeAmount, @RequestParam String paymentType, @RequestParam Long orderId) throws Exception {
		
		return coinWalletService.walletFreezeAmount(userId, coinId, freezeAmount, paymentType, orderId);
	}
	
	/**
	 * 解冻资金
	 * @param userId
	 * @param coinId
	 * @param freezeAmount
	 * @param paymentType
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/walletUnfreezeAmount")
	public boolean walletUnfreezeAmount(@RequestParam Long userId, @RequestParam Long coinId, @RequestParam BigDecimal freezeAmount, @RequestParam String paymentType, @RequestParam Long orderId) throws Exception {
		
		return coinWalletService.walletUnfreezeAmount(userId, coinId, freezeAmount, paymentType, orderId);
	}
	
	/**
	 * 资金交易
	 * @param reduceWalletId
	 * @param addWalletId
	 * @param coinId
	 * @param increment
	 * @param paymentType
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/transferAmount")
	public boolean transferAmount(@RequestParam long reduceWalletId, @RequestParam long addWalletId, @RequestParam long coinId, @RequestParam BigDecimal increment, @RequestParam String paymentType, @RequestParam long orderId) throws Exception {
		
		return coinWalletService.transferAmount(reduceWalletId, addWalletId, coinId, increment, paymentType, orderId);
	}
	
	/**
	 * 提现
	 * @param coinWalletId
	 * @param coinId
	 * @param increment
	 * @param paymentType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/withdrawalsAmount")
	public boolean withdrawalsAmount(@RequestParam long coinWalletId, @RequestParam long coinId, @RequestParam BigDecimal increment, @RequestParam String paymentType) throws Exception {
	
		return coinWalletService.withdrawalsAmount(coinWalletId, coinId, increment, paymentType);
	}
	
	/**
	 * 充值
	 * @param userId
	 * @param coinId
	 * @param increment
	 * @param paymentType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/rechargeAmount")
	public boolean rechargeAmount(@RequestParam long userId, @RequestParam long coinId, @RequestParam BigDecimal increment, @RequestParam String paymentType) throws Exception {
		
		return coinWalletService.rechargeAmount(userId, coinId, increment, paymentType);
	}
	
	/**
	 * 
	 * @param wallet
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insertUserCoinWallet")
	public boolean insertUserCoinWallet(@RequestBody CoinWallet wallet) throws Exception {
		
		return coinWalletService.insertUserCoinWallet(wallet);
	}
	
	/**
	 * 批量添加用户的币种钱包
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/batchInsertUserCoinWallet")
	public boolean batchInsertUserCoinWallet(@RequestParam Long coinId) throws Exception {
		
		return coinWalletService.batchInsertUserCoinWallet(coinId);
	}
	
	/**
	 * 刷新初始化钱包币
	 * @param coin
	 * @throws Exception
	 */
	@RequestMapping(value = "/refreshAcceptor")
	public void refreshAcceptor(@RequestBody CoinInfo coin) throws Exception {
		
		coinWalletService.refreshAcceptor(coin);
	}
	
	@RequestMapping(value = "/acceptorInit")
	public void acceptorInit() throws Exception {
		
		walletService.acceptorInit();
	}
	
	@RequestMapping(value = "/processNotDealReceiveCoin")
	public void processNotDealReceiveCoin() throws Exception {
		
		walletService.processNotDealReceiveCoin();
	}
	
	@RequestMapping(value = "/cleanRecSuccessTxId")
	public void cleanRecSuccessTxId() {
		
		walletService.cleanRecSuccessTxId();
	}
	
	/**
	 * 冻结解冻账号
	 * @param coinWalletId
	 * @param status
	 * @throws Exception 
	 */
	@RequestMapping(value = "/freezeAndUnfreezeWallet")
	public boolean freezeAndUnfreezeWallet(@RequestParam Long coinWalletId, @RequestParam Integer status) throws Exception {
		
		return coinWalletService.freezeAndUnfreezeWallet(coinWalletId, status);
	}
}
