/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.wallet.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.UserWallet;
import cn.waleychain.exchange.service.wallet.UserWalletService;

/**
 * 用户个人钱包相关服务
 * @author chenx
 * @date 2018年4月17日 下午2:39:09
 * @version 1.0 
 */
@RestController
@RequestMapping("/userWallet")
public class UserWalletProvider {

	@Autowired
	private UserWalletService userWalletService;
	
	/**
	 * 添加用户个人钱包
	 * @param userId
	 * @param coinId
	 * @param name
	 * @param address
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/new")
	public boolean addUserWallet(
			@RequestParam Long userId,
			@RequestParam Long coinId,
			@RequestParam String name,
			@RequestParam String address,
			@RequestParam String payPassword) throws Exception {
		
		return userWalletService.addUserWallet(userId, coinId, name, address, payPassword);
	}
	
	/**
	 * 获取钱包信息
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchUserWallet")
	public UserWallet fetchUserWalletById(@RequestParam Long userWalletId) throws Exception {
		
		return userWalletService.fetchUserWalletInfo(userWalletId);
	}
	
	/**
	 * 分页获取用户个人钱包列表
	 * @param name
	 * @param coinId
	 * @param userId
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchUserWalletPageList")
	public PageInfo<UserWallet> fetchUserWalletPageList(@RequestParam(required = false)String name, @RequestParam(required = false)Long coinId, 
			@RequestParam Long userId, @RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return userWalletService.fetchUserWalletPageList(name, coinId, userId, showCount, currentPage);
	}
	
	/**
	 * 修改用户个人钱包信息
	 * @param userWalletId
	 * @param userId
	 * @param name
	 * @param address
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/modifyUserWallet")
	public boolean modifyUserWallet(@RequestParam Long userWalletId, @RequestParam Long userId, @RequestParam String name, @RequestParam String address, @RequestParam String payPassword) throws Exception {
		
		return userWalletService.updateUserWallet(userWalletId, userId, name, address, payPassword);
	}
	
	/**
	 * 删除用户个人钱包
	 * @param userWalletId
	 * @param userId
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public boolean delete(@RequestParam Long userWalletId, @RequestParam Long userId, @RequestParam String payPassword) throws Exception {
		
		return userWalletService.delete(userWalletId, userId, payPassword);
	}
}
