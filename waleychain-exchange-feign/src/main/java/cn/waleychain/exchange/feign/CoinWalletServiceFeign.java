/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.constant.OperatePaymentType;
import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.CoinInfo;
import cn.waleychain.exchange.model.CoinWallet;
import cn.waleychain.exchange.model.WalletRecharge;
import cn.waleychain.exchange.model.WalletWithdraw;

@FeignClient(value = "waleychain-provider-wallet")
public interface CoinWalletServiceFeign {

	/**
	 * 获取用户平台钱包列表
	 * @param userId
	 * @param keyword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/fetchCoinWalletListByUserId")
	public PageInfo<CoinWallet> fetchCoinWalletList(
			@RequestParam(value = "userId", required = false) Long userId, 
			@RequestParam(value = "coinId", required = false) Long coinId,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "address", required = false) String address,
			@RequestParam(value = "status", required = false) Integer status,
			@RequestParam(value = "orderColumn", required = false) Integer orderColumn,
			@RequestParam(value = "orderType", required = false) Integer orderType,
			@RequestParam(value = "showCount", required = true) Integer showCount,
			@RequestParam(value = "currentPage", required = true) Integer currentPage) throws Exception;
	
	/**
	 * 获取用户平台钱包信息
	 * @param userId
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/fetchCoinWalletInfo")
	public CoinWallet fetchCoinWalletInfo(@RequestParam(value = "userId") Long userId, 
			@RequestParam(value = "coinId") Long coinId) throws Exception;
	
	/**
	 * 用户钱包提现
	 * @param userId
	 * @param userWalletId
	 * @param amount
	 * @param mobileCode
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/userWithdraw")
	public boolean userWithdraw(@RequestParam(value = "userId") Long userId,
			@RequestParam(value = "coinWalletId") Long coinWalletId,
			@RequestParam(value = "userWalletId") Long userWalletId,
			@RequestParam(value = "amount") BigDecimal amount,
			@RequestParam(value = "mobileCode") String mobileCode,
			@RequestParam(value = "payPassword") String payPassword) throws Exception;
	
	/**
	 * 设置提现记录状态
	 * @param idNo
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/setWithdrawStatus")
	public boolean setAccountWithdrawStatus(@RequestParam(value = "idNo") Long idNo, @RequestParam(value = "status") Integer status, @RequestParam(value = "remark")String remark) throws Exception;
	
	/**
	 * 用户撤销提现
	 * @param idNo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/userCancelWithdraw")
	public boolean userCancelWithdraw(@RequestParam(value = "idNo") Long idNo) throws Exception;
	
	/**
	 * 给用户打币成功，更新用户提币状态
	 * @param idNo
	 * @param txId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/sendCoinSuccess")
	public boolean sendCoinSuccess(@RequestParam(value = "idNo") Long idNo, @RequestParam(value = "txId") String txId) throws Exception;
	
	/**
	 * 获取用户提币记录
	 * @param userName
	 * @param status 状态（30 待审核 31 通过 32 拒绝 33 用户撤销 34 打币中）
	 * @param coinId
	 * @param mobile
	 * @param beginCoin
	 * @param endCoin
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/fetchWalletWithdrawPageList")
	public PageInfo<WalletWithdraw> fetchWalletWithdrawPageList(
			@RequestParam(value = "coinWalletId", required = false) Long coinWalletId, 
			@RequestParam(value = "userName", required = false) String userName, 
			@RequestParam(value = "status", required = false) Integer status, 
			@RequestParam(value = "coinId", required = false) Long coinId, 
			@RequestParam(value = "mobile", required = false) String mobile, 
			@RequestParam(value = "beginDate", required = false) Date beginDate, 
			@RequestParam(value = "endCoin", required = false) Date endDate, 
			@RequestParam(value = "showCount") int showCount, @RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 获取用户充币记录
	 * @param userName
	 * @param status 状态（40 待审核 41 通过）
	 * @param coinId
	 * @param mobile
	 * @param beginCoin
	 * @param endCoin
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/fetchWalletRechargePageList")
	public PageInfo<WalletRecharge> fetchWalletRechargePageList(
			@RequestParam(value = "coinWalletId", required = false) Long coinWalletId, 
			@RequestParam(value = "userName", required = false) String userName, 
			@RequestParam(value = "status", required = false) Integer status, 
			@RequestParam(value = "coinId", required = false) Long coinId, 
			@RequestParam(value = "mobile", required = false) String mobile, 
			@RequestParam(value = "beginDate", required = false) Date beginDate, 
			@RequestParam(value = "endDate", required = false) Date endDate, 
			@RequestParam(value = "showCount") int showCount, @RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 冻结资金
	 * @param userId
	 * @param coinId
	 * @param freezeAmount
	 * @param paymentType
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/walletFreezeAmount")
	public boolean walletFreezeAmount(
			@RequestParam(value = "userId") Long userId, 
			@RequestParam(value = "coinId") Long coinId, 
			@RequestParam(value = "freezeAmount") BigDecimal freezeAmount, 
			@RequestBody OperatePaymentType paymentType, 
			@RequestParam(value = "orderId") Long orderId) throws Exception;
	
	/**
	 * 解冻资金
	 * @param userId
	 * @param coinId
	 * @param freezeAmount
	 * @param paymentType
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/walletUnfreezeAmount")
	public boolean walletUnfreezeAmount(
			@RequestParam(value = "userId") Long userId, 
			@RequestParam(value = "coinId") Long coinId, 
			@RequestParam(value = "freezeAmount") BigDecimal freezeAmount, 
			@RequestBody OperatePaymentType paymentType, 
			@RequestParam(value = "orderId") Long orderId) throws Exception;
	
	/**
	 * 资金交易
	 * @param reduceWalletId
	 * @param addWalletId
	 * @param coinId
	 * @param increment
	 * @param paymentType
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/transferAmount")
	public boolean transferAmount(
			@RequestParam(value = "reduceWalletId") long reduceWalletId, 
			@RequestParam(value = "addWalletId") long addWalletId, 
			@RequestParam(value = "coinId") long coinId, 
			@RequestParam(value = "increment") BigDecimal increment, 
			@RequestBody OperatePaymentType paymentType, 
			@RequestParam(value = "orderId") long orderId) throws Exception;
	
	/**
	 * 提现
	 * @param coinWalletId
	 * @param coinId
	 * @param increment
	 * @param paymentType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/withdrawalsAmount")
	public boolean withdrawalsAmount(
			@RequestParam(value = "coinWalletId") long coinWalletId, 
			@RequestParam(value = "coinId") long coinId, 
			@RequestParam(value = "increment") BigDecimal increment, 
			@RequestBody OperatePaymentType paymentType) throws Exception;
	
	/**
	 * 充值
	 * @param userId
	 * @param coinId
	 * @param increment
	 * @param paymentType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/rechargeAmount")
	public boolean rechargeAmount(
			@RequestParam(value = "userId") long userId, 
			@RequestParam(value = "coinId") long coinId, 
			@RequestParam(value = "increment") BigDecimal increment, 
			@RequestParam(value = "paymentType") String paymentType) throws Exception;
	
	/**
	 * 
	 * @param wallet
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/insertUserCoinWallet")
	public boolean insertUserCoinWallet(@RequestBody CoinWallet wallet) throws Exception;
	
	/**
	 * 批量添加用户的币种钱包
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/batchInsertUserCoinWallet")
	public boolean batchInsertUserCoinWallet(@RequestParam(value = "coinId") Long coinId) throws Exception;
	
	/**
	 * 刷新初始化钱包币
	 * @param coin
	 * @throws Exception
	 */
	@RequestMapping(value = "/coinWallet/refreshAcceptor")
	public void refreshAcceptor(@RequestBody CoinInfo coin) throws Exception;
	
	@RequestMapping(value = "/coinWallet/acceptorInit")
	public void acceptorInit() throws Exception;
	
	@RequestMapping(value = "/coinWallet/processNotDealReceiveCoin")
	public void processNotDealReceiveCoin() throws Exception;
	
	@RequestMapping(value = "/coinWallet/cleanRecSuccessTxId")
	public void cleanRecSuccessTxId();
	
	/**
	 * 冻结解冻账号
	 * @param coinWalletId
	 * @param status
	 * @throws Exception 
	 */
	@RequestMapping(value = "/coinWallet/freezeAndUnfreezeWallet")
	public boolean freezeAndUnfreezeWallet(@RequestParam(value = "coinWalletId") Long coinWalletId, @RequestParam(value = "status") Integer status) throws Exception;
	
}
