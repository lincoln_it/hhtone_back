/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.CoinInfo;

@FeignClient(value = "waleychain-provider-market")
public interface CoinServiceFeign {

	/**
	 * 获取Btcx币种标识ID
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coin/fetchBtcxCoinId")
	public Long fetchBtcxCoinId() throws Exception;
	
	
	@RequestMapping(value = "/coin/fetchCoinPageList")
	public PageInfo<CoinInfo> fetchCoinPageList(
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "title", required = false) String title, 
			@RequestParam(value = "coinType", required = false) Integer coinType, 
			@RequestParam(value = "status", required = false) Integer status, 
			@RequestParam(value = "showCount") int showCount, @RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 新增币种
	 * @param coin
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coin/new")
	public boolean addCoin(@RequestBody CoinInfo coin) throws Exception;
	
	/**
	 * 获取币种信息
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coin/fetchCoinById")
	public CoinInfo fetchCoinById(@RequestParam(value = "coinId") Long coinId) throws Exception;
	
	/**
	 * 设置币种状态
	 * @param coinId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coin/setCoinStatus")
	public boolean setCoinStatus(@RequestParam(value = "coinId") Long coinId, @RequestParam(value = "status") Integer status) throws Exception;
	
	/**
	 * 编辑币种信息
	 * @param coin
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coin/modifyCoinInfo")
	public boolean modifyCoinInfo(@RequestBody CoinInfo coin) throws Exception;
	
	/**
	 * 从缓存中获取所有币种
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coin/fetchCacheCoinAllList")
	public List<CoinInfo> fetchCacheCoinAllList() throws Exception;
	
	/**
	 * 从缓存中获取所有币种
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/coin/fetchCoinList")
	public List<CoinInfo> fetchCoinList(@RequestParam(value = "coinType") Integer coinType, @RequestParam(value = "walletType") Integer walletType, @RequestParam(value = "status") Integer status) throws Exception;
}
