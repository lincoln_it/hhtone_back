/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.DealOrder;
import cn.waleychain.exchange.model.EntrustOrder;

@FeignClient(value = "waleychain-provider-market")
public interface TradeServiceFeign {

	/**
	 * 分页获取完成订单列表
	 * @param marketId
	 * @param userId
	 * @param type
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/trade/fetchDealOrderPageList")
	public PageInfo<DealOrder> fetchDealOrderPageList(
			@RequestParam(value = "marketId", required = false) Long marketId, 
			@RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "dealId", required = false) Long dealId, 
			@RequestParam(value = "status", required = false) Integer status, 
			@RequestParam(value = "type", required = false) Integer type, 
			@RequestParam(value = "showCount") Integer showCount, 
			@RequestParam(value = "currentPage") Integer currentPage) throws Exception;
	
	/**
	 * 分页获取委托订单列表
	 * @param marketId
	 * @param userId
	 * @param type
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/trade/fetchEntrustOrderPageList")
	public PageInfo<EntrustOrder> fetchEntrustOrderPageList(
			@RequestParam(value = "marketId", required = false) Long marketId,
			@RequestParam(value = "userId", required = false) Long userId, 
			@RequestParam(value = "type", required = false) Integer type, 
			@RequestParam(value = "status", required = false) Integer status, 
			@RequestParam(value = "showCount") Integer showCount, 
			@RequestParam(value = "currentPage") Integer currentPage) throws Exception;
	
	/**
	 * 创建委托订单
	 * @param userId
	 * @param marketId
	 * @param price
	 * @param num
	 * @param type
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/trade/createEntrustOrder")
	public boolean createEntrustOrder(
			@RequestParam(value = "userId") Long userId,
			@RequestParam(value = "marketId") Long marketId,
			@RequestParam(value = "price") BigDecimal price, 
			@RequestParam(value = "num") BigDecimal num,
			@RequestParam(value = "type") Integer type, 
			@RequestParam(value = "payPassword") String payPassword) throws Exception;
	
	/**
	 * 取消委托
	 * @param userId
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/trade/cancelEntrustOrder")
	public boolean cancelEntrustOrder(
			@RequestParam(value = "userId") Long userId,
			@RequestParam(value = "orderId") Long orderId) throws Exception;
	
	/**
	 * 获取最近的委托买单
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/trade/fetchRecentlyEntrustBuyList")
	public List<EntrustOrder> fetchRecentlyEntrustBuyList(
			@RequestParam(value = "marketId") Long marketId,
			@RequestParam(value = "limit") int limit) throws Exception;
	
	/**
	 * 获取最近的委托卖单
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/trade/fetchRecentlyEntrustSellList")
	public List<EntrustOrder> fetchRecentlyEntrustSellList(
			@RequestParam(value = "marketId") Long marketId,
			@RequestParam(value = "limit") int limit) throws Exception;
	
	/**
	 * 获取最近成交的记录
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/trade/fetchRecentlyDealList")
	public List<DealOrder> fetchRecentlyDealList(
			@RequestParam(value = "marketId") Long marketId,
			@RequestParam(value = "limit") int limit) throws Exception;
	
}
