/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.AccountRecharge;
import cn.waleychain.exchange.model.AccountWithdraw;
import cn.waleychain.exchange.model.UserAccount;

@FeignClient(value = "waleychain-provider-account")
public interface AccountServiceFeign {

	/**
	 * 分页获取用户资金账号列表
	 * @param coinId
	 * @param userName
	 * @param mobile
	 * @param status
	 * @param begin
	 * @param end
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/fetchAccountPageList")
	public PageInfo<UserAccount> fetchAccountPageList(
			@RequestParam(value = "coinId", required = false) Long coinId, 
			@RequestParam(value = "userName", required = false) String userName, 
			@RequestParam(value = "realName", required = false) String realName, 
			@RequestParam(value = "mobile", required = false) String mobile, 
			@RequestParam(value = "status", required = false) Integer status, 
			@RequestParam(value = "begin", required = false) BigDecimal begin, 
			@RequestParam(value = "end", required = false) BigDecimal end, 
			@RequestParam(value = "showCount") int showCount, 
			@RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 获取资金账号信息
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/fetchAccountInfo")
	public UserAccount fetchAccountInfo(@RequestParam(value = "accountId") Long accountId) throws Exception;
	
	/**
	 * 通过用户ID获取资金账号信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/fetchAccountByUserId")
	public UserAccount fetchAccountByUserId(@RequestParam(value = "userId") Long userId) throws Exception;
	
	/**
	 * 分页获取资金账号提现记录
	 * @param userName
	 * @param realName
	 * @param beginDate
	 * @param endDate
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/fetchAccountWithdrowPageList")
	public PageInfo<AccountWithdraw> fetchAccountWithdrowPageList(
			@RequestParam(value = "userName", required = false) String userName, 
			@RequestParam(value = "realName", required = false) String realName, 
			@RequestParam(value = "beginDate", required = false) Date beginDate, 
			@RequestParam(value = "endDate", required = false) Date endDate, 
			@RequestParam(value = "status", required = false) Integer status, 
			@RequestParam(value = "showCount") int showCount, 
			@RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 分页获取资金账号充值记录
	 * @param userName
	 * @param realName
	 * @param beginDate
	 * @param endDate
	 * @param status
	 * @param tradeNo
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/fetchAccountRechargePageList")
	public PageInfo<AccountRecharge> fetchAccountRechargePageList(
			@RequestParam(value = "userName", required = false) String userName, 
			@RequestParam(value = "realName", required = false) String realName, 
			@RequestParam(value = "beginDate", required = false) Date beginDate, 
			@RequestParam(value = "endDate", required = false) Date endDate, 
			@RequestParam(value = "status", required = false) Integer status, 
			@RequestParam(value = "tradeNo", required = false) String tradeNo, 
			@RequestParam(value = "showCount") int showCount, 
			@RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 设置提现记录状态
	 * @param idNo
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/setAccountWithdrawStatus")
	public boolean setAccountWithdrawStatus(@RequestParam(value = "idNo") Long idNo, @RequestParam(value = "status") Integer status, @RequestParam(value = "remark")String remark) throws Exception;
	
	/**
	 * 设置充值记录状态
	 * @param idNo
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/setAccountRechargeStatus")
	public boolean setAccountRechargeStatus(@RequestParam(value = "idNo") Long idNo, @RequestParam(value = "status") Integer status, @RequestParam(value = "remark")String remark) throws Exception;
	
	/**
	 * 用户资金账户提现
	 * @param accountWithdraw
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/addAccountWithdraw")
	public boolean addAccountWithdraw(@RequestBody AccountWithdraw accountWithdraw, @RequestParam(value = "userId")Long userId, 
			@RequestParam(value = "payPassword")String payPassword) throws Exception;
	
	/**
	 * 资金账户充值
	 * @param accountRecharge
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/addAccountRecharge")
	public AccountRecharge addAccountRecharge(@RequestParam(value = "userId")Long userId, @RequestParam("amount")BigDecimal amount, @RequestParam("payType")Integer payType) throws Exception;
	
}
