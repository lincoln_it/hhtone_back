/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.Market;

@FeignClient(value = "waleychain-provider-market")
public interface MarketServiceFeign {

	/**
	 * 分页获取市场列表
	 * @param name
	 * @param title
	 * @param status
	 * @param transAreaId
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/market/fetchMarketPageList")
	public PageInfo<Market> fetchMarketPageList(
			@RequestParam(value = "name", required = false)String name, 
			@RequestParam(value = "title", required = false)String title, 
			@RequestParam(value = "status", required = false)Integer status, 
			@RequestParam(value = "transAreaId", required = false)String transAreaId, 
			@RequestParam(value = "showCount")int showCount, 
			@RequestParam(value = "currentPage")int currentPage) throws Exception;
	
	/**
	 * 新增市场
	 * @param market
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/market/new")
	public boolean addMarket(@RequestBody Market market) throws Exception;
	
	/**
	 * 获取市场信息
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/market/fetchMarketInfoById")
	public Market fetchMarketInfoById(@RequestParam(value = "marketId") Long marketId) throws Exception;
	
	/**
	 * 设置市场状态
	 * @param marketId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/market/setMarketStatus")
	public boolean setMarketStatus(@RequestParam(value = "marketId")Long marketId, @RequestParam(value = "status")Integer status) throws Exception;
	
	/**
	 * 编辑市场信息
	 * @param market
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/market/modifyMarketInfo")
	public boolean updateMarketInfo(@RequestBody Market market) throws Exception;
	
	/**
	 * 通过币种获取可用的交易市场列表
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/market/fetchMarketListByCoinId")
	public List<Market> fetchMarketListByCoinId(@RequestParam("coinId") Long coinId) throws Exception;
	
	/**
	 * 从缓存中获取验证后的交易市场列表（可用）
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/market/fetchCacheMarketVaildList")
	public List<Market> fetchCacheMarketVaildList() throws Exception;
	
	/**
	 * 从缓存中获取市场信息
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/market/fetchMarketByCache")
	public Market fetchMarketByCache(@RequestParam("marketId") long marketId) throws Exception;
}
