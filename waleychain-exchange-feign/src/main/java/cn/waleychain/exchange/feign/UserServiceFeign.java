/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import java.util.Date;
import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.UserBank;
import cn.waleychain.exchange.model.UserInfo;

@FeignClient(value = "waleychain-provider-account")
public interface UserServiceFeign {

	/**
	 * 获取系统用户userId
	 * @return
	 */
	@RequestMapping(value = "/user/fetchAdminUserId")
	public long fetchAdminUserId();
	
	/**
	 * 验证手机号是否已注册
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/checkMobileIsExist")
	public boolean checkMobileIsExist(@RequestParam(value = "mobile") String mobile) throws Exception;
	
	/**
	 * 验证邮箱是否已注册
	 * @param email
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/checkEmailIsExist")
	public boolean checkEmailIsExist(@RequestParam(value = "email") String email) throws Exception;
	
	/**
	 * 新增用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/user/addUser")
	public UserInfo addUserInfo(@RequestBody UserInfo user) throws Exception;
	
	/**
	 * 用户邮箱注册
	 * @param email
	 * @param password
	 * @param invitionCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/emailRgstry")
	public UserInfo emailRgstry(@RequestParam(value = "email") String email, @RequestParam(value = "password") String password, @RequestParam(value = "invitionCode") String invitionCode) throws Exception;
	
	/**
	 * 邮箱注册验证
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/activateAccount")
	public boolean activateAccount(@RequestParam(value = "code") String code) throws Exception;
	
	/**
	 * 分页搜索获取用户列表
	 * @param userId
	 * @param phoneNum
	 * @param keyword
	 * @param beginDate
	 * @param endDate
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/user/fetchUserPageList")
	public PageInfo<UserInfo> fetchUserPageList(
			@RequestParam(value = "userId", required = false) String userId, 
			@RequestParam(value = "phoneNum", required = false) String phoneNum, 
			@RequestParam(value = "keyword", required = false) String keyword, 
			@RequestParam(value = "beginDate", required = false) Date beginDate, 
			@RequestParam(value = "endDate", required = false) Date endDate, 
			@RequestParam(value = "showCount") int showCount, 
			@RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 获取用户信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/user/fetchUserInfo")
	public UserInfo fetchUserInfo(@RequestParam(value = "userId") Long userId) throws Exception;
	
	/**
	 * 编辑用户信息
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/user/modifyUser")
	public boolean modifyUserInfo(@RequestBody UserInfo user) throws Exception;
	
	/**
	 * 用户登录
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/login")
	public UserInfo login(@RequestParam(value = "mobile") String mobile, @RequestParam(value = "password") String password) throws Exception;
	
	/**
	 * 通过手机号设置登录密码
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/findPassword")
	public boolean modifyPassByMobile(@RequestParam(value = "mobile") String mobile, @RequestParam(value = "password") String password) throws Exception;
	
	/**
	 * 修改密码
	 * @param idNo
	 * @param oldPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/modifyPassword")
	public boolean modifyPassword(@RequestParam(value = "userId") Long userId, @RequestParam(value = "password") String password, @RequestParam(value = "oldPassword") String oldPassword) throws Exception;
	
	/**
	 * 用户绑定银行卡
	 * @param userBank
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/bindBankCard")
	public boolean bindBankCard(@RequestBody UserBank userBank) throws Exception;
	
	/**
	 * 判断用户是否实名认证
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/checkUserIsRealNameAuth")
	public boolean checkUserIsRealNameAuth(@RequestParam(value = "userId") Long userId) throws Exception;
	
	/**
	 * 用户实名认证
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/userIndentifyAuth")
	public boolean userIdentifyAuth(@RequestBody UserInfo user) throws Exception;
	
	/**
	 * 获取用户的银行卡列表
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/fetchUserBankList")
	public List<UserBank> fetchUserBankList(@RequestParam(value = "userId") Long userId) throws Exception;

	/**
	 * 验证支付密码
	 * @param userId
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/checkUserPayPassword")
	public boolean checkUserPayPassword(@RequestParam(value = "userId") Long userId, @RequestParam(value = "payPassword") String payPassword) throws Exception;
	
	/**
	 * 修改支付密码
	 * @param idNo
	 * @param oldPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/modifyPayPassword")
	public boolean modifyPayPassword(@RequestParam(value = "userId") Long userId, @RequestParam(value = "isSetPayPassword") Integer isSetPayPassword, @RequestParam(value = "password") String password, @RequestParam(value = "oldPassword", required = false) String oldPassword) throws Exception;
	
	/**
	 * 通过手机号设置资金密码
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/findPayPassword")
	public boolean modifyPayPassByMobile(@RequestParam(value = "mobile") String mobile, @RequestParam(value = "password") String password) throws Exception;
	
}
