/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.KLineUser;
import cn.waleychain.exchange.core.entity.MarketResponseDTO;
import cn.waleychain.exchange.core.entity.TradeNewest;

@FeignClient(value = "waleychain-provider-market")
public interface TradeDataServiceFeign {

	/**
	 * 获取k线数据
	 * @param marketId 币种ID
	 * @param dataType 图表格式
	 * @param kLimit   图表数据量
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tradeData/fetchKLineData")
	public Map<String, Object> fetchKLineData(@RequestParam("marketId") long marketId, @RequestParam("dataType") String dataType, @RequestParam("kLimit") int kLimit) throws Exception;
	
	/**
	 * 将访问k线用户数据存入到缓存
	 * @param sessionId
	 * @param marketId
	 * @param dataType
	 * @param kLimit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tradeData/resetKLineUser")
	public KLineUser resetKLineUser(@RequestParam("sessionId") String sessionId, @RequestParam("marketId") Long marketId, @RequestParam("dataType") String dataType, @RequestParam("kLimit") int kLimit) throws Exception;
	
	/**
	 * 根据市场Id 和 日期类型获取图表数据
	 */
	@RequestMapping({"/tradeData/fetchDataDesc"})
	public String fetchDataDesc(@RequestParam("marketId") long marketId, @RequestParam("dataType") String dataType) throws Exception;
	
	/**
	 * 获取最新委托
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tradeData/fetchNewest")
	public TradeNewest fetchNewest(@RequestParam("marketId") Long marketId) throws Exception;
	
	/**
	 * 获取首页交易市场数据
	 * @param areaId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tradeData/fetchTradeMarketByAreaId")
	public List<MarketResponseDTO> fetchTradeMarketByAreaId(@RequestParam("areaId") String areaId) throws Exception;
}
