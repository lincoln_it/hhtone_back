/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.FileResource;

@FeignClient(value = "waleychain-provider-dfs")
public interface DfsServiceFeign {

	/**
	 * 获取文件资源列表
	 * @param name
	 * @param type
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dfs/fetchResourcesList")
	public PageInfo<FileResource> fetchResourcesList(@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "type", required = false) Integer type, 
			@RequestParam(value = "showCount") int showCount, @RequestParam(value = "currentPage") int currentPage) throws Exception;
}
