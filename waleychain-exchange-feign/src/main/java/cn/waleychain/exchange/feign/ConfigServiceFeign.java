/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.DictionaryInfo;

@FeignClient(value = "waleychain-provider-sys")
public interface ConfigServiceFeign {

	/**
	 * 获取业务配置类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cfg/fetchConfigType")
	public List<DictionaryInfo> fetchConfigType() throws Exception;
	
	/**
	 * 获取业务配置列表
	 * @param type 配置类型（1 系统 2 业务 3 交易区域）
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cfg/fetchConfigList")
	public PageInfo<DictionaryInfo> fetchConfigList( 
			@RequestParam(value = "type", required = true) Long type, 
			@RequestParam(value = "showCount") int showCount, @RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 获取配置内容
	 * @param configCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cfg/fetchConfigInfo")
	public DictionaryInfo fetchConfigInfo( 
			@RequestParam(value = "configCode") String configCode) throws Exception;
	
	/**
	 * 新增配置
	 * @param type
	 * @param name
	 * @param code
	 * @param value
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cfg/addConfig")
	public boolean addConfig(@RequestParam(value = "type") Long type, 
			@RequestParam(value = "name") String name, @RequestParam(value = "code") String code, @RequestParam(value = "value") String value) throws Exception;
	
	/**
	 * 变更业务配置
	 * @param idNo
	 * @param name
	 * @param value
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cfg/modify")
	public boolean modify( 
			@RequestParam(value = "idNo") Long idNo, 
			@RequestParam(value = "name") String name, 
			@RequestParam(value = "value") String value, @RequestParam(value = "status") int status, @RequestParam(value = "remark") String remark) throws Exception;
	
	/**
	 * 获取配置值
	 * @param configCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cfg/fetchSystemConfigValue")
	public String fetchSystemConfigValue(@RequestParam(value = "configCode")String configCode) throws Exception;
}
