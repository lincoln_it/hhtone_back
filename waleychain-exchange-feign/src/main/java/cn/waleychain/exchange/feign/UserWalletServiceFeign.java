/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.UserWallet;

@FeignClient(value = "waleychain-provider-wallet")
public interface UserWalletServiceFeign {

	@RequestMapping(value = "/userWallet/new")
	public boolean addUserWallet(
			@RequestParam(value = "userId") Long userId,
			@RequestParam(value = "coinId") Long coinId,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "address") String address,
			@RequestParam(value = "payPassword") String payPassword) throws Exception;
	
	/**
	 * 获取钱包信息
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userWallet/fetchUserWallet")
	public UserWallet fetchUserWalletById(@RequestParam(value = "userWalletId") Long userWalletId) throws Exception;
	
	/**
	 * 分页获取用户个人钱包列表
	 * @param name
	 * @param coinId
	 * @param userId
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userWallet/fetchUserWalletPageList")
	public PageInfo<UserWallet> fetchUserWalletPageList(
			@RequestParam(value = "name", required = false)String name, 
			@RequestParam(value = "coinId", required = false)Long coinId, 
			@RequestParam(value = "userId") Long userId, 
			@RequestParam(value = "showCount") int showCount, 
			@RequestParam(value = "currentPage") int currentPage) throws Exception;
	
	/**
	 * 修改用户个人钱包信息
	 * @param userWalletId
	 * @param userId
	 * @param name
	 * @param address
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userWallet/modifyUserWallet")
	public boolean modifyUserWallet(
			@RequestParam(value = "userWalletId") Long userWalletId, 
			@RequestParam(value = "userId") Long userId, 
			@RequestParam(value = "name") String name, 
			@RequestParam(value = "address") String address, 
			@RequestParam(value = "payPassword") String payPassword) throws Exception;
	
	/**
	 * 删除用户个人钱包
	 * @param userWalletId
	 * @param userId
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userWallet/delete")
	public boolean delete(@RequestParam(value = "userWalletId") Long userWalletId, @RequestParam(value = "userId") Long userId, @RequestParam(value = "payPassword") String payPassword) throws Exception;
}
