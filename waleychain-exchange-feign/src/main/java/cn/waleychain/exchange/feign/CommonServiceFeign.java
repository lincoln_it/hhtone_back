/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.model.Sms;

@FeignClient(value = "waleychain-provider-sys")
public interface CommonServiceFeign {

	/**
	 * 发送短信
	 * @param mobile
	 * @param content
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sendSms")
	public Sms sendSms(@RequestParam(value = "mobile") String mobile, @RequestParam(value = "content") String content) throws Exception;
	
	/**
	 * 发送短信验证码
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/common/sendSmsCode")
	public boolean sendSmsCode(@RequestParam(value = "mobile") String mobile) throws Exception;
	
	/**
	 * 验证短信验证码
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/common/vaildateSmsCode")
	public boolean vaildateSmsCode(@RequestParam(value = "mobile") String mobile, @RequestParam(value = "code") String code) throws Exception;
}
