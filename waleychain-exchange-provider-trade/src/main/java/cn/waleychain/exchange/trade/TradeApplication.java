package cn.waleychain.exchange.trade;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 交易
 * 
 * @author chenx
 * @date 2018年4月11日 上午10:00:08
 * @version 1.0
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages="cn.waleychain.exchange.dao")
@ComponentScan(basePackages = {"cn.waleychain.exchange.service.impl", "cn.waleychain.exchange.trade.provider"})
@EnableTransactionManagement
public class TradeApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(TradeApplication.class, args);
	}
}
