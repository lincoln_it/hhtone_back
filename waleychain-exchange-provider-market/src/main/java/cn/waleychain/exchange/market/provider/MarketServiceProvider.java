/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.market.provider;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.Market;
import cn.waleychain.exchange.service.market.MarketService;

@RestController
@RequestMapping("/market")
public class MarketServiceProvider {

	@Autowired
	private MarketService marketService;
	
	/**
	 * 分页获取市场列表
	 * @param name
	 * @param title
	 * @param status
	 * @param transAreaId
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchMarketPageList")
	public PageInfo<Market> fetchMarketPageList(@RequestParam(required = false) String name, @RequestParam(required = false)String title, @RequestParam(required = false)Integer status, 
			@RequestParam(required = false)String transAreaId, @RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return marketService.fetchMarketPageList(name, title, status, transAreaId, showCount, currentPage);
	}
	
	/**
	 * 新增市场
	 * @param market
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/new")
	public boolean addMarket(@RequestBody Market market) throws Exception {
		
		return marketService.addMarket(market);
	}
	
	/**
	 * 获取市场信息
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchMarketInfoById")
	public Market fetchMarketInfoById(@RequestParam Long marketId) throws Exception {
		
		return marketService.fetchMarketInfoById(marketId);
	}
	
	/**
	 * 设置市场状态
	 * @param marketId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setMarketStatus")
	public boolean setMarketStatus(@RequestParam Long marketId, @RequestParam Integer status) throws Exception {
		
		return marketService.setMarketStatus(marketId, status);
	}
	
	/**
	 * 编辑市场信息
	 * @param market
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/modifyMarketInfo")
	public boolean updateMarketInfo(@RequestBody Market market) throws Exception {
		
		return marketService.updateMarketInfo(market);
	}
	
	/**
	 * 通过币种获取可用的交易市场列表
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchMarketListByCoinId")
	public List<Market> fetchMarketListByCoinId(@RequestParam Long coinId) throws Exception {
		
		return marketService.fetchMarketListByCoinId(coinId);
	}
	
	/**
	 * 从缓存中获取验证后的交易市场列表（可用）
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchCacheMarketVaildList")
	public List<Market> fetchCacheMarketVaildList() throws Exception {
		
		return marketService.fetchCacheMarketVaildList();
	}
	
	/**
	 * 从缓存中获取市场信息
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchMarketByCache")
	public Market fetchMarketByCache(long marketId) throws Exception {
		
		return marketService.fetchMarketByCache(marketId);
	}
}
