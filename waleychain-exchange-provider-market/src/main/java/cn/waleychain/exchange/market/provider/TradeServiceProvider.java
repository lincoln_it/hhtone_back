/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.market.provider;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.DealOrder;
import cn.waleychain.exchange.model.EntrustOrder;
import cn.waleychain.exchange.service.trade.TradeService;

/**
 * 交易相关服务
 * @author chenx
 * @date 2018年4月17日 下午2:39:09
 * @version 1.0 
 */
@RestController
@RequestMapping("/trade")
public class TradeServiceProvider {

	@Autowired
	private TradeService tradeService;
	
	/**
	 * 分页获取完成订单列表
	 * @param marketId
	 * @param userId
	 * @param type
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchDealOrderPageList")
	public PageInfo<DealOrder> fetchDealOrderPageList(
			@RequestParam(required = false) Long marketId, 
			@RequestParam(required = false) String userName,
			@RequestParam(required = false) Long dealId, 
			@RequestParam(required = false) Integer status, 
			@RequestParam(required = false) Integer type, 
			@RequestParam Integer showCount, 
			@RequestParam Integer currentPage) throws Exception {
		
		return tradeService.fetchDealOrderPageList(marketId, userName, dealId, type, status, showCount, currentPage);
	}
	
	/**
	 * 分页获取委托订单列表
	 * @param marketId
	 * @param userId
	 * @param type
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchEntrustOrderPageList")
	public PageInfo<EntrustOrder> fetchEntrustOrderPageList(
			@RequestParam(required = false) Long marketId,
			@RequestParam(required = false) Long userId, 
			@RequestParam(required = false) Integer type, 
			@RequestParam(required = false) Integer status, 
			@RequestParam Integer showCount, 
			@RequestParam Integer currentPage) throws Exception {
		
		return tradeService.fetchEntrustOrderPageList(marketId, userId, type, status, showCount, currentPage);
	}
	
	/**
	 * 创建委托订单
	 * @param userId
	 * @param marketId
	 * @param price
	 * @param num
	 * @param type
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/createEntrustOrder")
	public boolean createEntrustOrder(
			@RequestParam Long userId,
			@RequestParam Long marketId,
			@RequestParam BigDecimal price, 
			@RequestParam BigDecimal num,
			@RequestParam Integer type, 
			@RequestParam String payPassword) throws Exception {
		
		return tradeService.createEntrustOrder(userId, marketId, price, num, type, payPassword);
	}
	
	/**
	 * 取消委托
	 * @param userId
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cancelEntrustOrder")
	public boolean cancelEntrustOrder(
			@RequestParam Long userId,
			@RequestParam Long orderId) throws Exception {
		
		return tradeService.cancelEntrustOrder(userId, orderId);
	}
	
	/**
	 * 获取最近的委托买单
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchRecentlyEntrustBuyList")
	public List<EntrustOrder> fetchRecentlyEntrustBuyList(
			@RequestParam Long marketId,
			@RequestParam int limit) throws Exception {
		
		return tradeService.fetchRecentlyBuyEntrustList(marketId, limit);
	}
	
	/**
	 * 获取最近的委托卖单
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchRecentlyEntrustSellList")
	public List<EntrustOrder> fetchRecentlyEntrustSellList(
			@RequestParam Long marketId,
			@RequestParam int limit) throws Exception {
		
		return tradeService.fetchRecentlySellEntrustList(marketId, limit);
	}
	
	/**
	 * 获取最近成交的记录
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchRecentlyDealList")
	public List<DealOrder> fetchRecentlyDealList(
			@RequestParam Long marketId,
			@RequestParam int limit) throws Exception {
		
		return tradeService.fetchRecentlyDealList(marketId, limit);
	}
	
}
