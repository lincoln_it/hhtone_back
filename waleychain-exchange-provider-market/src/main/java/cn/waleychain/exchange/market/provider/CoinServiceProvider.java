/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.market.provider;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.CoinInfo;
import cn.waleychain.exchange.service.market.CoinService;

@RestController
@RequestMapping("/coin")
public class CoinServiceProvider {

	@Autowired
	private CoinService coinService;
	
	/**
	 * 获取Btcx币种标识ID
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchBtcxCoinId")
	public Long fetchBtcxCoinId() throws Exception {
		
		return coinService.fetchBtcxCoinId();
	}
	
	@RequestMapping(value = "/fetchCoinPageList")
	public PageInfo<CoinInfo> fetchCoinPageList(@RequestParam(required = false) String name, 
			@RequestParam(required = false) String title, @RequestParam(required = false) Integer coinType, @RequestParam(required = false) Integer status, 
			@RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return coinService.fetchCoinPageList(name, title, coinType, status, showCount, currentPage);
	}
	
	/**
	 * 新增币种
	 * @param coin
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/new")
	public boolean addCoin(@RequestBody CoinInfo coin) throws Exception {
		
		return coinService.addCoinInfo(coin);
	}
	
	/**
	 * 获取币种信息
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchCoinById")
	public CoinInfo fetchCoinById(@RequestParam Long coinId) throws Exception {
		
		return coinService.fetchCoinById(coinId);
	}
	
	/**
	 * 设置币种状态
	 * @param coinId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setCoinStatus")
	public boolean setCoinStatus(@RequestParam Long coinId, @RequestParam Integer status) throws Exception {
		
		return coinService.setCoinStatus(coinId, status);
	}
	
	/**
	 * 编辑币种信息
	 * @param coin
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/modifyCoinInfo")
	public boolean modifyCoinInfo(@RequestBody CoinInfo coin) throws Exception {
		
		return coinService.updateCoinInfo(coin);
	}
	
	/**
	 * 从缓存中获取所有币种
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchCacheCoinAllList")
	public List<CoinInfo> fetchCacheCoinAllList() throws Exception {
		
		return coinService.fetchCacheCoinAllList();
	}
	
	/**
	 * 从缓存中获取所有币种
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchCoinList")
	public List<CoinInfo> fetchCoinList(@RequestParam Integer coinType, @RequestParam Integer walletType, @RequestParam Integer status) throws Exception {
		
		return coinService.fetchCoinList(coinType, walletType, status);
	}
}
