/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.market.provider;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.constant.KLineDataType;
import cn.waleychain.exchange.core.entity.KLineUser;
import cn.waleychain.exchange.core.entity.MarketResponseDTO;
import cn.waleychain.exchange.core.entity.TradeNewest;
import cn.waleychain.exchange.service.trade.KLineService;
import cn.waleychain.exchange.service.trade.TradeDataService;

@RestController
@RequestMapping("/tradeData")
public class TradeDataServiceProvider {

	@Autowired
	private KLineService kLineService;
	
	@Autowired
	private TradeDataService tradeDataService;
	
	/**
	 * 获取k线数据
	 * @param marketId 币种ID
	 * @param dataType 图表格式
	 * @param kLimit   图表数据量
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchKLineData")
	public Map<String, Object> fetchKLineData(@RequestParam long marketId, @RequestParam String dataType, @RequestParam int kLimit) throws Exception {
		
		return kLineService.fetchKLineData(marketId, dataType, kLimit);
	}
	
	/**
	 * 根据市场Id 和 日期类型获取图表数据
	 */
	@RequestMapping({"/fetchDataDesc"})
	public String fetchDataDesc(@RequestParam long marketId, @RequestParam String dataType) throws Exception {
		
		KLineDataType klineType = KLineDataType.convertByKey(dataType);
		
		String rs = tradeDataService.getDataDesc(marketId, klineType);
		
		return rs;
	}
	
	/**
	 * 获取最新委托
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchNewest")
	public TradeNewest fetchNewest(@RequestParam Long marketId) throws Exception {
		
		String tradeDesc = tradeDataService.getDataDesc(marketId, KLineDataType.ONE_DAY_LIST);
		
		return tradeDataService.getNewest(tradeDesc);
	}
	
	/**
	 * 将访问k线用户数据存入到缓存
	 * @param sessionId
	 * @param marketId
	 * @param dataType
	 * @param kLimit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/resetKLineUser")
	public KLineUser resetKLineUser(@RequestParam String sessionId, @RequestParam Long marketId, @RequestParam String dataType, @RequestParam int kLimit) throws Exception {
		
		KLineDataType klineType = KLineDataType.convertByKey(dataType);
		
		KLineUser kuser = tradeDataService.resetKLineUser(sessionId, marketId, klineType, kLimit);
		
		return kuser;
	}
	
	/**
	 * 获取首页交易市场数据
	 * @param areaId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchTradeMarketByAreaId")
	public List<MarketResponseDTO> fetchTradeMarketByAreaId(@RequestParam String areaId) throws Exception {
		
		return tradeDataService.fetchTradeMarketByAreaId(areaId);
	}
}
