package cn.waleychain.exchange.authority;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * 权限
 * 
 * @author chenx
 * @date 2018年4月11日 上午9:53:19
 * @version 1.0
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages="cn.waleychain.exchange.dao")
@ComponentScan
public class AuthorityApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(AuthorityApplication.class, args);
	}
}
