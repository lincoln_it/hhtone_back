package cn.waleychain.exchange.dao;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseUserAccountMapper;
import cn.waleychain.exchange.model.UserAccount;

/**
 * 用户资金账户
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:41
 */
public interface UserAccountMapper extends BaseUserAccountMapper {

	/**
	 * 分页获取用户资金账号列表
	 * @param userName
	 * @param mobile
	 * @param status
	 * @param begin
	 * @param end
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public Page<UserAccount> fetchAccountPageList(@Param("userName")String userName, @Param("realName")String realName, @Param("mobile")String mobile, @Param("status")Integer status, 
			@Param("begin")BigDecimal begin, @Param("end")BigDecimal end) throws Exception;
	
	/**
	 * 通过用户ID获取用户资金账号信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public UserAccount fetchAccountByUserId(@Param("userId")Long userId) throws Exception;
	
	/**
	 * 通过标识ID获取资金账号信息
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	public UserAccount fetchAccountById(@Param("accountId")Long accountId) throws Exception;
}
