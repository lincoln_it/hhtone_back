package cn.waleychain.exchange.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseDictionaryInfoMapper;
import cn.waleychain.exchange.model.DictionaryInfo;

/**
 * 系统码表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:40
 */
public interface DictionaryInfoMapper extends BaseDictionaryInfoMapper {
	
	/**
	 * 通过系统配置码获取配置信息
	 * @param configCode
	 * @return
	 * @throws Exception
	 */
	public DictionaryInfo fetchDictionaryByConfigCode(@Param("configCode") String configCode) throws Exception;

	/**
	 * 获取业务配置列表
	 * @param type 类型（1 系统 2 业务 3 交易区域）
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public Page<DictionaryInfo> fetchConfigList(@Param("typeId")Long typeId) throws Exception;
	
	/**
	 * 获取配置类型
	 * @return
	 * @throws Exception
	 */
	public List<DictionaryInfo> fetchConfigType() throws Exception;
}
