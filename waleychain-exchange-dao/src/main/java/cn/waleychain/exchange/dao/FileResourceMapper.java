package cn.waleychain.exchange.dao;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseFileResourceMapper;
import cn.waleychain.exchange.model.FileResource;

/**
 * 文件资源
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:40
 */
public interface FileResourceMapper extends BaseFileResourceMapper {
	
	/**
	 * 获取文件资源列表
	 * @param name 名称
	 * @param type 类型（1 图片 2 文件）
	 * @return
	 * @throws Exception
	 */
	public Page<FileResource> fetchResourcesList(@Param("name")String name, @Param("type")Integer type) throws Exception;
	
}
