package cn.waleychain.exchange.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseEntrustOrderMapper;
import cn.waleychain.exchange.model.EntrustOrder;

/**
 * 委托订单
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:40
 */
public interface EntrustOrderMapper extends BaseEntrustOrderMapper {

	/**
	 * 分页获取委托订单列表
	 * @param marketId
	 * @param userId
	 * @param type
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public Page<EntrustOrder> fetchEntrustOrderPageList(@Param("marketId")Long marketId, @Param("userId")Long userId, @Param("type")Integer type, @Param("status")Integer status) throws Exception;
	
	/**
	 * 查询匹配的委托单
	 * @param marketId 交易市场
	 * @param type 交易类型 1：买入；2：卖出
	 * @param price 委托价格
	 * @return
	 */
	EntrustOrder fetchMatchOrder(@Param("marketId") long marketId, @Param("type") int type, @Param("price") BigDecimal price) throws Exception;
	
	/**
	 * 交易撮合完成后，更新委托单状态
	 * @param orderId 委托单ID
	 * @param status 状态
	 * @param deal 成交数量
	 * @param freeze 冻结资金或数量
	 * @param currentTime 当前时间
	 */
	int updateEntrustOrderDealAndStatus(@Param("orderId") long orderId, @Param("status") int status, @Param("deal") BigDecimal deal, @Param("freeze") BigDecimal freeze, @Param("currentTime") Date currentTime);
	
	/**
	 * 交易撮合完成后，更新委托单
	 * @param orderId 委托单ID
	 * @param deal 成交数量
	 * @param freeze 冻结资金或数量
	 * @param currentTime 当前时间
	 * @return
	 */
	int updateEntrustOrderDeal(@Param("orderId") long orderId, @Param("deal") BigDecimal deal, @Param("freeze") BigDecimal freeze, @Param("currentTime") Date currentTime);
	
	/**
	 * 修改委托单状态
	 * @param orderId
	 * @param status
	 * @param currentTime
	 */
	void updateEntrustOrderStatus(@Param("orderId") long orderId, @Param("status") int status, @Param("currentTime") Date currentTime);
	
	/**
	 * 通过id获取委托单信息
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	public EntrustOrder fetchEntrustOrderById(@Param("orderId")long orderId) throws Exception;
	
	List<EntrustOrder> fetchNewEntrustOrderList(@Param("marketId") long marketId,
			   @Param("type") int type,
			   @Param("orderby") String orderby,
			   @Param("limit") int limit);
}
