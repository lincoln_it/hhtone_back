package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.AccountChange;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 资金账户流水记录（?该表只保留核心字段，用视图实现统计流水功能）
通过biz_type和biz_type_i
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:16
 */
public interface BaseAccountChangeMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(AccountChange t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<AccountChange> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(AccountChange t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	AccountChange queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<AccountChange> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
