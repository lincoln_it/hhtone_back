package cn.waleychain.exchange.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseCoinWalletMapper;
import cn.waleychain.exchange.model.CoinWallet;

/**
 * 用户平台钱包
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:39
 */
public interface CoinWalletMapper extends BaseCoinWalletMapper {
	
	/**
	 * 获取币种对应平台用户钱包列表
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	public List<CoinWallet> fetchCoinWalletListByCoinId(@Param("coinId")Long coinId) throws Exception;
	
	/**
	 * 批量添加币种对应的平台用户钱包
	 * @param coinWallet
	 * @return
	 * @throws Exception
	 */
	public int batchInsertCoinWallet(CoinWallet coinWallet) throws Exception;
	
	/**
	 * 获取用户的平台钱包账号列表
	 * @param userId
	 * @param keyword
	 * @return
	 * @throws Exception
	 */
	public Page<CoinWallet> fetchCoinWalletListByUserId(@Param("userId")Long userId,
			@Param("coinId")Long coinId,
			@Param("mobile")String mobile,
			@Param("address")String address,
			@Param("status")Integer status,
			@Param("orderColumn")Integer orderColumn,
			@Param("orderType")Integer orderType) throws Exception;
	
	/**
	 * 通过用户id和币种id获取用户钱包
	 * @param userId
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	public CoinWallet fetchCoinWalletByCoinIdAndUserId(@Param("userId")Long userId, @Param("coinId")Long coinId) throws Exception;
	
	/**
	 * 通过币种和地址获取钱包信息
	 * @param coinId
	 * @param addr
	 * @return
	 * @throws Exception
	 */
	public CoinWallet fetchWalletByCoinIdAndAddr(@Param("coinId")Long coinId, @Param("addr")String addr) throws Exception;
}
