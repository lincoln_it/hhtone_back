package cn.waleychain.exchange.dao;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseAccountWithdrawMapper;
import cn.waleychain.exchange.model.AccountWithdraw;

/**
 * 人民币提现
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:38
 */
public interface AccountWithdrawMapper extends BaseAccountWithdrawMapper {
	
	/**
	 * 分页获取资金账号提现记录
	 * @param userName
	 * @param realName
	 * @param beginDate
	 * @param endDate
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public Page<AccountWithdraw> fetchAccountWithdrawPageList(@Param("userName")String userName, @Param("realName")String realName, 
			@Param("beginDate")Date beginDate, @Param("endDate")Date endDate, @Param("status")Integer status) throws Exception;
	
	/**
	 * 通过充值标识获取提现信息
	 * @param idNo
	 * @return
	 * @throws Exception
	 */
	public AccountWithdraw fetchAccountWithdrawById(@Param("idNo")Long idNo) throws Exception;
	
	/**
	 * 获取用户单日提现金额，包含未审核的
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	public BigDecimal fetchSimpleDaySumWithdrawAmount(@Param("accountId")Long accountId) throws Exception;
}
