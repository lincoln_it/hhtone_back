package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.UserWallet;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户提现钱包
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:21
 */
public interface BaseUserWalletMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(UserWallet t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<UserWallet> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(UserWallet t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "userWalletId") Long userWalletId);
	
	/**
	 * 通过主键获取数据
	 */
	UserWallet queryByPrimaryKey(@Param(value = "userWalletId") Long userWalletId);
	
	/**
	 * 查询所有数据
	 */
	List<UserWallet> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
