package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.ActivationCode;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户邮箱注册激活码
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-05-02 12:06:55
 */
public interface BaseActivationCodeMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(ActivationCode t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<ActivationCode> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(ActivationCode t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "userId") Long userId);
	
	/**
	 * 通过主键获取数据
	 */
	ActivationCode queryByPrimaryKey(@Param(value = "userId") Long userId);
	
	/**
	 * 查询所有数据
	 */
	List<ActivationCode> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
