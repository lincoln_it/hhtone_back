package cn.waleychain.exchange.dao;

import org.apache.ibatis.annotations.Param;

import cn.waleychain.exchange.dao.base.BaseActivationCodeMapper;
import cn.waleychain.exchange.model.ActivationCode;

/**
 * 用户邮箱注册激活码
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-05-02 12:06:55
 */
public interface ActivationCodeMapper extends BaseActivationCodeMapper {
	
	/**
	 * 通过激活码获取信息
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public ActivationCode fetchInfoByCode(@Param("code")String code) throws Exception;
}
