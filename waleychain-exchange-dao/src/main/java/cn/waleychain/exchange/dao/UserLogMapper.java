package cn.waleychain.exchange.dao;

import cn.waleychain.exchange.dao.base.BaseUserLogMapper;

/**
 * 用户操作日志
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:42
 */
public interface UserLogMapper extends BaseUserLogMapper {
	
}
