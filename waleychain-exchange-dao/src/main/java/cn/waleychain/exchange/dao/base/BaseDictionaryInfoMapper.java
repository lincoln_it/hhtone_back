package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.DictionaryInfo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 系统码表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-05-03 09:52:51
 */
public interface BaseDictionaryInfoMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(DictionaryInfo t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<DictionaryInfo> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(DictionaryInfo t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	DictionaryInfo queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<DictionaryInfo> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
