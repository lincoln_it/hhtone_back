package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.TradeData;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 市场行情
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-23 16:18:55
 */
public interface BaseTradeDataMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(TradeData t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<TradeData> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(TradeData t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	TradeData queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<TradeData> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
