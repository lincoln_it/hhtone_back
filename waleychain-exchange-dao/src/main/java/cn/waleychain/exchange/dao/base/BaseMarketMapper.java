package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.Market;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:19
 */
public interface BaseMarketMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(Market t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<Market> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(Market t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "marketId") Long marketId);
	
	/**
	 * 通过主键获取数据
	 */
	Market queryByPrimaryKey(@Param(value = "marketId") Long marketId);
	
	/**
	 * 查询所有数据
	 */
	List<Market> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
