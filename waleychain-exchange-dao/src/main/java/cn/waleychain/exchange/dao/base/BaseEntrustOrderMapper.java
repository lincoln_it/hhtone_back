package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.EntrustOrder;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 委托订单
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:18
 */
public interface BaseEntrustOrderMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(EntrustOrder t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<EntrustOrder> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(EntrustOrder t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "orderId") Long orderId);
	
	/**
	 * 通过主键获取数据
	 */
	EntrustOrder queryByPrimaryKey(@Param(value = "orderId") Long orderId);
	
	/**
	 * 查询所有数据
	 */
	List<EntrustOrder> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
