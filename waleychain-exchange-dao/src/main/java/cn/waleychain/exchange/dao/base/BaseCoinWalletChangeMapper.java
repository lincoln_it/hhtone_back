package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.CoinWalletChange;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 钱包数据流水记录（?该表只保留核心字段，用视图实现统计流水功能）
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:17
 */
public interface BaseCoinWalletChangeMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(CoinWalletChange t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<CoinWalletChange> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(CoinWalletChange t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	CoinWalletChange queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<CoinWalletChange> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
