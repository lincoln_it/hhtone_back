package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.CoinWallet;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户平台钱包
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-28 16:53:47
 */
public interface BaseCoinWalletMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(CoinWallet t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<CoinWallet> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(CoinWallet t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "coinWalletId") Long coinWalletId);
	
	/**
	 * 通过主键获取数据
	 */
	CoinWallet queryByPrimaryKey(@Param(value = "coinWalletId") Long coinWalletId);
	
	/**
	 * 查询所有数据
	 */
	List<CoinWallet> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
