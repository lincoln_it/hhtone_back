package cn.waleychain.exchange.dao;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseUserWalletMapper;
import cn.waleychain.exchange.model.UserWallet;

/**
 * 用户提现钱包
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:42
 */
public interface UserWalletMapper extends BaseUserWalletMapper {
	
	/**
	 * 获取钱包信息
	 * @param userWalletId
	 * @return
	 * @throws Exception
	 */
	public UserWallet fetchUserWalletInfo(@Param("userWalletId")Long userWalletId) throws Exception;
	
	/**
	 * 分页获取用户个人钱包列表
	 * @param name
	 * @param coinId
	 * @param userId
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public Page<UserWallet> fetchUserWalletPageList(@Param("name")String name, @Param("coinId")Long coinId, @Param("userId")Long userId) throws Exception;
	
}
