package cn.waleychain.exchange.dao;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseUserInfoMapper;
import cn.waleychain.exchange.model.UserInfo;

/**
 * 用户信息表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:42
 */
public interface UserInfoMapper extends BaseUserInfoMapper {
	
	/**
	 * 分页搜索获取用户列表
	 * @param userId
	 * @param phoneNum
	 * @param keyword
	 * @param beginDate
	 * @param endDate
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public Page<UserInfo> fetchUserPageList(@Param("userId")Long userId, @Param("phoneNum")String phoneNum, @Param("keyword")String keyword, @Param("status")Integer status) throws Exception;
	
	/**
	 * 通过标识id获取信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public UserInfo fetchUserByUserId(@Param("userId")Long userId) throws Exception;
	
	/**
	 * 验证手机号是否已注册
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public int fetchCountByMobile(@Param("mobile") String mobile) throws Exception;
	
	/**
	 * 验证邮箱是否已注册
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public int fetchCountByEmail(@Param("email") String email) throws Exception;
	
	/**
	 * 用户登录
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public UserInfo login(@Param("mobile")String mobile, @Param("password")String password) throws Exception;
	
	/**
	 * 通过手机号获取用户数据
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public UserInfo fetchUserByMobile(@Param("mobile") String mobile) throws Exception;
	
	/**
	 * 判断原密码是否正确
	 * @param userId
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public int checkOldPasswordIsExist(@Param("userId") Long userId, @Param("password")String password) throws Exception;
	
	/**
	 * 通过邀请码获取用户信息
	 * @param invitionCode
	 * @return
	 * @throws Exception
	 */
	public UserInfo fetchUserByInvitionCode(@Param("invitionCode") String invitionCode) throws Exception;
	
	/**
	 * 验证支付密码是否正确
	 * @param userId
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	public int checkPayPassword(@Param("userId")Long userId, @Param("payPassword")String payPassword) throws Exception;
	
	/**
	 * 通过证件号查询是否有使用
	 * @param idCard
	 * @return
	 * @throws Exception
	 */
	public int fetchCountByIdCardNo(@Param("idCard")String idCard) throws Exception;
}
