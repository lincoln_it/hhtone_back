package cn.waleychain.exchange.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseCoinInfoMapper;
import cn.waleychain.exchange.model.CoinInfo;

/**
 * 币种信息
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:39
 */
public interface CoinInfoMapper extends BaseCoinInfoMapper {
	
	/**
	 * 分页获取币种列表
	 * @param name
	 * @param title
	 * @param coinType
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public Page<CoinInfo> fetchCoinPageList(@Param("name")String name, @Param("title")String title, @Param("coinType")Integer coinType, @Param("status")Integer status) throws Exception;
	
	/**
	 * 通过名称获取币种
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public CoinInfo fetchCoinByName(@Param("name")String name) throws Exception;
	
	/**
	 * 通过ID获取币种
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public CoinInfo fetchCoinById(@Param("coinId")Long coinId) throws Exception;
	
	/**
	 * 获取所有币种
	 * @return
	 * @throws Exception
	 */
	public List<CoinInfo> fetchCoinAllList() throws Exception;
	
	/**
	 * 获取币种列表
	 * @param coinType
	 * @param walletType
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public List<CoinInfo> fetchCoinList(@Param("coinType") Integer coinType, @Param("walletType") Integer walletType, @Param("status") Integer status) throws Exception;
	
}
