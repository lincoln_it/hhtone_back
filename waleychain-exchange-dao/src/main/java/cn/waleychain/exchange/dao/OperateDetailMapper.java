package cn.waleychain.exchange.dao;

import cn.waleychain.exchange.dao.base.BaseOperateDetailMapper;

/**
 * 相关操作流水
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 14:23:13
 */
public interface OperateDetailMapper extends BaseOperateDetailMapper {
	
}
