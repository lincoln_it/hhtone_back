package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.UserLog;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户操作日志
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:20
 */
public interface BaseUserLogMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(UserLog t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<UserLog> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(UserLog t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	UserLog queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<UserLog> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
