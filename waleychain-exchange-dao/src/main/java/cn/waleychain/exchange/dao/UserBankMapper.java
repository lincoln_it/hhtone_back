package cn.waleychain.exchange.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.waleychain.exchange.dao.base.BaseUserBankMapper;
import cn.waleychain.exchange.model.UserBank;

/**
 * 用户绑卡信息表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:42
 */
public interface UserBankMapper extends BaseUserBankMapper {
	
	/**
	 * 验证用户是否存在此银行卡
	 * @param userId
	 * @param userBankId
	 * @return
	 * @throws Exception
	 */
	public int checkBankIsUser(@Param("userId")Long userId, @Param("userBankId")Long userBankId) throws Exception;
	
	/**
	 * 获取用户的银行卡列表
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public List<UserBank> fetchBankListByUserId(@Param("userId")Long userId) throws Exception;
}
