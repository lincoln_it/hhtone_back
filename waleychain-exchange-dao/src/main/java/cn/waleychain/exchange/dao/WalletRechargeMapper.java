package cn.waleychain.exchange.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseWalletRechargeMapper;
import cn.waleychain.exchange.model.WalletRecharge;

/**
 * 钱包充值
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:43
 */
public interface WalletRechargeMapper extends BaseWalletRechargeMapper {
	
	/**
	 * 分页获取钱包充币记录
	 * @param userName
	 * @param status
	 * @param coinId
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public Page<WalletRecharge> fetchWalletRechargePageList(@Param("coinWalletId")Long coinWalletId, @Param("userName")String userName, @Param("status")Integer status, @Param("coinId")Long coinId, 
			@Param("mobile")String mobile, @Param("beginDate")Date beginDate, @Param("endDate")Date endDate) throws Exception;

	/**
	 * 通过交易ID和地址获取充值信息
	 * @param txId
	 * @param address
	 * @return
	 * @throws Exception
	 */
	public WalletRecharge fetchWalletRechargeByTxIdAndAddress(@Param("txId")String txId, @Param("address")String address) throws Exception;
	
	/**
	 * 查询未处理的充值记录
	 * @param coinId
	 * @param beforeDate
	 * @return
	 */
	List<WalletRecharge> fetchNotDealInWallet(@Param("coinId") long coinId, @Param("beforeDate") Date beforeDate);
	
}
