package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.FileResource;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 文件资源
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:18
 */
public interface BaseFileResourceMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(FileResource t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<FileResource> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(FileResource t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "resId") Long resId);
	
	/**
	 * 通过主键获取数据
	 */
	FileResource queryByPrimaryKey(@Param(value = "resId") Long resId);
	
	/**
	 * 查询所有数据
	 */
	List<FileResource> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
