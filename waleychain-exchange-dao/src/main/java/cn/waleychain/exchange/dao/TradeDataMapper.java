package cn.waleychain.exchange.dao;

import java.util.List;

import cn.waleychain.exchange.core.entity.NewPriceDTO;
import cn.waleychain.exchange.dao.base.BaseTradeDataMapper;

/**
 * 市场行情
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-23 16:18:55
 */
public interface TradeDataMapper extends BaseTradeDataMapper {
	
	/**
     * 查询最新成交价
     * @return
     */
    List<NewPriceDTO> fetchNewPrice();
}
