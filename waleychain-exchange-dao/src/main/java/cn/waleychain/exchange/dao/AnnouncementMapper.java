package cn.waleychain.exchange.dao;

import cn.waleychain.exchange.dao.base.BaseAnnouncementMapper;

/**
 * 系统公告
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:38
 */
public interface AnnouncementMapper extends BaseAnnouncementMapper {
	
}
