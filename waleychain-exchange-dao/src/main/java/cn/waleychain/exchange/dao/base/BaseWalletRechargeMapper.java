package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.WalletRecharge;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 钱包充值
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:21
 */
public interface BaseWalletRechargeMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(WalletRecharge t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<WalletRecharge> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(WalletRecharge t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	WalletRecharge queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<WalletRecharge> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
