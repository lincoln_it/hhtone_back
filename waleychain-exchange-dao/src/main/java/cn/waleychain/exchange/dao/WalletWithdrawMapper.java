package cn.waleychain.exchange.dao;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseWalletWithdrawMapper;
import cn.waleychain.exchange.model.WalletWithdraw;

/**
 * 钱包提现
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:43
 */
public interface WalletWithdrawMapper extends BaseWalletWithdrawMapper {
	
	/**
	 * 获取用户提币申请信息
	 * @param withdrawId
	 * @return
	 * @throws Exception
	 */
	public WalletWithdraw fetchWalletWithdrawById(@Param("withdrawId")Long withdrawId) throws Exception;
	
	/**
	 * 分页获取钱包提币记录
	 * @param userName
	 * @param status
	 * @param coinId
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public Page<WalletWithdraw> fetchWalletWithdrawPageList(@Param("coinWalletId")Long coinWalletId, @Param("userName")String userName, @Param("status")Integer status, @Param("coinId")Long coinId, @Param("mobile")String mobile, @Param("beginDate")Date beginDate, @Param("endDate")Date endDate) throws Exception;
	
}
