package cn.waleychain.exchange.dao;

import cn.waleychain.exchange.dao.base.BaseCoinWalletChangeMapper;

/**
 * 钱包数据流水记录（?该表只保留核心字段，用视图实现统计流水功能）
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:39
 */
public interface CoinWalletChangeMapper extends BaseCoinWalletChangeMapper {
	
}
