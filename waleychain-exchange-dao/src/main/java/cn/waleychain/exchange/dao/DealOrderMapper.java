package cn.waleychain.exchange.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.core.entity.TradeTurnoverDetails;
import cn.waleychain.exchange.dao.base.BaseDealOrderMapper;
import cn.waleychain.exchange.model.DealOrder;

/**
 * 成交订单
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:39
 */
public interface DealOrderMapper extends BaseDealOrderMapper {
	
	/**
	 * 分页获取完成订单列表
	 * @param marketId
	 * @param userId
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public Page<DealOrder> fetchDealOrderPageList(@Param("marketId")Long marketId, @Param("userName")String userName, @Param("dealId")Long dealId, @Param("status")Integer status, @Param("type")Integer type) throws Exception;
	
	/**
	 * 获取最后交易日的成交价
	 * @param marketId
	 * @param currentDate
	 * @return
	 */
	public BigDecimal fetchLastClosePrice(@Param("marketId")long marketId, @Param("currentDate")String currentDate);
	
	/**
	 * 查询已成交单
	 * @param marketId 交易市场ID
	 * @param entrusBuyOrderId 委托买单ID
	 * @param entrustSellOrderId 委托卖单ID
	 * @param status 状态
	 * @return
	 */
	public DealOrder fetchByOrderIdAndStatus(@Param("marketId") long marketId,  @Param("entrusBuyOrderId") long entrusBuyOrderId, @Param("entrustSellOrderId") long entrustSellOrderId, @Param("status") int status);
	
	/**
	 * 获取指定市场最新前多少条记录
	 * @param marketId
	 * @param status
	 * @param limitCount
	 * @return
	 * @throws Exception
	 */
	public List<DealOrder> fetchDealOrderListLimit(@Param("marketId")long marketId, @Param("status")Integer status, @Param("limitCount")Integer limitCount) throws Exception;
	
	public TradeTurnoverDetails fetchSecondsTurnoverPrice(@Param("marketId") long marketId, @Param("date") Date date);
}
