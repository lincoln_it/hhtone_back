package cn.waleychain.exchange.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.waleychain.exchange.dao.base.BaseWalletCollectTaskMapper;
import cn.waleychain.exchange.model.WalletCollectTask;

/**
 * 钱包转账定时任务
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-28 16:53:47
 */
public interface WalletCollectTaskMapper extends BaseWalletCollectTaskMapper {
	
	List<WalletCollectTask> fetchTodoTask(@Param("nowDate") Date nowDate);
}
