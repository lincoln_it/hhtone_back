package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.UserInfo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户信息表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:20
 */
public interface BaseUserInfoMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(UserInfo t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<UserInfo> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(UserInfo t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "userId") Long userId);
	
	/**
	 * 通过主键获取数据
	 */
	UserInfo queryByPrimaryKey(@Param(value = "userId") Long userId);
	
	/**
	 * 查询所有数据
	 */
	List<UserInfo> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
