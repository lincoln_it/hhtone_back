package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.UserBank;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户绑卡信息表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:20
 */
public interface BaseUserBankMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(UserBank t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<UserBank> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(UserBank t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "userBankId") Long userBankId);
	
	/**
	 * 通过主键获取数据
	 */
	UserBank queryByPrimaryKey(@Param(value = "userBankId") Long userBankId);
	
	/**
	 * 查询所有数据
	 */
	List<UserBank> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
