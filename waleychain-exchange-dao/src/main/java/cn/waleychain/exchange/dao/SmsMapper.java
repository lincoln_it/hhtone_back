package cn.waleychain.exchange.dao;

import cn.waleychain.exchange.dao.base.BaseSmsMapper;

/**
 * 用户短信记录
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:41
 */
public interface SmsMapper extends BaseSmsMapper {
	
}
