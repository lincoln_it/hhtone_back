package cn.waleychain.exchange.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseMarketMapper;
import cn.waleychain.exchange.model.Market;

/**
 * 市场
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:41
 */
public interface MarketMapper extends BaseMarketMapper {

	/**
	 * 分页获取市场列表
	 * @param name
	 * @param title
	 * @param status
	 * @param transAreaId
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public Page<Market> fetchMarketPageList(@Param("name")String name, @Param("title")String title, @Param("status")Integer status, 
			 @Param("transAreaId")String transAreaId) throws Exception;
	
	/**
	 * 通过市场名称获取市场信息
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public Market fetchMarketInfoByName(@Param("name")String name) throws Exception;
	
	/**
	 * 获取市场信息
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	public Market fetchMarketInfoById(@Param("marketId")Long marketId) throws Exception;
	
	/**
	 * 获取所有市场列表
	 * @return
	 * @throws Exception
	 */
	public List<Market> fetchMarketAllList() throws Exception;
	
}
