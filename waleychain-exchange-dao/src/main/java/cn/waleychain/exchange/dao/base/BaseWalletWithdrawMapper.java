package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.WalletWithdraw;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 钱包提现
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:21
 */
public interface BaseWalletWithdrawMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(WalletWithdraw t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<WalletWithdraw> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(WalletWithdraw t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	WalletWithdraw queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<WalletWithdraw> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
