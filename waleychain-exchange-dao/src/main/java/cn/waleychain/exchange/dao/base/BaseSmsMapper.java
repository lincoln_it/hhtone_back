package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.Sms;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户短信记录
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:19
 */
public interface BaseSmsMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(Sms t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<Sms> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(Sms t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	Sms queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<Sms> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
