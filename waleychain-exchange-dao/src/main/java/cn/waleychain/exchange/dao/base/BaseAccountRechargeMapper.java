package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.AccountRecharge;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 人民币充值记录
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:16
 */
public interface BaseAccountRechargeMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(AccountRecharge t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<AccountRecharge> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(AccountRecharge t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	AccountRecharge queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<AccountRecharge> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
