package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.WalletCollectTask;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 钱包转账定时任务
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-28 16:53:47
 */
public interface BaseWalletCollectTaskMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(WalletCollectTask t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<WalletCollectTask> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(WalletCollectTask t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 通过主键获取数据
	 */
	WalletCollectTask queryByPrimaryKey(@Param(value = "idNo") Long idNo);
	
	/**
	 * 查询所有数据
	 */
	List<WalletCollectTask> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
