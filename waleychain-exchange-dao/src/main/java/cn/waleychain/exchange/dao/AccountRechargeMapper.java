package cn.waleychain.exchange.dao;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cn.waleychain.exchange.dao.base.BaseAccountRechargeMapper;
import cn.waleychain.exchange.model.AccountRecharge;

/**
 * 人民币充值记录
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:38
 */
public interface AccountRechargeMapper extends BaseAccountRechargeMapper {
	
	/**
	 * 分页获取资金账号充值记录
	 * @param userName
	 * @param realName
	 * @param beginDate
	 * @param endDate
	 * @param status
	 * @param tradeNo
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public Page<AccountRecharge> fetchAccountRechargePageList(@Param("userName")String userName, @Param("realName")String realName, 
			@Param("beginDate")Date beginDate, @Param("endDate")Date endDate, @Param("status")Integer status, @Param("tradeNo")String tradeNo) throws Exception;
	
	/**
	 * 通过充值标识获取充值信息
	 * @param idNo
	 * @return
	 * @throws Exception
	 */
	public AccountRecharge fetchAccountRechargeById(@Param("idNo")Long idNo) throws Exception;
}
