package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.DealOrder;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 成交订单
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:18
 */
public interface BaseDealOrderMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(DealOrder t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<DealOrder> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(DealOrder t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "dealId") Long dealId);
	
	/**
	 * 通过主键获取数据
	 */
	DealOrder queryByPrimaryKey(@Param(value = "dealId") Long dealId);
	
	/**
	 * 查询所有数据
	 */
	List<DealOrder> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
