package cn.waleychain.exchange.dao.base;

import cn.waleychain.exchange.model.CoinInfo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 币种信息
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 16:09:17
 */
public interface BaseCoinInfoMapper {
	
	/**
	 * 插入单条数据
	 */
	int insertSelective(CoinInfo t);
	
	/**
	 * 批量插入数据
	 */
	int insertBatch(@Param(value = "list") List<CoinInfo> list);
	
	/**
	 * 通过主键更新数据
	 */
	int updateByPrimaryKeySelective(CoinInfo t);
	
	/**
	 * 通过主键删除数据
	 */
	int deleteByPrimaryKey(@Param(value = "coinId") Long coinId);
	
	/**
	 * 通过主键获取数据
	 */
	CoinInfo queryByPrimaryKey(@Param(value = "coinId") Long coinId);
	
	/**
	 * 查询所有数据
	 */
	List<CoinInfo> queryList();
	
	/**
	 * 查询总条数
	 */
	int queryTotal();
	
}
