/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.schedule.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.service.trade.TradeDataService;

/**
 * 获取缓存数据
 * @author chenx
 * @date 2018年4月24日 上午10:02:16
 * @version 1.0 
 */
@Component
public class TradeDataSearchTask {
	
	private static final Logger mLog = LoggerFactory.getLogger(TradeDataSearchTask.class);
	
	@Autowired
	private TradeDataService tradeDataService;
	
	@Scheduled(fixedDelay = 2000) // 延迟2000毫秒执行
	public void dataSearch() {
		try {
			tradeDataService.syncGetData();
		} catch (Exception e) {
			LoggerHelper.printLogErrorNotThrows(mLog, e, "获取缓存数据：" + e.getMessage());
		}
	}
}
