/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.schedule.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.service.trade.TradeService;

/**
 * 处理委托订单
 * @author chenx
 * @date 2018年4月21日 下午12:57:43
 * @version 1.0
 */
@Component
public class TradeOrderProcessTask {
	
	private static final Logger mLog = LoggerFactory.getLogger(TradeOrderProcessTask.class);
	
	@Autowired
	private TradeService tradeService;
	
	@Scheduled(fixedDelay = 500) // 延迟500毫秒执行
	public void run() {
		try {
			tradeService.tradeOrderProcess();
		} catch (Exception e) {
			LoggerHelper.printLogErrorNotThrows(mLog, null, "处理委托订单异常：" + e.getMessage());
		}
	}

}
