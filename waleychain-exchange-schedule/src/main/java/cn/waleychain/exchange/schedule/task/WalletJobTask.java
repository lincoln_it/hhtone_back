/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.schedule.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.utils.IPUtils;
import cn.waleychain.exchange.feign.CoinWalletServiceFeign;
import cn.waleychain.exchange.service.impl.wallet.WalletCollectTaskServiceImpl;

/**
 * 交易对的最新成交记录（50条）
 * @author chenx
 * @date 2018年4月23日 上午9:44:37
 * @version 1.0 
 */
@Component
@PropertySource(value = { "classpath:application.yml" })
public class WalletJobTask {

private static final Logger mLog = LoggerFactory.getLogger(TradeOrderProcessTask.class);
	
	@Value("${wallet.acceptor.enabled}")
	private int acceptorEnabled;
	@Value("${wallet.task.enabled}")
	private int taskEnabled;
	@Value("${wallet.task.ip}")
	private String taskIp;
	@Value("${wallet.task.minconf}")
	private int minconf;
	
	private boolean isInit = false;
	
	@Autowired
	private CoinWalletServiceFeign coinWalletFeign;
	
	@Autowired
	private WalletCollectTaskServiceImpl walletTaskService;
	

	@Scheduled(cron="0 0/1 * * * ?") // 延迟60000毫秒执行
	public void handler() {
		try {
			LoggerHelper.printLogInfo(mLog, "serverHost=" + this.taskIp + ",acceptorEnabled=" + this.acceptorEnabled + ",taskEnabled=" + this.taskEnabled);
		 	if (!IPUtils.getLocalhost().contains(this.taskIp)) {
		 		return;
		 	}
		 	
		 	LoggerHelper.printLogInfo(mLog, "this.isInit=" + this.isInit);
		 	if (this.acceptorEnabled == 1 && !this.isInit) {
		 		coinWalletFeign.acceptorInit();
		 		this.isInit = true;
		 	}
		 	if (this.taskEnabled == 1) {
		 		LoggerHelper.printLogInfo(mLog, "minconf=" + this.minconf);
		 		long now = System.currentTimeMillis();
		 		LoggerHelper.printLogInfo(mLog, "======= 2处理未完成的充值 =======");
		 		this.coinWalletFeign.processNotDealReceiveCoin();
		 		this.coinWalletFeign.cleanRecSuccessTxId();
		 		LoggerHelper.printLogInfo(mLog, "======= 2处理共计耗时" + (System.currentTimeMillis() - now) / 1000L + "s ======");
		 		now = System.currentTimeMillis();
		 		LoggerHelper.printLogInfo(mLog, "======= 3归集任务 =======");
		 		this.walletTaskService.collectTask();
		 		LoggerHelper.printLogInfo(mLog, "======= 3归集任务" + (System.currentTimeMillis() - now) / 1000L + "s ======");
		 	}
		} catch (Exception e) {
			LoggerHelper.printLogErrorNotThrows(mLog, null, e.getMessage());
		}
	}
	
}
