/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.schedule.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.service.trade.CacheDataService;

/**
 * 交易对的最新成交记录（50条）
 * @author chenx
 * @date 2018年4月23日 上午9:44:37
 * @version 1.0 
 */
@Component
public class TradeOrderSearchTask {

private static final Logger mLog = LoggerFactory.getLogger(TradeOrderProcessTask.class);
	
	@Autowired
	private CacheDataService orderService;
	
	/**
	 * 根据交易对ID获取最新50笔成交记录，并保存到内存中
	 */
	@Scheduled(fixedDelay = 2000) // 延迟2000毫秒执行
	public void turnoverOrderSearch() {
		try {
			orderService.resetTurnoverOrderList();
		} catch (Exception e) {
			LoggerHelper.printLogErrorNotThrows(mLog, null, "根据交易对ID获取最新50笔成交记录，并保存到内存中：" + e.getMessage());
		}
	}
	
	/**
	 * 最新委托订单【卖单/买单】（10档）方便K线交易数据显示
	 */
	@Scheduled(fixedDelay = 2000) // 延迟2000毫秒执行
	public void newestEntrustOrderSearch() {
		try {
			orderService.resetNewestEntrustOrderList();
		} catch (Exception e) {
			LoggerHelper.printLogErrorNotThrows(mLog, null, "最新委托订单【卖单/买单】（10档）方便K线交易数据显示，并保存到内存中：" + e.getMessage());
		}
	}
}
