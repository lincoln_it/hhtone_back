package cn.waleychain.exchange.schedule;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 调度
 * @author chenx
 * @date 2018年4月20日 下午6:20:42
 * @version 1.0
 */
@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@MapperScan(basePackages="cn.waleychain.exchange.dao")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"cn.waleychain.exchange.feign"})
@ComponentScan(basePackages = {"cn.waleychain.exchange.schedule.config", "cn.waleychain.exchange.service.impl", "cn.waleychain.exchange.schedule.task"})
@EnableTransactionManagement
public class ScheduleApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(ScheduleApplication.class, args);
	}
}
