/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.sys.provider;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.DictionaryInfo;
import cn.waleychain.exchange.service.sys.ConfigService;

@RestController
@RequestMapping("/cfg")
public class ConfigServiceProvider {

	@Autowired
	private ConfigService configService;
	
	/**
	 * 获取业务配置类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchConfigType")
	public List<DictionaryInfo> fetchConfigType() throws Exception {
		
		return configService.fetchConfigType();
	}
	
	/**
	 * 获取业务配置列表
	 * @param type
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchConfigList")
	public PageInfo<DictionaryInfo> fetchConfigList( 
			@RequestParam Long type, 
			@RequestParam int showCount, @RequestParam int currentPage) throws Exception {
		
		return configService.fetchConfigList(type, showCount, currentPage);
	}
	
	/**
	 * 新增配置
	 * @param type
	 * @param name
	 * @param code
	 * @param value
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addConfig")
	public boolean addConfig(@RequestParam(required = false) Long type, 
			@RequestParam String name, @RequestParam String code, @RequestParam String value) throws Exception {
		
		return configService.addSystemConfig(type, 
				name, code, value);
	}
	
	/**
	 * 变更业务配置
	 * @param idNo
	 * @param name
	 * @param value
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/modify")
	public boolean modify( 
			@RequestParam Long idNo, 
			@RequestParam String name, 
			@RequestParam String value, @RequestParam int status, @RequestParam String remark) throws Exception {
		
		return configService.updateSystemConfig(idNo, name, value, status, remark);
	}
	
	/**
	 * 获取配置内容
	 * @param configCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchConfigInfo")
	public DictionaryInfo fetchConfigInfo( 
			@RequestParam String configCode) throws Exception {
		
		return configService.fetchSystemConfigInfo(configCode);
	}
	
	/**
	 * 获取配置值
	 * @param configCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/fetchSystemConfigValue")
	public String fetchSystemConfigValue(@RequestParam String configCode) throws Exception {
		
		return configService.fetchSystemConfigValue(configCode);
	}
}
