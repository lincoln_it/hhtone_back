package cn.waleychain.exchange.sys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 系统平台
 * 
 * @author chenx
 * @date 2018年4月11日 上午9:58:39
 * @version 1.0
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages="cn.waleychain.exchange.dao")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"cn.waleychain.exchange.feign"})
@ComponentScan(basePackages = {"cn.waleychain.exchange.service.impl", "cn.waleychain.exchange.sys.provider"})
@EnableTransactionManagement
public class SysApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(SysApplication.class, args);
	}
}
