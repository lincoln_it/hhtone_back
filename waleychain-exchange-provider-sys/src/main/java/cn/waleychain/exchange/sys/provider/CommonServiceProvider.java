/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.sys.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.SysParaKey;
import cn.waleychain.exchange.core.constant.SmsContent;
import cn.waleychain.exchange.core.utils.RandomUtils;
import cn.waleychain.exchange.model.Sms;
import cn.waleychain.exchange.service.sys.CommonService;
import cn.waleychain.exchange.service.sys.ConfigService;

@RestController
@RequestMapping("/common")
public class CommonServiceProvider {

	@Autowired
	private CommonService commonService;
	
	@Autowired
	private ConfigService configService;
	
	/**
	 * 发送短信
	 * @param mobile
	 * @param content
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sendSms")
	public Sms sendSms(@RequestParam String mobile, @RequestParam String content) throws Exception {
		
		return commonService.sendSms(mobile, content);
	}
	
	/**
	 * 发送短信验证码
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sendSmsCode")
	public boolean sendSmsCode(@RequestParam String mobile) throws Exception {
		
		// 发送验证码
		String code = RandomUtils.generateInteger(6);
		String time = configService.fetchSystemConfigValue(SysParaKey.SYS_CONFIG_PHONE_VAILDATE_CODE_EXPIRE_TIME);
		String content = SmsContent.genMobileCode(code, time);

		Sms sms = commonService.sendSms(mobile, content);
		
		return commonService.addMobileCode(sms, code);
	}
	
	/**
	 * 验证短信验证码
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/vaildateSmsCode")
	public boolean vaildateSmsCode(@RequestParam String mobile, @RequestParam String code) throws Exception {
		
		return commonService.checkSmsVaildateCode(mobile, code);
	}
}
