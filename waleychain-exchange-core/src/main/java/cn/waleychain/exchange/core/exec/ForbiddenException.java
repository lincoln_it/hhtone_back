package cn.waleychain.exchange.core.exec;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author chenx
 * @date 2016年8月2日
 * @version V1.0
 * @description:
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class ForbiddenException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -508455524797678102L;

	public ForbiddenException(String message) {
		super(message);
	}

}
