/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.constant;

public enum KLineDataType {

	/**
	 * 1秒
	 */
	ONE_SECONDS_LIST("oneseconds"),

	/**
	 * 5秒
	 */
	FIVE_SECONDS_LIST("fiveseconds"),

	/**
	 * 1分
	 */
	ONE_MINUTES_LIST("oneminutes"),

	/**
	 * 3分
	 */
	THREE_MINUTES_LIST("threeminutes"),

	/**
	 * 5分
	 */
	FIVE_MINUTES_LIST("fiveminutes"),

	/**
	 * 15分
	 */
	FIFTEEN_MINUTES_LIST("fifteenminutes"),

	/**
	 * 30分
	 */
	THIRTY_MINUTES_LIST("thirtyminutes"),

	/**
	 * 1小时
	 */
	ONE_HOURS_LIST("onehours"),

	/**
	 * 2小时
	 */
	TWO_HOURS_LIST("twohours"),

	/**
	 * 4小时
	 */
	FOUR_HOURS_LIST("fourhours"),

	/**
	 * 6小时
	 */
	SIX_HOURS_LIST("sixhours"),

	/**
	 * 12小时
	 */
	TWELVE_HOURS_LIST("twelvehours"),

	/**
	 * 1天
	 */
	ONE_DAY_LIST("oneday"),

	/**
	 * 7天
	 */
	SEVEN_DAY_LIST("sevenday"),
	
	/**
	 * 一个月
	 */
	ONE_MONTH_LIST("onemonth"),
	
	/**
	 * 1年
	 */
	ONE_YEAR_LIST("oneyear");
	
	private String key;
	
	KLineDataType(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
	
	public static KLineDataType convertByKey(String key) {
		for (KLineDataType type : KLineDataType.values()) {
			if (type.getKey().equals(key)) {
				return type;
			}
		}
		
		return null;
	}
}
