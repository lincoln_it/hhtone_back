/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.cache;

/**
 * 缓存应用中常量编写
 * @author chenx
 * @date 2017年8月11日 下午3:47:13
 * @version 1.0 
 */
public class CacheConts {

	/**
	 * 登录token缓存名称
	 */
	public static final String CACHE_NAME_LOGIN_TOKEN = "cn.starsand.cache.name.login.token";
	
	/**
	 * 系统统计存储名称
	 */
	public static final String CACHE_NAME_SYS_STATIS_STORAGE = "cn.starsand.cache.name.sys.statis.storage";
	
	/**
	 * 手机验证码缓存名称
	 */
	public static final String CACHE_NAME_PHONE_VERIFY_CODE = "cn.starsand.cache.name.phone.verify.code";
	
	/**
	 * 系统永久存储的缓存名称
	 */
	public static final String CACHE_NAME_SYS_PERPETUAL_STORAGE = "cn.starsand.cache.name.sys.perpetual.storage";
	
	/**
	 * 用户平台钱包锁前缀
	 */
	public static final String LOCK_COIN_WALLET = "coinWalletLock_";
	
	/**
	 * 任务缓存前缀名称
	 */
	public static final String CACHE_TASK_LIST = "cache_task_list_";
	
	/**
	 * 交易委托单锁
	 */
	public static final String LOCK_TRADE_ENTRUST_ORDER_KEY = "trade_entrust_lock_key";
	
	/**
	 * 委托订单缓存
	 */
	public static final String ENTRUST_ORDER = "entrust_order_";
	
	/**
	 * 交易市场静态缓存名称
	 */
	public static final String CACHE_STATIC_LIST_TRADE_MARKET = "cache_static_list_trade_market_";
	
	/**
	 * 交易市场缓存锁
	 */
	public static final String LOCK_TRADE_MARKET_KEY = "trade_market_lock";
	
	/**
	 * 币种静态缓存名称
	 */
	public static final String CACHE_STATIC_LIST_TRADE_COIN = "cache_static_list_trade_coin_";
	
	/**
	 * 币种缓存锁
	 */
	public static final String LOCK_TRADE_COIN_KEY = "trade_coin_lock";
	
	/**
	 * 订单相关静态缓存数据锁前缀
	 */
	public static final String LOCK_STATIC_DATA = "static_data_lock_";

	/**
	 * 最新交易单缓存
	 */
	public static final String NEWST_DEAL_ORDER_KEY = "newst_deal_order_key";
	
	/**
	 * 最新委托买单缓存
	 */
	public static final String NEWST_ENTRUST_BUY_ORDER_KEY = "newst_entrust_buy_order_key";
	
	/**
	 * 最新委托卖单缓存
	 */
	public static final String NEWST_ENTRUST_SELL_ORDER_KEY = "newst_entrust_sell_order_key";
	
	/**
	 * 最新行情缓存
	 */
	public static final String NEWST_PRICE_KEY = "newst_price_key";
	
	/**
	 * k线数据缓存
	 */
	public static final String K_LINE_DATA_KEY = "k_line_data_key";
	
	/**
	 * 
	 */
	public static final String CACHE_DATA_LIST = "cache_data_list_";
	
	/**
	 * 访问k线数据用户缓存
	 */
	public static final String CACHE_STATIC_KLINE_USER_LIST = "cache_static_kline_user_list_";
	
}
