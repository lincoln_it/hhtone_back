/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.utils;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * 随机工具类
 * @author luhf
 * @date 2017年7月6日 下午3:49:06
 * @version 1.0
 */
public class RandomUtils {
	
	public static final String ALLCHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String LETTERCHAR = "abcdefghijkllmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String NUMBERCHAR = "0123456789";

	private RandomUtils(){}
	
	/**
	 * 返回uuid
	 * @return
	 */
	public synchronized static String uuid(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
	/**
	 * 产生随机数字
	 */
	public static char genRandomDec() {
		return (char) ('0' + (int) (java.lang.Math.random() * 10));
	}

	/**
	 * 产生随机数字格式的字符串
	 */
	public static String genRandomDec(int size) {
		StringBuffer sb = new StringBuffer();
		while (size-- > 0) {
			sb.append(genRandomDec());
		}
		return sb.toString();
	}

	/**
	 * 产生随机小写字母
	 */
	public static char genRandomLowerAlpha() {
		return (char) ('a' + (int) (java.lang.Math.random() * 26));
	}

	/**
	 * 产生随机小写字母字符串
	 */
	public static String genRandomLowerAlpha(int size) {
		StringBuffer sb = new StringBuffer();
		while (size-- > 0) {
			sb.append(genRandomLowerAlpha());
		}
		return sb.toString();
	}

	/**
	 * 产生随机大写字母
	 */
	public static char genRandomUpperAlpha() {
		return (char) ('A' + (int) (java.lang.Math.random() * 26));
	}

	/**
	 * 产生随机大写字母串
	 */
	public static String genRandomUpperAlpha(int size) {
		StringBuffer sb = new StringBuffer();
		while (size-- > 0) {
			sb.append(genRandomUpperAlpha());
		}
		return sb.toString();
	}
	
	/**
	 * 产生随机大写字母和数字的组合字符串
	 * @param size
	 * @return
	 */
	public static String genRandomUpperAlphaAndDec(int size) {
		StringBuffer sb = new StringBuffer();
		while (size-- > 0) {
			sb.append(genRandomUpperAlpha());
			if (size-- > 0) {
				sb.append(genRandomDec());
			}
		}
		return sb.toString();
	}	
	/**
	 * 返回一个定长的随机字符串(只包含大小写字母、数字)
	 * 
	 * @param length
	 *            随机字符串长度
	 * @return 随机字符串
	 */
	public static String generateString(int length) {
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(ALLCHAR.charAt(random.nextInt(ALLCHAR.length())));
		}
		return sb.toString();
	}
	
	/**
	 * 返回一个定长的随机数字(只包含数字)
	 * @param length 随机字符串长度
	 * @return 随机字符串
	 */
	public static String generateInteger(int length){
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(NUMBERCHAR.charAt(random.nextInt(NUMBERCHAR.length())));
		}
		return sb.toString();
	}

	/**
	 * 生成一个定长的纯0字符串
	 * 
	 * @param length
	 *            字符串长度
	 * @return 纯0字符串
	 */
	public static String generateZeroString(int length) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			sb.append('0');
		}
		return sb.toString();
	}

	/**
	 * 根据数字生成一个定长的字符串，长度不够前面补0
	 * 
	 * @param num
	 *            数字
	 * @param fixdlenth
	 *            字符串长度
	 * @return 定长的字符串
	 */
	public static String toFixdLengthString(long num, int fixdlenth) {
		StringBuffer sb = new StringBuffer();
		String strNum = String.valueOf(num);
		if (fixdlenth - strNum.length() >= 0) {
			sb.append(generateZeroString(fixdlenth - strNum.length()));
		} else {
			throw new RuntimeException("将数字" + num + "转化为长度为" + fixdlenth + "的字符串发生异常！");
		}
		sb.append(strNum);
		return sb.toString();
	}

	/**
	 * 每次生成的len位数都不相同
	 * 
	 * @param param
	 * @return 定长的数字
	 */
	public static int getNotSimple(int[] param, int len) {
		Random rand = new Random();
		for (int i = param.length; i > 1; i--) {
			int index = rand.nextInt(i);
			int tmp = param[index];
			param[index] = param[i - 1];
			param[i - 1] = tmp;
		}
		int result = 0;
		for (int i = 0; i < len; i++) {
			result = result * 10 + param[i];
		}
		return result;
	}
	
	/**
	 * 获取几以内的随机数
	 * @param xx 
	 * @return
	 */
	public static int genRandom(int xx) {
		
		Random random = new Random();
		
		int d = random.nextInt(xx);
		
		return d;
	}
	
	/**
	 * 获取两数之间的随机数
	 * @param xx 
	 * @return
	 */
	public static int genTwinAmongRandom(int min, int max) {
		
		Random random = new Random();
		
		int n = 0;
		do {
			n = random.nextInt(max + 1);
		} while (n < min);
		
		return n;
	}

	public static void main(String[] args) {
		System.out.println("返回一个定长的随机字符串(只包含大小写字母、数字):" + generateString(5));
		System.out.println("返回一个定长的随机字符串(只包含大小写字母、数字):" + generateInteger(5));
		System.out.println("生成一个定长的纯0字符串:" + generateZeroString(10));
		System.out.println("根据数字生成一个定长的字符串，长度不够前面补0:" + toFixdLengthString(123, 10));
		int[] in = { 1, 2, 3, 4, 5, 6, 7 };
		System.out.println("每次生成的len位数都不相同:" + getNotSimple(in, 3));
		
		System.out.println(DateUtils.format(new Date(), DateUtils.fullFormat1));
	}
}
