package cn.waleychain.exchange.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 交易市场响应数据
 * @author chenx
 * @date 2018年4月26日 上午10:23:56
 * @version 1.0
 */
public class MarketResponseDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8597794208383341794L;

	/**
     * 主键
     */
    private long id;

    /**
     * 市场名称
     */
    private String name;

    /**
     * 标题
     */
    private String title;

    /**
     * 图片
     */
    private String image;

    /**
     * 当前价
     */
    private BigDecimal currentPrice;

    /**
     * 当前价（人民币）
     */
    private BigDecimal currentPriceCNY;

    /**
     * 最高价
     */
    private BigDecimal highPrice;

    /**
     * 最低价
     */
    private BigDecimal lowPrice;

    /**
     * 日总交易量
     */
    private BigDecimal volume;

    /**
     * 日总成交额
     */
    private BigDecimal turnoverAmount;

    /**
     * 涨幅
     */
    private String change;

    /**
     * 价格走势
     */
    private List<BigDecimal> priceTrend;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice.setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getCurrentPriceCNY() {
        return currentPriceCNY;
    }

    public void setCurrentPriceCNY(BigDecimal currentPriceCNY) {
        this.currentPriceCNY = currentPriceCNY.setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(BigDecimal highPrice) {
        this.highPrice = highPrice;
    }

    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(BigDecimal lowPrice) {
        this.lowPrice = lowPrice.setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume.setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getTurnoverAmount() {
        return turnoverAmount;
    }

    public void setTurnoverAmount(BigDecimal turnoverAmount) {
        this.turnoverAmount = turnoverAmount.setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public List<BigDecimal> getPriceTrend() {
        return priceTrend;
    }

    public void setPriceTrend(List<BigDecimal> priceTrend) {
        this.priceTrend = priceTrend;
    }
}
