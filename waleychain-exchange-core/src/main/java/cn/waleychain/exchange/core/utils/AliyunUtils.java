/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.utils;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

public class AliyunUtils {

	/**
	 * 
	 * @param userName
	 * @param idCard
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static boolean validateIdCard(String userName, String idCard) {
		String url = "http://aliyunverifyidcard.haoservice.com/idcard/VerifyIdcardv2";
		String appcode = "eb7e63fcdb584666875c5ea26995d071";
		Map<String, String> headers = new HashMap();
		headers.put("Authorization", "APPCODE " + appcode);
		Map<String, String> querys = new HashMap();
		querys.put("cardNo", idCard);
		querys.put("realName", userName);
		String response = null;

		try {
			response = HttpUtils.get(url, querys, headers);
			JSONObject jsonObj = JSONObject.parseObject(response);
			if (0 == jsonObj.getIntValue("error_code")) {
				JSONObject json = jsonObj.getJSONObject("result");
				return json.getBoolean("isok");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
}
