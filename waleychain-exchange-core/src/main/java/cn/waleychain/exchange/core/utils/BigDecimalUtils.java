/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.utils;

import java.math.BigDecimal;

public class BigDecimalUtils {

	public static BigDecimal genInitValue(double value) {
		
		return new BigDecimal(value);
	}
	
	public static BigDecimal genInitValue() {
		
		return genInitValue(0.0);
	}
	
	public static BigDecimal getRoundAmount(BigDecimal amount, int scale) {
		return amount == null ? null : amount.setScale(scale, BigDecimal.ROUND_UP);
	}
}
