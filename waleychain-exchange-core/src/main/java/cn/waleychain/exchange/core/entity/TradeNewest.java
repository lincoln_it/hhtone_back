package cn.waleychain.exchange.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeNewest implements Serializable {

	private static final long serialVersionUID = 4918040279075334409L;
	private long marketId;
	private BigDecimal openPrice = new BigDecimal(0);
	private BigDecimal closePrice = new BigDecimal(0);
	private BigDecimal maxPrice = new BigDecimal(0);
	private BigDecimal minPrice = new BigDecimal(0);
	private BigDecimal totalNum = new BigDecimal(0);
	private BigDecimal currentPrice = new BigDecimal(0);
	private BigDecimal buyOne = new BigDecimal(0);
	private BigDecimal sellOne = new BigDecimal(0);

	public TradeNewest() {
	}

	public long getMarketId() {
		return this.marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public BigDecimal getOpenPrice() {
		return this.openPrice;
	}

	public void setOpenPrice(BigDecimal openPrice) {
		this.openPrice = openPrice;
	}

	public BigDecimal getClosePrice() {
		return this.closePrice;
	}

	public void setClosePrice(BigDecimal closePrice) {
		this.closePrice = closePrice;
	}

	public BigDecimal getMaxPrice() {
		return this.maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}

	public BigDecimal getMinPrice() {
		return this.minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public BigDecimal getTotalNum() {
		return this.totalNum;
	}

	public void setTotalNum(BigDecimal totalNum) {
		this.totalNum = totalNum;
	}

	public BigDecimal getCurrentPrice() {
		return this.currentPrice;
	}

	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}

	public BigDecimal getBuyOne() {
		return this.buyOne;
	}

	public void setBuyOne(BigDecimal buyOne) {
		this.buyOne = buyOne;
	}

	public BigDecimal getSellOne() {
		return this.sellOne;
	}

	public void setSellOne(BigDecimal sellOne) {
		this.sellOne = sellOne;
	}
}
