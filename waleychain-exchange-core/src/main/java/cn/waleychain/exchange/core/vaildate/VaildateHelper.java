package cn.waleychain.exchange.core.vaildate;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.waleychain.exchange.core.Field;
import cn.waleychain.exchange.core.exec.ClientException;
import cn.waleychain.exchange.core.result.RetResultCode;
import cn.waleychain.exchange.core.utils.DateUtils;
import cn.waleychain.exchange.core.utils.StringUtils;

/**
 * @author chenx
 * @date 2016年9月6日
 * @version V1.0
 * @description: 
 */
public class VaildateHelper {

	
	private static final String phoneExp = "^1[3|5|8|4|7][0-9]\\d{8}";
	
	private static final String identityExp = "^(\\d{6})(\\d{4})(\\d{2})(\\d{2})(\\d{3})([0-9]|X)$";
	
	/**
	 * 验证手机号
	 * @param phone
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildatePhone(String phone) throws ClientException {
		
		Pattern p = Pattern.compile(phoneExp);  
			  
		Matcher m = p.matcher(phone);
		
		boolean bool = m.matches();  
		
		if (!bool) {
			throw new ClientException(RetResultCode.E11002.ret_code, RetResultCode.E11002.ret_msg);
		}
		
		return true;
	}
	
	/**
	 * 验证身份证号
	 * @param identity
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateIdentity(boolean isNotNull, String identity) throws ClientException {
		
		if (isNotNull) {
			vaildateStrIsNull(identity, "身份证号码");
		} else {
			if (StringUtils.isBlank(identity)) 
				return true;
		}
		
		Pattern p = Pattern.compile(identityExp);  
			  
		Matcher m = p.matcher(identity);
		
		boolean bool = m.matches();  
		
		if (!bool) {
			throw new ClientException(RetResultCode.E11003.ret_code, RetResultCode.E11003.ret_msg);
		}
		
		return true;
	}
	
	/**
	 * 验证String型的参数
	 * @param str1
	 * @param other
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateStrParams(String str1, String ... other) throws ClientException {
		
		if (StringUtils.isBlank(str1)) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg);
		}
		
		if (other != null) {
			for (String str : other) {
				if (StringUtils.isBlank(str)) {
					throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg);
				}
			}
		}
		
		return true;
	}
	
	/**
	 * 验证Int型的参数
	 * @param str1
	 * @param other
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateIntParams(Integer param, String explain, int ... other) throws ClientException {
		
		if (param == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + " 为空");
		}
		
		if (other != null) {
			
			int n = other.length;
			if (n == 1) {
				if (param < other[0]) {
					throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + "（" + param + "）不能小于 " + other[0]);
				}
			} else {
				boolean b = false;
				for (int str : other) {
					if (param == str) {
						b = true;
					}
				}
				if (!b) {
					throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + "（" + param + "）错误");
				}
			}
			
		}
		
		return true;
	}
	
	/**
	 * 验证Int型的参数
	 * @param str1
	 * @param other
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateLongParams(Long param, String explain, Long ... other) throws ClientException {
		
		if (param == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + " 为空");
		}
		
		if (other != null) {
			
			int n = other.length;
			if (n == 1) {
				if (param < other[0]) {
					throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + "（" + param + "）不能小于 " + other[0]);
				}
			} else {
				boolean b = false;
				for (long str : other) {
					if (param == str) {
						b = true;
					}
				}
				if (!b) {
					throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + "（" + param + "）错误");
				}
			}
			
		}
		
		return true;
	}
	
	/**
	 * 验证Double型的参数
	 * @param str1
	 * @param other
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateDoubleParams(Double param, String explain, double ... other) throws ClientException {
		
		if (param == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + " 为空");
		}
		
		if (other != null) {
			
			int n = other.length;
			if (n == 1) {
				if (param < other[0]) {
					throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + "（" + param + "）不能小于 " + other[0]);
				}
			} else {
				boolean b = false;
				for (double str : other) {
					if (param == str) {
						b = true;
					}
				}
				if (!b) {
					throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + "（" + param + "）错误");
				}
			}
			
		}
		
		return true;
	}
	
	/**
	 * 验证金额的参数
	 * @param str1
	 * @param other
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateMoneyParams(Double param, String explain, double limit) throws ClientException {
		
		if (param == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + " 为空");
		}
			
		if (param < limit) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + "（" + param + "）不能小于 " + limit);
		}
		
		return true;
	}
	
	/**
	 * 验证开始时间和结束时间型的参数
	 * @param str1
	 * @param other
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateBeginAndEndParams(Date begin, Date end) throws ClientException {
		
		if (begin == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " 开始时间（" + begin + "）错误");
		}
		
		if (end == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " 结束时间（" + end + "）错误");
		}
		
		if (begin.after(end)) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + 
					" 开始时间不能在结束时间之后（" + DateUtils.format(begin, DateUtils.longFormat) + " - " + DateUtils.format(end, DateUtils.longFormat) + "）");
		}
		
		return true;
	}
	
	/**
	 * 验证String型的null
	 * @param param 参数值
	 * @param explain 参数说明
	 * @param length 验证长度 -1 则不验证
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateStrIsNull(String param, String explain, int length, boolean isNotNull) throws ClientException {
		
		if (isNotNull) {
			if (StringUtils.isBlank(param)) {
				throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + " 为空");
			}
		}
		
		if (length != -1 && StringUtils.isNotBlank(param)) {
			if (param.length() > length) {
				throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + "（" + param + "）超过长度限制 " + length);
			}
		}
		
		return true;
	}
	
	/**
	 * 验证String型的null
	 * @param param 参数值
	 * @param explain 参数说明
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateStrIsNull(String param, String explain) throws ClientException {
		
		vaildateStrIsNull(param, explain, -1, true);
		
		return true;
	}
	
	/**
	 * 验证Object型的null
	 * @param param 参数值
	 * @param explain 参数说明
	 * @param length 验证长度 -1 则不验证
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateObjectIsNull(Object param, String explain) throws ClientException {
		
		if (param == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + explain + " 为空或格式错误");
		}
		
		return true;
	}
	
	/**
	 * 验证OBject型的参数，为null时抛出异常
	 * @param str1
	 * @param other
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateObjParams(Object str1, Object ... other) throws ClientException {
		
		if (str1 == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg);
		}
		
		if (other != null) {
			for (Object str : other) {
				if (str == null) {
					throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg);
				}
			}
		}
		
		return true;
	}
	
	/**
	 * 验证OBject型的参数，全为null时抛出异常
	 * @param str1
	 * @param other
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateParamsAllIsNull(Object str1, Object ... other) throws ClientException {
		
		boolean isNull = false;
		if (str1 != null) {
			isNull = true;
		}
		
		if (other != null) {
			for (Object str : other) {
				if (str != null) {
					isNull = true;
				}
			}
		}
		
		if (!isNull) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " 不能同时为空");
		}
		
		return true;
	}
	
	/**
	 * 验证entity型的null
	 * @param entity 实体对象
	 * @param msg 附加说明
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateEntityIsNull(Object obj, String msg) throws ClientException {
		
		if (obj == null) {
			if (StringUtils.isNotBlank(msg)) 
				throw new ClientException(RetResultCode.E13001.ret_code, RetResultCode.E13001.ret_msg + "：" + msg); 
			else
				throw new ClientException(RetResultCode.E13001.ret_code, RetResultCode.E13001.ret_msg);
		}
		
		return true;
	}
	
	/**
	 * 验证entity型的null
	 * @param entity 实体对象
	 * @param msg 附加说明
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateEntityIsNull(Object obj) throws ClientException {
		
		vaildateEntityIsNull(obj, null);
		
		return true;
	}
	
	/**
	 * 验证entity是否自己可以操作
	 * @param entity 实体对象
	 * @return
	 * @throws ClientException
	 */
	public static boolean vaildateCanOperateEntity(String id1, String id2) throws ClientException {
		
		if (!id1.equals(id2)) {
			throw new ClientException(RetResultCode.E13002.ret_code, RetResultCode.E13002.ret_msg);
		}
		
		return true;
	}
	
	public static boolean vaildateParse(boolean bool, String content) throws ClientException {
		
		if (!bool) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + content);
		}
		return true;
	}
	
	public static boolean vaildateObjNotNull(Object obj, String content) throws ClientException {
		
		if (obj == null) {
			throw new ClientException(RetResultCode.E11001.ret_code, RetResultCode.E11001.ret_msg + " " + content);
		}
		
		return true;
	}
	
	public static void vaildateBooleanResult(boolean result, Field retCode) throws ClientException {
		if (result) {
			throw new ClientException(retCode);
		}
	}
	
	public static void vaildateBooleanResult(boolean result, Field retCode, Object attrMsg) throws ClientException {
		if (result) {
			throw new ClientException(retCode, attrMsg);
		}
	}
	
}
