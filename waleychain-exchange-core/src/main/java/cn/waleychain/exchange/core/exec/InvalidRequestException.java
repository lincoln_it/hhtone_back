package cn.waleychain.exchange.core.exec;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author chenx
 * @date 2016年8月2日
 * @version V1.0
 * @description:
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2356298811411308981L;

	public InvalidRequestException(String message) {
		super(message);
	}
}
