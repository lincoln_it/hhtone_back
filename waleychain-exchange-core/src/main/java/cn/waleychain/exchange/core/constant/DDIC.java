/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.constant;

/**
 * 数据字典常量
 * @author chenx
 * @date 2018年2月7日 上午10:33:58
 * @version 1.0 
 */
public class DDIC {

	/**
	 * boolean类型的数据字典
	 * @author chenx
	 * @date 2018年2月7日 上午11:46:33
	 * @version 1.0DDIC
	 */
	public enum Boolean {
		
		/**
		 * 是/true
		 */
		 BOOL_TRUE_1(1),
		
		/**
		 * 否/false
		 */
		BOOL_FALSE_0(0);
		
		public int id;
		private Boolean(int id) {
			this.id = id;
		}
		
		public int[] convertIntValues() {
			int[] arr = new int[Boolean.values().length];
			for (int i = 0; i < Boolean.values().length; i++) {
				arr[i] = Boolean.values()[i].id;
			}
			
			return arr;
		}
	}
	
	/**
	 * 系统数据状态
	 * @author chenx
	 * @date 2018年2月7日 上午11:46:33
	 * @version 1.0DDIC
	 */
	public enum DataStat {
		
		/**
		 * 正常
		 */
		NORMAL_STAT_1(1),
		
		/**
		 * 删除
		 */
		DELETE_STAT_0(0);
		
		public int id;
		private DataStat(int id) {
			this.id = id;
		}

	}
	
	/**
	 * 资金账号业务类别
	 * @author chenx
	 * @date 2018年2月7日 上午10:34:10
	 * @version 1.0DDIC 
	 */
	public enum AccountBizType {
		
		/**
		 * 委托订单（entrust_order）
		 */
		ENTRUST_ORDER_1(1),
		
		/**
		 * 成交订单（deal_order）
		 */
		DEAL_ORDER_2(2),
		
		/**
		 * 提现（account_withdraw）
		 */
		ACCOUNT_WITHDRAW_3(3),
		
		/**
		 * 充值（account_recharge）
		 */
		ACCOUNT_RECHARGE_4(4);
		
		public int id;
		private AccountBizType(int id) {
			this.id = id;
		}
		
	}
	
	/**
	 * 资金账号收支流水类型
	 * @author chenx
	 * @date 2018年2月7日 上午11:43:57
	 * @version 1.0DDIC
	 */
	public enum AccountChangeType {
		
		/**
		 * 收入
		 */
		INCOME_1(1),
		
		/**
		 * 支出
		 */
		PAYOUT_2(2);
		
		public int id;
		private AccountChangeType(int id) {
			this.id = id;
		}

	}
	
	/**
	 * 资金账号收支流水状态
	 * @author chenx
	 * @date 2018年2月7日 上午11:43:57
	 * @version 1.0DDIC
	 */
	public enum AccountChangeStatus {
		
		/**
		 * 提现：待审核
		 */
		STATUS_30(30),
		
		/**
		 * 提现：拒绝
		 */
		STATUS_31(31),
		
		/**
		 * 提现：完成
		 */
		STATUS_32(32),
		
		/**
		 * 充值：未付款
		 */
		NOTPAY(40),
		
		/**
		 * 充值：到账成功
		 */
		TO_SUCCESS(41),
		
		/**
		 * 充值：人工到账
		 */
		LABOUR(42),
		
		/**
		 * 充值：处理中
		 */
		BEING_PROCESSED(43),
		
		/**
		 * 未找到
		 */
		STATUS_NOT(-1);
		
		public int id;
		private AccountChangeStatus(int id) {
			this.id = id;
		}

		public static AccountChangeStatus convertId(int id) {
			for (AccountChangeStatus status : AccountChangeStatus.values()) {
				if (id == status.id) {
					return status;
				}
			}
			
			return STATUS_NOT;
		}
	}
	
	/**
	 * 用户资金账户状态
	 * @author chenx
	 * @date 2018年2月7日 下午2:51:05
	 * @version 1.0DDIC
	 */
	public enum UserAccountStatus {
		
		/**
		 * 禁用
		 */
		DISABLED(0),
		
		/**
		 * 启用
		 */
		ENABLED(1);
		
		public int id;
		private UserAccountStatus(int id) {
			this.id = id;
		}
		
	}
	
	/**
	 * 手机验证码状态
	 * @author chenx
	 * @date 2018年2月7日 下午2:54:38
	 * @version 1.0DDIC
	 */
	public enum SmsStatus {
		/**
		 * 失败
		 */
		STATUS_0(0),
		
		/**
		 * 成功
		 */
		STATUS_1(1);
		
		public int id;
		private SmsStatus(int id) {
			this.id = id;
		}
		
	}
	
	/**
	 * 用户身份认证级别
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum UserAuthLevel {
		/**
		 * 低
		 */
		AUTH_LEVEL_1(1),
		
		/**
		 * 中
		 */
		AUTH_LEVEL_2(2),
		
		/**
		 * 高
		 */
		AUTH_LEVEL_3(3);
		
		public int id;
		private UserAuthLevel(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 用户银行卡状态
	 * @author chenx
	 * @date 2018年2月7日 下午2:51:05
	 * @version 1.0DDIC
	 */
	public enum UserBankStatus {
		
		/**
		 * 停用
		 */
		STATUS_0(0),
		
		/**
		 * 启用
		 */
		STATUS_1(1);
		
		public int id;
		private UserBankStatus(int id) {
			this.id = id;
		}
		
	}
	
	/**
	 * 币种类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum CoinType {
		/**
		 * 钱包币1
		 */
		WALLET_COIN(1),
		
		/**
		 * 认购币2
		 */
		OFFER_BUY(2);
		
		public int id;
		private CoinType(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 钱包类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum WalletType {
		/**
		 * 人民币
		 */
		XNB(0),
		
		/**
		 * 比特币系列
		 */
		DEFAULT(1),
		
		/**
		 * 以太坊
		 */
		ETH(2),
		
		/**
		 * 以太坊代币
		 */
		ETH_TOKEN(3);
		
		public int id;
		private WalletType(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 币种状态
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum CoinStatus {
		/**
		 * 无效
		 */
		DISALBED(0),
		
		/**
		 * 有效
		 */
		ENABLED(1);
		
		public int id;
		private CoinStatus(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 币种是否允许交易
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum IsAllowTrade {
		/**
		 * 不可交易
		 */
		IS_ALLOW_TRADE_0(0),
		
		/**
		 * 允许交易
		 */
		IS_ALLOW_TRADE_1(1);
		
		public int id;
		private IsAllowTrade(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 币种是否允许提现
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum IsAllowWithdraw {
		/**
		 * 禁止提现
		 */
		IS_ALLOW_WITHDRAW_0(0),
		
		/**
		 * 允许提现
		 */
		IS_ALLOW_WITHDRAW_1(1),
		/**
		 * 提现缓慢
		 */
		IS_ALLOW_WITHDRAW_2(2);
		
		public int id;
		private IsAllowWithdraw(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 币种是否允许充值
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum IsAllowRecharge {
		/**
		 * 禁止充值
		 */
		IS_ALLOW_RECHARGE_0(0),
		
		/**
		 * 允许充值
		 */
		IS_ALLOW_RECHARGE_1(1),
		/**
		 * 充值缓慢
		 */
		IS_ALLOW_RECHARGE_2(2);
		
		public int id;
		private IsAllowRecharge(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 用户平台钱包状态
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum CoinWalletStatus {
		/**
		 * 正常
		 */
		NORMAL(1),
		
		/**
		 * 冻结
		 */
		FREEZE(2);
		
		public int id;
		private CoinWalletStatus(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 市场币对状态
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum MarketStatus {
		/**
		 * 禁用
		 */
		MARKET_STATUS_0(0),
		
		/**
		 * 启用
		 */
		MARKET_STATUS_1(1);
		
		public int id;
		private MarketStatus(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 用户个人钱包状态
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum UserWalletStatus {
		/**
		 * 无效
		 */
		USER_WALLET_STATUS_0(0),
		
		/**
		 * 有效
		 */
		USER_WALLET_STATUS_1(1);
		
		public int id;
		private UserWalletStatus(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 钱包币提现类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum WalletWithdrawType {
		/**
		 * 站外
		 */
		WALLET_WITHDRAW_TYPE_0(0),
		
		/**
		 * 站内
		 */
		WALLET_WITHDRAW_TYPE_1(1);
		
		public int id;
		private WalletWithdrawType(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 钱包账号操作业务类别
	 * @author chenx
	 * @date 2018年2月7日 上午10:34:10
	 * @version 1.0DDIC 
	 */
	public enum WalletBizType {
		
		/**
		 * 委托订单（entrust_order）
		 */
		ENTRUST_ORDER(1),
		
		/**
		 * 成交订单（deal_order）
		 */
		DEAL_ORDER(2),
		
		/**
		 * 提现（wallet_withdraw）
		 */
		WALLET_WITHDRAW(3),
		
		/**
		 * 充值（wallet_recharge）
		 */
		WALLET_RECHARGE(4);
		
		public int id;
		private WalletBizType(int id) {
			this.id = id;
		}
		
	}
	
	/**
	 * 用户钱包账号操作类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum TradeDetailType {
		/**
		 * 收入
		 */
		TRADE_DETAIL_TYPE_INCOME(1),
		
		/**
		 * 支出
		 */
		TRADE_DETAIL_TYPE_PAYOUT(2);
		
		public int id;
		private TradeDetailType(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 资金账号收支流水状态
	 * @author chenx
	 * @date 2018年2月7日 上午11:43:57
	 * @version 1.0DDIC
	 */
	public enum WalletChangeStatus {
		
		/**
		 * 提现：待审核
		 */
		STATUS_30(30),
		
		/**
		 * 提现：通过
		 */
		STATUS_31(31),
		
		/**
		 * 提现：审核拒绝
		 */
		STATUS_32(32),
		
		/**
		 * 提现：用户撤销
		 */
		STATUS_33(33),
		
		/**
		 * 打币中
		 */
		STATUS_34(34),
		
		/**
		 * 充值：失败
		 */
		RECHARGE_FAIL(40),
		
		/**
		 * 充值：成功
		 */
		RECHARGE_SUSS(41),
		
		/**
		 * 未找到
		 */
		STATUS_NOT(-1);
		
		public int id;
		private WalletChangeStatus(int id) {
			this.id = id;
		}

		public static WalletChangeStatus convertId(int id) {
			for (WalletChangeStatus status : WalletChangeStatus.values()) {
				if (id == status.id) {
					return status;
				}
			}
			
			return STATUS_NOT;
		}
	}
	
	/**
	 * 交易类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum TradeType {
		/**
		 * 买
		 */
		BUY(1),
		
		/**
		 * 卖
		 */
		SELL(2),
		
		/**
		 * 
		 */
		NOT(-1);
		
		public int id;
		private TradeType(int id) {
			this.id = id;
		}
		
		public static TradeType convertById(int id) {
			for (TradeType type : TradeType.values()) {
				if (type.id == id) {
					return type;
				}
			}
			
			return TradeType.NOT;
		}
	}
	
	/**
	 * 委托单状态
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum EntrustOrderStatus {
		/**
		 * 未成交
		 */
		UNSETTLED(0),
		/**
		 * 已成交
		 */
		TRADED(1),
		/**
		 * 已撤销
		 */
		CANCELED(2),
		/**
		 * 异常单
		 */
		EXCEPTION(4);
		
		public int id;
		private EntrustOrderStatus(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 成交单状态
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum DealOrderStatus {
		/**
		 * 未成交
		 */
		UNSETTLED(0),
		/**
		 * 已成交
		 */
		TRADED(1),
		/**
		 * 已撤销
		 */
		CANCELED(2),
		/**
		 * 异常单
		 */
		ABNORMITY(4);
		
		public int id;
		private DealOrderStatus(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 支付类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum PayType {
		/**
		 * 银联支付
		 */
		BANK(1),
		
		/**
		 * 支付宝支付
		 */
		ALIPAY(2),
		
		/**
		 * 微信支付
		 */
		WECHAT(3),
		
		/**
		 * 线下支付
		 */
		LINEPAY(4);
		
		public int id;
		private PayType(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 系统配置类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum SysConfigType {
		/**
		 * 系统
		 */
		SYS(1),
		
		/**
		 * 业务
		 */
		BIZ(2),
		
		/**
		 * 交易区域
		 */
		TRANS_AREA(3);
		
		public int id;
		private SysConfigType(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 资源类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum ResourceType {
		/**
		 * 文件
		 */
		FILE(1),
		
		/**
		 * 图片
		 */
		IMAGE(2);
		
		public int id;
		private ResourceType(int id) {
			this.id = id;
		}
	}
	
	/**
	 * 系统码表值类型
	 * @author chenx
	 * @date 2018年3月2日 上午9:59:06
	 * @version 1.0DDIC 
	 */
	public enum DictionaryValueType {
		/**
		 * 字符串
		 */
		STRING(1),
		/**
		 * 布尔
		 */
		BOOLEAN(2),
		/**
		 * 数值
		 */
		INT(3),
		/**
		 * double
		 */
		DOUBLE(4);
		
		public int id;
		private DictionaryValueType(int id) {
			this.id = id;
		}
	}
}
