/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core;

/**
 * 通用常量记录
 * @author chenx
 * @date 2017年8月11日 下午3:53:46
 * @version 1.0 
 */
public class Global {

	/**一串通用码*/
	public static final String PROJ_GENERAL_CODE = "WALEYCHAIN_";
	
	/**系统中管理员默认密码*/
	public static final String PROJ_DEFAULT_PASS = "123456";
	
	/**
	 * 当前登录的token
	 */
	public static final String CURRENT_LOGIN_TOKEN = "CURRENT_LOGIN_TOKEN";
	
	/**
	 * 默认分隔符
	 */
	public static final String DEFAULT_SEPARATOR = ",";
	
	/**
	 * 超级管理员默认角色
	 */
	public static final String SUPER_ADMIN_DEFAULT_ROLE = "*";
	
	/**
	 * 超级管理员默认权限
	 */
	public static final String SUPER_ADMIN_DEFAULT_PERMISSION = "fun:*";
	
	/**
	 * 默认10分钟毫秒时间
	 */
	public static final long DEFAULT_TIMES = (10 * 60 * 1000L); // 十分钟有效
	
	/**
	 * 市场title中币种的分隔符
	 */
	public static final String MARKET_COIN_TITLE_SEPARATOR = "/";
	/**
	 * 市场那么中币种的分隔符
	 */
	public static final String MARKET_COIN_NAME_SEPARATOR = "_";
	
	/**
	 * 金额保留小数位数
	 */
	public static final int RETAIN_DECIMAL_LEN = 8;

	/**
	 * 存储图片验证码名称
	 */
	public static final String IMAGE_VAILDATE_CODE_NAME = "EASYLIFE_CAPCHA";
	
}
