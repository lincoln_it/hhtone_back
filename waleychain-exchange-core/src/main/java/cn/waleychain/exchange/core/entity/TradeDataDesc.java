package cn.waleychain.exchange.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.fastjson.JSONObject;

import cn.waleychain.exchange.core.utils.DateUtils;
import cn.waleychain.exchange.core.utils.StringUtils;

public class TradeDataDesc implements Serializable {

    private static final long serialVersionUID = 5657818675843906805L;

    private BigDecimal maxPrice;
    private BigDecimal minPrice;
    private BigDecimal openPrice;
    private BigDecimal closePrice;
    private BigDecimal totalNum;
    private Date statisticsTime;

    public TradeDataDesc(Date statisticsTime) {
        this.maxPrice = new BigDecimal(0);
        this.minPrice = new BigDecimal(0);
        this.openPrice = new BigDecimal(0);
        this.closePrice = new BigDecimal(0);
        this.totalNum = new BigDecimal(0);
        this.statisticsTime = statisticsTime;
    }

    public TradeDataDesc(String desc) {
        if (StringUtils.isNotBlank(desc)) {
            JSONObject obj = JSONObject.parseObject(desc);
            if (obj != null) {
                String maxPrice = obj.getString("maxPrice");
                this.maxPrice = StringUtils.isBlank(maxPrice) ? null : new BigDecimal(maxPrice);
                String minPrice = obj.getString("minPrice");
                this.minPrice = StringUtils.isBlank(minPrice) ? null : new BigDecimal(minPrice);
                String openPrice = obj.getString("openPrice");
                this.openPrice = StringUtils.isBlank(openPrice) ? null : new BigDecimal(openPrice);
                String closePrice = obj.getString("closePrice");
                this.closePrice = StringUtils.isBlank(closePrice) ? null : new BigDecimal(closePrice);
                String totalNum = obj.getString("totalNum");
                this.totalNum = StringUtils.isBlank(totalNum) ? null : new BigDecimal(totalNum);
                String statisticsTime = obj.getString("statisticsTime");
                this.statisticsTime = StringUtils.isBlank(statisticsTime) ? null : DateUtils.convertDate(statisticsTime);
            }

        } else {
            throw new IllegalArgumentException("Invalid data desc.");
        }
    }

    public BigDecimal getMaxPrice() {
        return this.maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinPrice() {
        return this.minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getOpenPrice() {
        return this.openPrice;
    }

    public void setOpenPrice(BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    public BigDecimal getClosePrice() {
        return this.closePrice;
    }

    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    public BigDecimal getTotalNum() {
        return this.totalNum;
    }

    public void setTotalNum(BigDecimal totalNum) {
        this.totalNum = totalNum;
    }

    public void setStatisticsTime(Date statisticsTime) {
        this.statisticsTime = statisticsTime;
    }

    public Date getStatisticsTime() {
        return this.statisticsTime;
    }

    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("{");
        s.append("\"").append("maxPrice").append("\":\"").append(this.maxPrice == null ? "" : this.maxPrice.toPlainString()).append("\",");
        s.append("\"").append("minPrice").append("\":\"").append(this.minPrice == null ? "" : this.minPrice.toPlainString()).append("\",");
        s.append("\"").append("openPrice").append("\":\"").append(this.openPrice == null ? "" : this.openPrice.toPlainString()).append("\",");
        s.append("\"").append("closePrice").append("\":\"").append(this.closePrice == null ? "" : this.closePrice.toPlainString()).append("\",");
        s.append("\"").append("totalNum").append("\":\"").append(this.totalNum == null ? "" : this.totalNum.toPlainString()).append("\",");
        s.append("\"").append("statisticsTime").append("\":\"").append(this.statisticsTime == null ? "" : DateUtils.format(this.statisticsTime)).append("\"}");
        return s.toString();
    }
}
