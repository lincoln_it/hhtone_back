/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.vaildate;

import javax.servlet.http.HttpServletRequest;

public class SecurityCodeUtil {

	public static boolean whetherImgCaptchaMatch(String customCaptcha, HttpServletRequest request) {
		
		boolean captchaMatch = true;

		String requestCaptcha = (String) request.getSession().getAttribute("EASYLIFE_CAPCHA");

		if (customCaptcha.equals(requestCaptcha)) {
			captchaMatch = false;
		}

		return captchaMatch;
	}

}
