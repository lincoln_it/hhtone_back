package cn.waleychain.exchange.core.exec;

import cn.waleychain.exchange.core.Field;

/**
 * @author chenx
 * @date 2016年9月6日
 * @version V1.0
 * @description: 
 */
public class NotFoundDataException extends RuntimeException {

	private int msgCode;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4760542432229799445L;

	public NotFoundDataException() {
	}

	public NotFoundDataException(String message) {
		super(message);
	}
	
	public NotFoundDataException(int msgCode, String message) {
		super(msgCode + ":" + message);
		this.msgCode = msgCode;
	}
	
	public NotFoundDataException(Field field) {
		super(field.getRet_code() + ":" + field.getRet_msg());
		this.msgCode = field.getRet_code();
	}
	
	public NotFoundDataException(Field field, String attrMsg) {
		super(field.getRet_code() + ":" + field.getRet_msg() + " " + attrMsg);
		this.msgCode = field.getRet_code();
	}
	
	public int getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(int msgCode) {
		this.msgCode = msgCode;
	}

}
