package cn.waleychain.exchange.core;

/**
 * @author chenx
 * @date 2016年11月9日
 * @version V1.0
 * @description: 系统参数KEY配置 
 */
public class SysParaKey {

	/**
	 * 默认服务器接口路径
	 */
	public static final String defaultApiReqUrl = "defaultApiReqUrl";
	
	/**
	 * 系统默认（MD5）签名密钥
	 */
	public static final String defaultMd5SignKey = "defaultMd5SignKey";
	
	/**
	 * 系统配置：手机验证码过期时长（分钟）
	 */
	public static final String SYS_CONFIG_PHONE_VAILDATE_CODE_EXPIRE_TIME = "SYS_CONFIG_PHONE_VAILDATE_CODE_EXPIRE_TIME";
	
	/**
	 * 系统配置：平台用户标识ID
	 */
	public static final String SYS_CONFIG_PLATFORM_USER_ID = "SYS_CONFIG_PLATFORM_USER_ID";
	
	/**
	 * 系统配置：btcx币的标识ID
	 */
	public static final String SYS_CONFIG_BTCX_COIN_ID = "SYS_CONFIG_BTCX_COIN_ID";
	
	/**
	 * 系统配置：btcx兑hht币的交易市场标识ID
	 */
	public static final String SYS_CONFIG_BTCX_2_HHT_MARKET_ID = "SYS_CONFIG_BTCX_2_HHT_MARKET_ID";
	
	/**
	 * 系统配置：邮件服务器IP
	 */
	public static final String SYS_CONFIG_EMAIL_SERVER_IP = "SYS_CONFIG_EMAIL_SERVER_IP";
	
	/**
	 * 系统配置：邮件服务器端口
	 */
	public static final String SYS_CONFIG_EMAIL_SERVER_PORT = "SYS_CONFIG_EMAIL_SERVER_PORT";
	
	/**
	 * 系统配置：邮件服务器用户
	 */
	public static final String SYS_CONFIG_EMAIL_SERVER_USER = "SYS_CONFIG_EMAIL_SERVER_USER";
	
	/**
	 * 系统配置：邮件服务器密码
	 */
	public static final String SYS_CONFIG_EMAIL_SERVER_PASS = "SYS_CONFIG_EMAIL_SERVER_PASS";
	
	/**
	 * 系统配置：邮箱注册验证地址
	 */
	public static final String SYS_CONFIG_REGS_TO_EMAIL_URL = "SYS_CONFIG_REGS_TO_EMAIL_URL";
	
	/**
	 * 系统配置：最小取现额
	 */
	public static final String TRADE_CONFIG_WITHDRAWALS_MIN_AMOUNT = "TRADE_CONFIG_WITHDRAWALS_MIN_AMOUNT";
	
	/**
	 * 系统配置：最大取现额
	 */
	public static final String TRADE_CONFIG_WITHDRAWALS_MAX_AMOUNT = "TRADE_CONFIG_WITHDRAWALS_MAX_AMOUNT";
	
	/**
	 * 系统配置：最小取现手续费
	 */
	public static final String TRADE_CONFIG_WITHDRAWALS_MIN_POUNDAGE = "TRADE_CONFIG_WITHDRAWALS_MIN_POUNDAGE";
	
	/**
	 * 系统配置：取现手续费率
	 */
	public static final String TRADE_CONFIG_WITHDRAWALS_POUNDAGE_RATE = "TRADE_CONFIG_WITHDRAWALS_POUNDAGE_RATE";
	
	/**
	 * 系统配置：取现值必须是基数的倍数，基数如果是100，那么取现值只能是100的倍数，例如：200,300等等
	 */
	public static final String TRADE_CONFIG_WITHDRAWALS_BASEAMOUNT = "TRADE_CONFIG_WITHDRAWALS_BASEAMOUNT";
	
	/**
	 * 系统配置：系统交易状态，0表示关闭，1表示开启
	 */
	public static final String TRADE_CONFIG_SYSTEM_STATUS = "TRADE_CONFIG_SYSTEM_STATUS";
	
	/**
	 * 系统配置：提现状态，0表示不能提现，1表示可以提现
	 */
	public static final String TRADE_CONFIG_WITHDRAWALS_STATUS = "TRADE_CONFIG_WITHDRAWALS_STATUS";
	
	/**
	 * 系统配置：每日最大提现额
	 */
	public static final String TRADE_CONFIG_WITHDRAWALS_MAX_AMOUNT_PERDAY = "TRADE_CONFIG_WITHDRAWALS_MAX_AMOUNT_PERDAY";
	
	/**
	 * 系统配置：交易数据同步状态，0表示关闭，1表示开启
	 */
	public static final String TRADE_CONFIG_DATA_SYNC_STATUS = "TRADE_CONFIG_DATA_SYNC_STATUS";
	
	/**
	 * 系统配置：人民币美元换算费率
	 */
	public static final String TRADE_CONFIG_CASH_US = "TRADE_CONFIG_CASH_US";
	
	/**
	 * 系统配置：充值核准后自动转BTCX, 0-关闭, 1-开启
	 */
	public static final String TRADE_CONFIG_AUTO_EXCHANGE_CNY2BTCX = "TRADE_CONFIG_AUTO_EXCHANGE_CNY2BTCX";
	
}
