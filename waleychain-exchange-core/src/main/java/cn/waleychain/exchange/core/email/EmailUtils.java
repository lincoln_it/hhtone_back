/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.email;

import javax.mail.internet.MimeMultipart;

import org.apache.commons.mail.MultiPartEmail;

public class EmailUtils {
	
	/**
	 * 发送超文本邮件
	 * 
	 * @param emailContent
	 *            邮件内容
	 * @throws Exception
	 */
	public static void sendHyperText(EmailConfig config, String toUser, String emailSubject, MimeMultipart mimeMultipart)
			throws Exception {

		if (config == null || toUser == null) {
			throw new Exception("config或toUser配置为空");
		}

		MultiPartEmail email = new MultiPartEmail();
		email.setHostName(config.getServerIp());
		email.setSmtpPort(config.getServerPort());
		// 验证发送者
		email.setAuthentication(config.getServerUser(),
				config.getServerPass());

		// 设置收件人
		email.addTo(toUser);

		email.setFrom(config.getServerUser());
		email.setSubject(emailSubject);
		email.setCharset("GBK");

		// 邮件内容
		email.setContent(mimeMultipart);
		// 发送
		email.send();
	}
}
