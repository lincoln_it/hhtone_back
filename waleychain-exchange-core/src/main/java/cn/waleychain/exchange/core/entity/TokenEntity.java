/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 系统token
 * @author chenx
 * @date 2017年8月11日 下午3:18:42
 * @version 1.0 
 */
public class TokenEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6518170297388047537L;

	/**
	 * token值
	 */
	private Serializable token;
	
	/**
	 * 子系统码
	 */
	private String subSystemCode;
	
	/**
	 * 唯一标识ID（用户端使用）
	 */
	private Long unionId;
	
	/**
	 * 权限资源码列表
	 */
	private List<String> resourceList;
	
	/**
	 * 角色集合
	 */
	private List<String> roleList;

	public Serializable getToken() {
		return token;
	}

	public void setToken(Serializable token) {
		this.token = token;
	}

	public String getSubSystemCode() {
		return subSystemCode;
	}

	public void setSubSystemCode(String subSystemCode) {
		this.subSystemCode = subSystemCode;
	}

	public List<String> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<String> resourceList) {
		this.resourceList = resourceList;
	}

	public List<String> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<String> roleList) {
		this.roleList = roleList;
	}

	public Long getUnionId() {
		return unionId;
	}

	public void setUnionId(Long unionId) {
		this.unionId = unionId;
	}

}
