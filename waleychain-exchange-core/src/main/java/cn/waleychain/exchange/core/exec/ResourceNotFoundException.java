package cn.waleychain.exchange.core.exec;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author chenx
 * @date 2016年8月2日
 * @version V1.0
 * @description:
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4869648115984240140L;

	public ResourceNotFoundException(String message) {
		super(message);
	}

}
