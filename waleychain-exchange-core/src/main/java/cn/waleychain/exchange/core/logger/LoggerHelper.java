/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.logger;

import org.slf4j.Logger;

import cn.waleychain.exchange.core.exec.ClientException;
import cn.waleychain.exchange.core.exec.ForbiddenException;
import cn.waleychain.exchange.core.exec.InvalidRequestException;
import cn.waleychain.exchange.core.exec.ResourceNotFoundException;
import cn.waleychain.exchange.core.utils.StringUtils;

public class LoggerHelper {

	/**
	 * 记录info级别的日志
	 * 
	 * @param mLog
	 * @param e
	 *            异常
	 * @param msgContent
	 *            内容
	 */
	public static void printLogInfo(Logger mLog, Exception e, String msgContent) {

		if (mLog == null) {
			return;
		}

		if (e == null && StringUtils.isBlank(msgContent)) {
			return;
		}

		msgContent = "WALEYCHAIN-INFO =>> " + msgContent;

		if (e != null) {
			mLog.info(msgContent + "：" + e.getMessage(), e);
		} else {
			mLog.info(msgContent);
		}
	}
	
	/**
	 * 记录info级别的日志
	 * 
	 * @param mLog
	 * @param e
	 *            异常
	 * @param msgContent
	 *            内容
	 */
	public static void printLogInfo(Logger mLog, String msgContent) {

		if (mLog == null) {
			return;
		}

		if (StringUtils.isBlank(msgContent)) {
			return;
		}

		msgContent = "WALEYCHAIN-INFO =>> " + msgContent;

		mLog.info(msgContent);
	}

	/**
	 * 记录Debug级别的日志
	 * 
	 * @param mLog
	 * @param e
	 *            异常
	 * @param msgContent
	 *            内容
	 */
	public static void printLogDebug(Logger mLog, String msgContent, String ... msgParam) {

		if (mLog == null) {
			return;
		}

		if (!mLog.isDebugEnabled()) {
			return;
		}
		
		StringBuffer content = new StringBuffer();
		content.append("WALEYCHAIN-DEBUG =>> ");
		
		if (!StringUtils.isBlank(msgContent)) {
			content.append(msgContent);
		}

		if (msgParam != null && msgParam.length > 0) {
			content.append(" PARAMS{");
			for (String param : msgParam) {
				content.append(param);
				content.append(", ");
			}
			content.append("}");
		}
		
		mLog.debug(content.toString());
	}
	
	/**
	 * 记录Error级别的日志
	 * 
	 * @param mLog
	 * @param e
	 *            异常
	 * @param msgContent
	 *            内容
	 */
	public static void printLogErrorNotThrows(Logger mLog, Exception e, String msgContent) {

		if (mLog == null) {
			return;
		}

		if (StringUtils.isBlank(msgContent)) {
			return;
		}

		msgContent = "WALEYCHAIN-ERROR =>> " + msgContent;

		if (e != null) {
			mLog.error(msgContent + "：" + e.getMessage(), e);
		} else {
			mLog.error(msgContent);
		}
	}
	
	/**
	 * 用于输出开始日志
	 * 
	 * @param functionName
	 */
	public static void printStartLog(Logger mLog, String functionName) {
		printStartLog(mLog, functionName, null);
	}

	/**
	 * 用于输出开始日志
	 * 
	 * @param functionName
	 * @param paramDesc
	 * @param param
	 */
	public static void printStartLog(Logger mLog, String functionName, String paramDesc, Object... param) {
		StringBuilder info = new StringBuilder().append("START - ").append(mLog.getName()).append(".")
				.append(functionName).append("(").append(paramDesc).append(")").append("[");

		if (param != null && param.length > 0) {
			for (int i = 0; i < param.length; i++) {
				info.append(param[i] == null ? "null" : param[i].toString());
				info.append(i == param.length - 1 ? "" : ", ");
			}
		}
		info.append("]");
		
		printLogInfo(mLog, info.toString());
	}

	/**
	 * 用于输出结束日志
	 * 
	 * @param functionName
	 */
	public static  void printEndLog(Logger mLog, String functionName) {
		printEndLog(mLog, functionName, null);
	}

	/**
	 * 用于输出结束日志
	 * 
	 * @param functionName
	 * @param paramDesc
	 * @param param
	 */
	public static void printEndLog(Logger mLog, String functionName, String paramDesc, Object... param) {
		StringBuilder info = new StringBuilder().append("  END - ").append(mLog.getName()).append(".")
				.append(functionName).append("(").append(paramDesc).append(")").append("[");

		if (param != null && param.length > 0) {
			for (int i = 0; i < param.length; i++) {
				info.append(param[i] == null ? "null" : param[i].toString());
				info.append(i == param.length - 1 ? "" : ", ");
			}
		}
		info.append("]");
		printLogInfo(mLog, info.toString());
	}

	/**
	 * 记录Error级别的日志
	 * 
	 * @param mLog
	 * @param e
	 *            异常
	 * @param msgContent
	 *            内容
	 */
	public static void printLogError(Logger mLog, Exception e, String msgContent) throws Exception {

		if (mLog == null) {
			return;
		}

		if (e == null && StringUtils.isBlank(msgContent)) {
			return;
		}

		msgContent = "WALEYCHAIN-ERROR =>> " + msgContent;

		if (e != null) {
			if (e instanceof ClientException) {
				ClientException ve = (ClientException) e;
				mLog.error(msgContent + "：" + e.getMessage());
				throw ve;
			} else if (e instanceof InvalidRequestException) {
				InvalidRequestException ve = (InvalidRequestException) e;
				mLog.error(msgContent + "：" + e.getMessage(), e);
				throw ve;
			} else if (e instanceof ResourceNotFoundException) {
				ResourceNotFoundException ve = (ResourceNotFoundException) e;
				mLog.error(msgContent + "：" + e.getMessage(), e);
				throw ve;
			} else if (e instanceof ForbiddenException) {
				ForbiddenException ve = (ForbiddenException) e;
				mLog.error(msgContent + "：" + e.getMessage(), e);
				throw ve;
			} else if (e instanceof RuntimeException) {
				RuntimeException ve = (RuntimeException) e;
				mLog.error(msgContent + "：" + e.getMessage(), e);
				throw ve;
			}
			
			mLog.error(msgContent + "：" + e.getMessage(), e);
			throw new Exception(msgContent + ": " + e.getMessage(), e);
		} else {
			mLog.error(msgContent);
			throw new Exception(msgContent);
		}
	}
}
