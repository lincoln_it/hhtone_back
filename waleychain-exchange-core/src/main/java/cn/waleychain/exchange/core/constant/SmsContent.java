/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.constant;

import java.math.BigDecimal;
import java.util.Date;

import cn.waleychain.exchange.core.utils.DateUtils;

public class SmsContent {

	/**
	 * 短信验证码
	 * @param code
	 * @param time
	 * @return
	 */
	public static String genMobileCode(String code, String time) {
		
		return "验证码：" + code + "，"+ time +"分钟有效，切勿将验证码泄漏于他人。";
	}

	/**
	 * 用户资金充值审核拒绝
	 * @param amount
	 * @param remark
	 * @return
	 */
	public static String genAccountRechargeReviewRefuse(BigDecimal amount, String remark) {
		
		return "您在HashToken申请的线下充值" + amount + "元已被拒绝，拒绝原因为：" + remark + "，如需退款或有疑问请联系在线客服。";
	}
	
	/**
	 * 用户资金充值审核人工到账
	 * @param amount
	 * @param time
	 * @return
	 */
	public static String genAccountRechargeLabour(BigDecimal amount, Date time) {
		
		return "您已在HashToken线下成功充值" + amount + "元，人工到账确认时间为：" + DateUtils.format(time, DateUtils.fullFormat2) + "。";
	}
	
	/**
	 * 用户钱包提现
	 * @param amount
	 * @param remark
	 * @return
	 */
	public static String genWalletWithdraw(BigDecimal amount, String coinName) {
		
		return "您已在HashToken成功转出" + amount + "个" + coinName + "，请注意查收。";
	}
}
