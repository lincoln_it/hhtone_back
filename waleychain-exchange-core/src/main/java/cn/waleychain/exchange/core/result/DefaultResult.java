package cn.waleychain.exchange.core.result;

import cn.waleychain.exchange.core.Field;

/**
 * @author chenx
 * @date 2016年9月8日
 * @version V1.0
 * @description: base响应结果
 */
public class DefaultResult extends IBaseResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5415791048571731565L;

	private Object data;
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public static IBaseResult buildResult(boolean result) {
		if (result) {
			return buildSuccessResult();
		}
		
		return buildFailedResult();
	}
	
	/**
	 * 构造请求操作成功返回结果
	 * @param field 系统定义的返回码
	 * @return
	 */
	public static IBaseResult buildSuccessResult() {
		
		IBaseResult result = new DefaultResult();
		result.setResult(RetResultCode.HANDLER_SUCCESS.result);
		result.setRetCode(RetResultCode.HANDLER_SUCCESS.ret_code);
		result.setRetMsg(RetResultCode.HANDLER_SUCCESS.ret_msg);
		
		return result;
	}
	
	/**
	 * 构造请求操作成功返回结果
	 * @param field 系统定义的返回码
	 * @return
	 */
	public static IBaseResult buildSuccessResult(Object obj) {
		
		DefaultResult result = new DefaultResult();
		result.setResult(RetResultCode.HANDLER_SUCCESS.result);
		result.setRetCode(RetResultCode.HANDLER_SUCCESS.ret_code);
		result.setRetMsg(RetResultCode.HANDLER_SUCCESS.ret_msg);
		result.setData(obj);
		
		return result;
	}
	
	/**
	 * 构造请求操作失败返回结果
	 * @param field 系统定义的返回码
	 * @return
	 */
	public static IBaseResult buildFailedResult(Field field) {
		
		IBaseResult result = new DefaultResult();
		result.setResult(field.result);
		result.setRetCode(field.ret_code);
		result.setRetMsg(field.ret_msg);
		
		return result;
	}
	
	/**
	 * 构造请求操作失败返回结果
	 * @param field 系统定义的返回码
	 * @return
	 */
	public static IBaseResult buildFailedResult() {

		IBaseResult result = new DefaultResult();
		result.setResult(RetResultCode.HANDLER_FIELED.result);
		result.setRetCode(RetResultCode.HANDLER_FIELED.ret_code);
		result.setRetMsg(RetResultCode.HANDLER_FIELED.ret_msg);
		
		return result;
	}
	
	/**
	 * 构造请求操作失败返回结果
	 * @param field 系统定义的返回码
	 * @return
	 */
	public static IBaseResult buildFailedResult(int errorCode, String errorMsg) {

		Field field = new Field(errorCode, errorMsg);
		
		return buildFailedResult(field);
	}
	
	/**
	 * 构造请求操作失败返回结果
	 * @param field 系统定义的返回码
	 * @return
	 */
	public static IBaseResult buildFailedResult(int errorCode, String errorMsg, Object obj) {

		DefaultResult result = new DefaultResult();
		result.setResult("failed");
		result.setRetCode(errorCode);
		result.setRetMsg(errorMsg);
		
		result.setData(obj);
		
		return result;
	}

	public static IBaseResult buildFailedResult(String message) {

		IBaseResult result = new DefaultResult();
		result.setResult(RetResultCode.HANDLER_FIELED.result);
		result.setRetCode(RetResultCode.HANDLER_FIELED.ret_code);
		result.setRetMsg(message);
		
		return result;
	}
}
