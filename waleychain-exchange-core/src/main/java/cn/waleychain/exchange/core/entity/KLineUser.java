/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.entity;

import java.io.Serializable;

public class KLineUser implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8944347646612205669L;

	/**
	 * sessionId
	 */
	private String sessionId;
	
	/**
	 * 市场标识ID
	 */
	private Long marketId;
	
	/**
	 * 数据时段类型（1 分钟 5 分钟 15 分钟...）
	 */
	private String dataType;
	
	/**
	 * 监听路径
	 */
	private String listenerPath;
	
	private Integer kLimit;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getListenerPath() {
		return listenerPath;
	}

	public void setListenerPath(String listenerPath) {
		this.listenerPath = listenerPath;
	}

	public Integer getkLimit() {
		return kLimit;
	}

	public void setkLimit(Integer kLimit) {
		this.kLimit = kLimit;
	}
	
}
