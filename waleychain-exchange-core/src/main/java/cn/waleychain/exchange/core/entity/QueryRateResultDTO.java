package cn.waleychain.exchange.core.entity;

/**
 * 查询美元兑换率返回结果对象
 * @author chenx
 * @date 2018年4月26日 上午10:11:39
 * @version 1.0
 */
public class QueryRateResultDTO {

    /**
     * 状态
     */
    private int success;

    /**
     * 结果
     */
    private ExchangeRateDTO result;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public ExchangeRateDTO getResult() {
        return result;
    }

    public void setResult(ExchangeRateDTO result) {
        this.result = result;
    }
}
