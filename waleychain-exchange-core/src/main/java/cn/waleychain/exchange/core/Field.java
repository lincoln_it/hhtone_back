package cn.waleychain.exchange.core;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

import cn.waleychain.exchange.core.utils.StringUtils;

/**   
 * @author chenx 
 * @date 2015年5月14日 下午3:09:03 
 * @version V1.0   
 * @Description: TODO(用一句话描述该文件做什么) 
 */
public class Field {

	/**
	 * 操作结果
	 */
	public final String result;
	
	/**
	 * 错误码， 当出现异常时才有此码
	 */
	public final int ret_code;
	
	/**
	 * 错误信息内容
	 */
	public final String ret_msg;
	
	public String getResult() {
		return result;
	}

	public int getRet_code() {
		return ret_code;
	}

	public String getRet_msg() {
		return ret_msg;
	}

	public Field(int ret_code, String ret_msg) {
		this.result = "failed";
		this.ret_code = ret_code;
		this.ret_msg = ret_msg;
	}
	
	public Field(String result, int ret_code, String ret_msg) {
		this.result = result;
		this.ret_code = ret_code;
		this.ret_msg = ret_msg;
	}
	
	@Override
	public String toString() {
		return "{'result' : '" + this.result + "', 'ret_code' : '" + this.ret_code + "', 'ret_msg' : '" + this.ret_msg + "'}";
	}
	
	public JSONObject toJson() {
		return JSONObject.parseObject(toString());
	}
	
	public JSONObject toJson(String key, Object value) {
		if (StringUtils.isBlank(key)) {
			return toJson();
		}
		
		String jsonObj = JSONObject.toJSONString(value);
		
		String json = "{'result' : '" + this.result + "', 'ret_code' : '" + this.ret_code + "', 'ret_msg' : '" + this.ret_msg + "', '" + key + "' : '" + jsonObj + "'}";
		return JSONObject.parseObject(json);
	}
	
	public JSONObject toJson(Object value) {
		return toJson(value, "");
	}
	
	public JSONObject toJson(Object value, String token) {
		
		String jsonObj = JSONObject.toJSONString(value);
		
		String json = "{'result' : '" + this.result + "', 'ret_code' : '" + this.ret_code + "', 'ret_msg' : '" + this.ret_msg + "', 'tokenId' : '" + token + "', 'datas' : '" + jsonObj + "'}";;
		return JSONObject.parseObject(json);
	}
	
	/**
	 * 转成json格式数据，主要用于分页查询
	 * @param value
	 * @param value2
	 * @param token 
	 * @return
	 */
	public JSONObject toJson(Object value, Object value2, String token) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", this.result);
		map.put("ret_code", this.ret_code);
		map.put("ret_msg", this.ret_msg);
		map.put("tokenId", token);
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("page", value2);
		map1.put("list", value);
		map.put("datas", map1);
		
		return (JSONObject) JSONObject.toJSON(map);
	}
	
	public boolean isEquals(Field waitField) {
		return this.ret_code == waitField.ret_code;
	}
}
