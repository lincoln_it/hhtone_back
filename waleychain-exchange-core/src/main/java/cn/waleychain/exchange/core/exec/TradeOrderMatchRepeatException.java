/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.exec;

import cn.waleychain.exchange.core.Field;

/**
 * @author chenx
 * @date 2018年4月21日 下午4:19:47
 * @version 1.0
 */
public class TradeOrderMatchRepeatException extends Exception {

	private static final long serialVersionUID = -6293046190181864580L;

	private int msgCode;
	
	public TradeOrderMatchRepeatException() {
	}

	public TradeOrderMatchRepeatException(Field field) {
		super(field.getRet_code() + ":" + field.getRet_msg());
		this.msgCode = field.getRet_code();
	}
	
	public TradeOrderMatchRepeatException(Field field, Object attrMsg) {
		super(field.getRet_code() + ":" + field.getRet_msg() + " " + attrMsg);
		this.msgCode = field.getRet_code();
	}
	
	public TradeOrderMatchRepeatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public TradeOrderMatchRepeatException(String message, Throwable cause) {
		super(message, cause);
	}

	public TradeOrderMatchRepeatException(String message) {
		super(message);
	}

	public TradeOrderMatchRepeatException(Throwable cause) {
		super(cause);
	}

	public int getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(int msgCode) {
		this.msgCode = msgCode;
	}
	
}
