/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.constant;

/**
 * 相关操作业务类型
 * @author chenx
 * @date 2018年4月18日 下午2:39:25
 * @version 1.0 
 */
public enum OperatePaymentType {

	/**
	 * 充人民币(recharge)
	 */
	TRADE_DETAIL_REMARK_TYPE_RECHARGE("recharge"),
	/**
	 * 钱包充值(recharge_into) 
	 */
    TRADE_DETAIL_REMARK_TYPE_RECHARGE_INTO("recharge_into"),
    /**
     * 奖励充值(bonus_into)
     */
    TRADE_DETAIL_REMARK_TYPE_BONUS_INFO("bonus_into"),
    /**
     * 奖励冻结
     */
    TRADE_DETAIL_REMARK_TYPE_BONUS_FREEZE("bonus_freeze"),
    /**
     * 人民币提现
     */
    TRADE_DETAIL_REMARK_TYPE_WITHDRAW_CNY("withdraw_cny"),
    /**
     * 提币冻结解冻(withdrawals)
     */
    TRADE_DETAIL_REMARK_TYPE_WITHDRAWALS("withdrawals"),
    /**
     * 提现审核通过(withdrawals_out) 
     */
    TRADE_DETAIL_REMARK_TYPE_WITHDRAWALS_OUT("withdrawals_out"),
    /**
     * 人民币提现手续费(withdraw_cny_poundage) 
     */
    TRADE_DETAIL_REMARK_TYPE_WITHDRAW_CNY_POUNDAGE("withdraw_cny_poundage"),
    /**
     * 提币手续费(withdrawals_poundage) 
     */
    TRADE_DETAIL_REMARK_TYPE_WITHDRAWALS_POUNDAGE("withdrawals_poundage"),
    /**
     * 下单(order_create) 
     */
    TRADE_DETAIL_REMARK_TYPE_ORDER_CREATE("order_create"),
    /**
     * 成交(order_turnover)
     */
    TRADE_DETAIL_REMARK_TYPE_ORDER_TURNOVER("order_turnover"),
    /**
     * 成交手续费(order_turnover_poundage)  
     */
    TRADE_DETAIL_REMARK_TYPE_ORDER_TURNOVER_POUNDAGE("order_turnover_poundage"),
    /**
     * 撤单(order_cancel)  
     */
    TRADE_DETAIL_REMARK_TYPE_ORDER_CANNCEL("order_cancel"),
    /**
     * 订单异常
     */
    TRADE_DETAIL_REMARK_TYPE_ORDER_EXCEPTION("order_exception"),
    /**
     * 注册奖励(bonus_register)
     */
    TRADE_DETAIL_REMARK_TYPE_BONUS_REGISTER("bonus_register"),
    /**
     * 交易奖励
     */
    TRADE_DETAIL_REMARK_TYPE_BONUS_DEAL("bonus_deal"),
    /**
     * 兑换(cny_btcx_exchange)
     */
    TRADE_DETAIL_REMARK_TYPE_CNY_BTCX_EXCHANGE("cny_btcx_exchange");

    private String remark;

    private OperatePaymentType(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return this.remark;
    }

    public String toString() {
        return this.remark;
    }
    
}
