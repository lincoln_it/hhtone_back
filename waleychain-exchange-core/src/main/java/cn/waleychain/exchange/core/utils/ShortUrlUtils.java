/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.utils;

import java.util.ArrayList;
import java.util.Random;

import cn.hutool.crypto.SecureUtil;

/**
 * 短链生成
 * @author chenx
 * @date 2017年7月6日 下午3:49:16
 * @version 1.0
 */
public class ShortUrlUtils {

	static String[] chars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "_", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	
	public static String[] shortUrl(String url) {
		// 可以自定义生成 MD5 加密字符传前的混合 KEY
		// 要使用生成 URL 的字符
		// 对传入网址进行 MD5 加密
		// String sMD5EncryptResult = (new CMyEncrypt()).getMD5OfStr(key + url);
		String sMD5EncryptResult = SecureUtil.md5(System.currentTimeMillis() + url);
		String hex = sMD5EncryptResult;

		String[] resUrl = new String[4];
		for (int i = 0; i < 4; i++) {

			// 把加密字符按照 8 位一组 16 进制与 0x3FFFFFFF 进行位与运算
			String sTempSubString = hex.substring(i * 8, i * 8 + 8);

			// 这里需要使用 long 型来转换，因为 Inteper .parseInt() 只能处理 31 位 , 首位为符号位 , 如果不用
			// long ，则会越界
			long lHexLong = 0x3FFFFFFF & Long.parseLong(sTempSubString, 16);
			String outChars = "";
			for (int j = 0; j < 6; j++) {
				// 把得到的值与 0x0000003D 进行位与运算，取得字符数组 chars 索引
				long index = 0x0000003D & lHexLong;
				// 把取得的字符相加
				outChars += chars[(int) index];
				// 每次循环按位右移 5 位
				lHexLong = lHexLong >> 5;
			}
			// 把字符串存入对应索引的输出数组
			resUrl[i] = outChars;
		}
		return resUrl;
	}
	
	/**
	 * 生成短链接
	 * @param urlPath
	 * @return
	 */
	public static String genShotUrl(String urlPath) {
		String[] aResult = shortUrl(urlPath);//将产生4组8位字符串
        
        Random random=new Random();
        int j = random.nextInt(4);//产成4以内随机数

        return aResult[j];//随机取一个作为短链
	}
	
	public static ArrayList<Integer> base62(long id) {  
	      
	    ArrayList<Integer> value = new ArrayList<Integer>();  
	    while (id > 0) {  
	        int remainder = (int) (id % 64);  
	        value.add(remainder);  
	        id = id / 64;  
	    }  
	      
	    return value;  
	}
	
	/**
	 * 根据序号生成短链接
	 * @param id
	 * @return
	 */
	public static String generateShortUrl(long id) {
		ArrayList<Integer> arr = base62(id);
		while (arr.size() < 4) {
			arr.add(0, 0);
		}
		
		String shortUrl = "";
		for (int i : arr) {
			shortUrl += chars[i];
		}
		
		return shortUrl;
	}
	
	public static void main(String[] args) {
		// 长连接： http://www.zuidaima.com/share/1550463378934784.htm
		// 新浪解析后的短链接为： http://t.cn/h1jGSC
		/*String sLongUrl = "http://www.zuidaima.com/share/1550463378934784.htm"; // 3BD768E58042156E54626860E241E999
//		String sLongUrl = "oCLXlw4kniTBSXFT03c0FeHahEaU"; // 3BD768E58042156E54626860E241E999
		String[] aResult = shortUrl(sLongUrl);
		// 打印出结果
		for (int i = 0; i < aResult.length; i++) {
			System.out.println("[" + i + "]:::" + aResult[i]);
		}*/
		
//		ArrayList<Integer> arr = base62(16777215);
		System.out.println(generateShortUrl(10000000000000L));
		
		/*long t1 = System.currentTimeMillis();
		for (int i = 1; i < 240000; i++) {
			System.out.println(generateShortUrl(i));
		}
		long t2 = System.currentTimeMillis();
		System.out.println(t2 - t1);*/
	}
}
