package cn.waleychain.exchange.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 最新成交价
 * @author chenx
 * @date 2018年4月23日 下午4:05:09
 * @version 1.0 
 */
public class NewPriceDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1886960651811124563L;

	/**
     * 交易对ID
     */
    private long marketId;

    /**
     * 最新成交价
     */
    private BigDecimal price;

    public long getMarketId() {
        return marketId;
    }

    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
