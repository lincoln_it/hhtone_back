package cn.waleychain.exchange.core.result;

import cn.waleychain.exchange.core.Field;

/**
 * @author chenx
 * @date 2017年3月15日
 * @version V1.0
 * @description: 
 */
public class RetResultCode {

	/**
	 * 操作成功
	 */
	public static final Field HANDLER_SUCCESS = new Field("success", 1, "操作成功");
	/**
	 * 操作失败
	 */
	public static final Field HANDLER_FIELED = new Field("failed", 0, "操作失败");
	
	
	/**
	 * 响应True
	 */
	public static final Field BOOLEAN_TRUE = new Field("true", 1, "是");
	/**
	 * 响应False
	 */
	public static final Field BOOLEAN_FALSE = new Field("false", 0, "否");
	
	
	
	/**
	 * token验证失败
	 */
	public static final Field E10001 = new Field(10001, "token验证失败");
	/**
	 * 登录超时
	 */
	public static final Field E10002 = new Field(10002, "登录超时");
	/**
	 * 用户验证失败
	 */
	public static final Field E10003 = new Field(10003, "用户验证失败");
	
	/**
	 * 参数错误
	 */
	public static final Field E11001 = new Field(11001, "参数错误");
	/**
	 * 手机号不合法
	 */
	public static final Field E11002 = new Field(11002, "手机号不合法");
	/**
	 * 身份证号不合法
	 */
	public static final Field E11003 = new Field(11003, "身份证号不合法");
	
	/**
	 * 手机验证码错误
	 * */
	public static final Field E12001 = new Field(12001, "手机验证码错误");
	/**
	 * 手机验证码已失效
	 * */
	public static final Field E12002 = new Field(12002, "手机验证码已失效");
	/**
	 * 手机验证码发送失败
	 * */
	public static final Field E12003 = new Field(12003, "手机验证码发送失败");
	/**
	 * 手机号已被绑定，请更换手机号
	 * */
	public static final Field E12004 = new Field(12004, "手机号已被绑定，请更换手机号");
	/**
	 * 验证码错误
	 * */
	public static final Field E12005 = new Field(12005, "验证码错误");
	
	/**
	 * 数据不存在
	 */
	public static final Field E13001 = new Field(13001, "数据不存在");
	/**
	 * 您无权操作此数据
	 */
	public static final Field E13002 = new Field(13002, "您无权操作此数据");
	/**
	 * 您没有此权限
	 */
	public static final Field E13003 = new Field(13003, "您没有此权限");
	/**
	 * 获取系统分布式锁失败，请稍后再试
	 */
	public static final Field E13004 = new Field(13004, "获取系统分布式锁失败，请稍后再试");
	
	/**
	 * 用户名或密码错误
	 */
	public static final Field E20001 = new Field(20001, "用户名或密码错误");
	/**
	 * 账号不存在
	 */
	public static final Field E20002 = new Field(20002, "账号不存在");
	/**
	 * 原密码错误
	 */
	public static final Field E20003 = new Field(20003, "原密码错误");
	/**
	 * 手机号与账号不符
	 */
	public static final Field E20004 = new Field(20004, "手机号与账号不符");
	/**
	 * 账号已被绑定
	 */
	public static final Field E20005 = new Field(20005, "账号已被绑定");
	/**
	 * 账号被冻结
	 */
	public static final Field E20006 = new Field(20006, "账号被冻结");
	/**
	 * 账号已存在
	 */
	public static final Field E20007 = new Field(20007, "账号已存在");
	/**
	 * 手机号已存在
	 */
	public static final Field E20008 = new Field(20008, "手机号已存在");
	/**
	 * 身份证号已存在
	 */
	public static final Field E20009 = new Field(20009, "身份证号已存在");
	/**
	 * 邀请码不存在
	 */
	public static final Field E20010 = new Field(20010, "邀请码不存在");
	/**
	 * 账户未设置支付密码
	 */
	public static final Field E20011 = new Field(20011, "账户未设置支付密码");
	/**
	 * 账户可用余额不足
	 */
	public static final Field E20012 = new Field(20012, "账户可用余额不足");
	/**
	 * 用户资金账户冻结
	 */
	public static final Field E20013 = new Field(20013, "用户资金账户冻结");
	/**
	 * 还未实名认证
	 */
	public static final Field E20014 = new Field(20014, "还未实名认证");
	/**
	 * 已实名认证过，不能重复认证
	 */
	public static final Field E20015 = new Field(20015, "已实名认证过，不能重复认证");
	/**
	 * 此证件号已实名认证过，不能重复使用
	 */
	public static final Field E20016 = new Field(20016, "此证件号已实名认证过，不能重复使用");
	/**
	 * 交易密码错误
	 */
	public static final Field E20017 = new Field(20017, "交易密码错误");
	/**
	 * 钱包账号已冻结
	 */
	public static final Field E20018 = new Field(20018, "钱包账号已冻结");
	/**
	 * 用户不存在
	 */
	public static final Field E20019 = new Field(20019, "用户不存在");
	/**
	 * 用户资金账号不存在
	 */
	public static final Field E20020 = new Field(20020, "用户资金账号不存在");
	
	/**
	 * 用户资金充值记录不存在
	 */
	public static final Field E20021 = new Field(20021, "用户资金充值记录不存在");
	/**
	 * 用户资金提现记录不存在
	 */
	public static final Field E20022 = new Field(20022, "用户资金提现记录不存在");
	
	/**
	 * 用户资金不足
	 */
	public static final Field E20023 = new Field(20023, "用户资金不足");
	/**
	 * 用户交易密码错误
	 */
	public static final Field E20024 = new Field(20024, "用户交易密码错误");
	/**
	 * 邮箱激活码不存在
	 */
	public static final Field E20025 = new Field(20025, "邮箱激活码不存在");
	
	/**
	 * 系统正在运行中请勿新增币种
	 */
	public static final Field E30001 = new Field(30001, "系统正在运行中请勿新增币种");
	/**
	 * 系统服务停止禁止交易
	 */
	public static final Field E30002 = new Field(30002, "系统服务停止禁止交易");
	/**
	 * 币种不存在
	 */
	public static final Field E30003 = new Field(30003, "币种不存在");
	/**
	 * 交易市场不存在
	 */
	public static final Field E30004 = new Field(30004, "交易市场不存在");
	/**
	 * 冻结资金不足
	 */
	public static final Field E30005 = new Field(30005, "冻结资金不足");
	/**
	 * 解冻资金不足
	 */
	public static final Field E30006 = new Field(30006, "解冻资金不足");
	/**
	 * 可用资金不足
	 */
	public static final Field E30007 = new Field(30007, "可用资金不足");
	
	/**
	 * 用户平台钱包账号不存在
	 */
	public static final Field E40001 = new Field(40001, "用户平台钱包账号不存在");
	/**
	 * 用户平台钱包账号冻结
	 */
	public static final Field E40002 = new Field(40002, "用户平台钱包账号冻结");
	
	/**
	 * 委托订单不存在
	 */
	public static final Field E50001 = new Field(50001, "委托订单不存在");
	
	/**
	 * 成交单重复
	 */
	public static final Field E50002 = new Field(50002, "成交单重复");
	
	/**
	 * 未找到匹配单
	 */
	public static final Field E50003 = new Field(50003, "未找到匹配单");
}
