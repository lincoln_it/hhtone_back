/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.utils;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import com.alibaba.fastjson.JSONObject;

import cn.waleychain.exchange.core.entity.QueryRateResultDTO;

public class RateUtils {

	/**
	 * 获取USD费率
	 * @return
	 */
	public static String getUsdRate() {
		
		HttpClient client = new HttpClient();
		String smsUrl = "http://api.nowapi.com/";
		String app = "finance.rate";
		String scur = "USD";
		String tcur = "CNY";
		String appkey = "31534";
		String sign = "52d31f348e8b42bcc6ee890dd52ed4e6";
		StringBuffer url = new StringBuffer();
		url.append(smsUrl).append("?")
				.append("app=").append(app).append("&")
				.append("scur=").append(scur).append("&")
				.append("tcur=").append(tcur).append("&")
				.append("appkey=").append(appkey).append("&")
				.append("sign=").append(sign);
		GetMethod getMethod = new GetMethod(url.toString());
		try {
			client.executeMethod(getMethod);
			String result = new String(getMethod.getResponseBodyAsString().getBytes("utf-8"));
			
			QueryRateResultDTO resultDTO = JSONObject.parseObject(result, QueryRateResultDTO.class);
			if (result != null && resultDTO.getResult() != null) {
				return resultDTO.getResult().getRate();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "6.7";
	}
	
}
