package cn.waleychain.exchange.core.utils;

import java.util.Calendar;
import java.util.Date;

public class CalendarUtils {
    public CalendarUtils() {
    }

    public static Date addDay(Date now, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    public static Date addMinute(Date now, int min) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MINUTE, min);
        return cal.getTime();
    }
}
