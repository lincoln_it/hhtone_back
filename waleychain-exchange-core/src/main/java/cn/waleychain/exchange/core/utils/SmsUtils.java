/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.utils;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

/**
 * 发送短信验证码
 * @author chenx
 * @date 2018年4月13日 下午1:48:00
 * @version 1.0
 */
public class SmsUtils {

	private static final String uId = "abcoin8";
	private static final String uKey = "b81b1d8ac61830a48326";
	private static final String url = "http://utf8.sms.webchinese.cn/";
	
	/**
	 * 发送验证码
	 * @param mobile
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public static boolean sendSms(String mobile, String content) throws Exception {
		
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(url);
		post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
		NameValuePair[] data = new NameValuePair[]{
				new NameValuePair("Uid", uId), 
				new NameValuePair("Key", uKey), 
				new NameValuePair("smsMob", mobile), 
				new NameValuePair("smsText", content)};
		
		post.setRequestBody(data);
		try {
			
			client.executeMethod(post);
			int statusCode = post.getStatusCode();
			post.releaseConnection();
			if (statusCode == 200) {
				return true;
			}
		} catch(Exception e) {
			throw new Exception("短信发送失败：" + e.getMessage());
		}
		
		return false;
	}
}
