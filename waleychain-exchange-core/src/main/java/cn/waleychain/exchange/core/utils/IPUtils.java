package cn.waleychain.exchange.core.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;


public class IPUtils {

	/**
	 * get localhost address
	 * 
	 * @return
	 */
	public static ArrayList<String> getLocalhost() {
		ArrayList<String> ipAddr = new ArrayList<>();
		try {
			Enumeration<NetworkInterface> interfaces = null;
			interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface ni = interfaces.nextElement();
				Enumeration<InetAddress> addresss = ni.getInetAddresses();
				while (addresss.hasMoreElements()) {
					InetAddress nextElement = addresss.nextElement();
					String hostAddress = nextElement.getHostAddress();
					ipAddr.add(hostAddress);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ipAddr;
	}
}
