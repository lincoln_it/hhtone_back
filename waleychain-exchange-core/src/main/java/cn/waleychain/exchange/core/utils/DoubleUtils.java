/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.utils;

import java.math.BigDecimal;

/**
 * double操作
 * @author chenx
 * @date 2017年8月24日 下午9:28:19
 * @version 1.0 
 */
public class DoubleUtils {

	/**
	 * @param number
	 *            要格式化的数字或者金额
	 * @param type
	 *            </br>
	 *            0: 总是在非 0 舍弃小数(即截断)之前增加数字 --- 例如：1.231 保留2位小数 = 1.24 </br>
	 *            1：从不在舍弃(即截断)的小数之前增加数字 ---- 例如：1.236 保留2位小数 = 1.23 </br>
	 *            4：四舍五入
	 * @return 处理金额
	 */
	public static double scaleNumber(Object number, int type) {
		BigDecimal returnNum = new BigDecimal(0.00);
		if (number != null && !"".equals(number)) {
			BigDecimal bDecimal = new BigDecimal(number.toString());
			/* 如果价钱是负数，则返回0元 */
			if (bDecimal.compareTo(returnNum) == 1) {
				String bdNum = bDecimal.setScale(2, type).toString();
				if (bdNum != null && !"".equals(bdNum)) {
					returnNum = new BigDecimal(bdNum.toString());
				}
			}
		}
		
		return returnNum.doubleValue();
	}
}
