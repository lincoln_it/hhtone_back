/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.core.constant;

public enum OperatePaymentRemark {

	/**
	 * 充值
	 */
	RECHARGE("充值"),
	/**
	 * 提现
	 */
	WITHDRAW("提现"),
	/**
	 * 冻结
	 */
	FREEZE("冻结"),
	/**
	 * 解冻
	 */
	UNFREEZE("解冻"),
	/**
	 * 转出
	 */
	OUT("转出"),
	/**
	 * 转入
	 */
	IN("转入");
	
	private String remark;
	
	OperatePaymentRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}
	
	
}
