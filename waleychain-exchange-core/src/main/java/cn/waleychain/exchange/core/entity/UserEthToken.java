package cn.waleychain.exchange.core.entity;

public class UserEthToken {
	
    private int coin_id;
    
    private String coin_name;

    private String contract_address;

    private int amount_unit;

    private int last_block;

    public int getCoin_id() {
        return coin_id;
    }

    public void setCoin_id(int coin_id) {
        this.coin_id = coin_id;
    }

    public String getCoin_name() {
        return coin_name;
    }

    public void setCoin_name(String coin_name) {
        this.coin_name = coin_name;
    }

    public String getContract_address() {
        return contract_address;
    }

    public void setContract_address(String contract_address) {
        this.contract_address = contract_address;
    }

    public int getAmount_unit() {
        return amount_unit;
    }

    public void setAmount_unit(int amount_unit) {
        this.amount_unit = amount_unit;
    }

    public int getLast_block() {
        return last_block;
    }

    public void setLast_block(int last_block) {
        this.last_block = last_block;
    }
}
