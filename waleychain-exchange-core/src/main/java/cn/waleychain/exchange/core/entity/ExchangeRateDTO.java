package cn.waleychain.exchange.core.entity;

/**
 * 查询美元兑换率返回数据对象
 * @author chenx
 * @date 2018年4月26日 上午10:11:17
 * @version 1.0
 */
public class ExchangeRateDTO {

    /**
     * 状态
     */
    private String status;

    /**
     * 基础币种
     */
    private String scur;

    /**
     * 兑换币种
     */
    private String tcur;

    /**
     * 兑换名称
     */
    private String ratenm;

    /**
     * 费率
     */
    private String rate;

    /**
     * 汇率更新时间
     */
    private String update;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScur() {
        return scur;
    }

    public void setScur(String scur) {
        this.scur = scur;
    }

    public String getTcur() {
        return tcur;
    }

    public void setTcur(String tcur) {
        this.tcur = tcur;
    }

    public String getRatenm() {
        return ratenm;
    }

    public void setRatenm(String ratenm) {
        this.ratenm = ratenm;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }
}
