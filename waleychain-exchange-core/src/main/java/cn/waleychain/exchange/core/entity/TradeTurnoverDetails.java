package cn.waleychain.exchange.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeTurnoverDetails implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -762175108868956444L;

	/**
	 * 成交均价
	 */
	private BigDecimal turnoverAveragePrice;
	
	/**
	 * 成交总数
	 */
	private BigDecimal turnoverTotalNum;

	public BigDecimal getTurnoverAveragePrice() {
		return turnoverAveragePrice;
	}

	public void setTurnoverAveragePrice(BigDecimal turnoverAveragePrice) {
		this.turnoverAveragePrice = turnoverAveragePrice;
	}

	public BigDecimal getTurnoverTotalNum() {
		return turnoverTotalNum;
	}

	public void setTurnoverTotalNum(BigDecimal turnoverTotalNum) {
		this.turnoverTotalNum = turnoverTotalNum;
	}
}
