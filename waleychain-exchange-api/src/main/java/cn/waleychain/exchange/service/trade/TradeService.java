/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.trade;

import java.math.BigDecimal;
import java.util.List;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.DealOrder;
import cn.waleychain.exchange.model.EntrustOrder;

public interface TradeService {

	/**
	 * 分页获取完成订单列表
	 * @param marketId
	 * @param userId
	 * @param type
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<DealOrder> fetchDealOrderPageList(Long marketId, String userName, Long dealId, Integer status, Integer type, Integer showCount, Integer currentPage) throws Exception;
	
	/**
	 * 分页获取委托订单列表
	 * @param marketId
	 * @param userId
	 * @param type
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<EntrustOrder> fetchEntrustOrderPageList(Long marketId, Long userId, Integer type, Integer status, Integer showCount, Integer currentPage) throws Exception;
	
	/**
	 * 创建委托订单
	 * @param userId
	 * @param marketId
	 * @param price
	 * @param num
	 * @param type
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	public boolean createEntrustOrder(Long userId, Long marketId, BigDecimal price, BigDecimal num, Integer type, String payPassword) throws Exception;
	
	/**
	 * 取消委托
	 * @param userId
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	public boolean cancelEntrustOrder(Long userId, Long orderId) throws Exception;
	
	/**
	 * 获取最近的委托买单
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<EntrustOrder> fetchRecentlyBuyEntrustList(Long marketId, int limit) throws Exception;
	
	/**
	 * 获取最近的委托卖单
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<EntrustOrder> fetchRecentlySellEntrustList(Long marketId, int limit) throws Exception;
	
	/**
	 * 获取最近成交的记录
	 * @param marketId
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<DealOrder> fetchRecentlyDealList(Long marketId, int limit) throws Exception;
	
	/**
	 * 存入委托单号
	 * @param orderId
	 * @throws Exception
	 */
	public void setTask(long orderId) throws Exception;
	
	/**
	 * 队列中取出委托单号
	 * @throws Exception
	 */
	public Long getTask() throws Exception;
	
	/**
	 * 交易订单撮合
	 * @throws Exception
	 */
	public void tradeOrderProcess() throws Exception;
}
