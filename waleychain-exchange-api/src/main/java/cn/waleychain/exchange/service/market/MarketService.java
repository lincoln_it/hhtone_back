/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.market;

import java.util.List;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.Market;

public interface MarketService {
	
	/**
	 * 分页获取市场列表
	 * @param name
	 * @param title
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<Market> fetchMarketPageList(String name, String title, Integer status, String transAreaId, int showCount, int currentPage) throws Exception;
	
	/**
	 * 新增市场
	 * @param market
	 * @return
	 * @throws Exception
	 */
	public boolean addMarket(Market market) throws Exception;
	
	/**
	 * 获取市场信息
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	public Market fetchMarketInfoById(Long marketId) throws Exception;
	
	/**
	 * 设置市场状态
	 * @param marketId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public boolean setMarketStatus(Long marketId, Integer status) throws Exception;
	
	/**
	 * 编辑修改市场信息
	 * @param market
	 * @return
	 * @throws Exception
	 */
	public boolean updateMarketInfo(Market market) throws Exception;
	
	/**
	 * 重置刷新缓存中市场列表
	 * @throws Exception
	 */
	public void resetCacheMarketList(boolean isForce) throws Exception;
	
	/**
	 * 从缓存中获取所有交易市场
	 * @return
	 * @throws Exception
	 */
	public List<Market> fetchCacheMarketAllList() throws Exception;
	
	/**
	 * 从缓存中获取验证后的交易市场列表（可用）
	 * @return
	 * @throws Exception
	 */
	public List<Market> fetchCacheMarketVaildList() throws Exception;
	
	/**
	 * 从缓存中获取市场信息
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	public Market fetchMarketByCache(long marketId) throws Exception;
	
	/**
	 * 通过币种获取可用的交易市场列表
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	public List<Market> fetchMarketListByCoinId(Long coinId) throws Exception;
}
