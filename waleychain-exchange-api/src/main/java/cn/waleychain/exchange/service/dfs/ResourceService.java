/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.dfs;

import java.util.List;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.FileResource;

public interface ResourceService {

	/**
	 * 获取文件资源列表
	 * @param name 名称
	 * @param type 类型（1 图片 2 文件）
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<FileResource> fetchResourcesList(String name, Integer type, int showCount, int currentPage) throws Exception;
	
	/**
	 * 批量插入文件
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public boolean batchInsert(List<FileResource> list) throws Exception;
}
