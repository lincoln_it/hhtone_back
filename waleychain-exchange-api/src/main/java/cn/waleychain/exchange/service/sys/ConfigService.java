/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.sys;

import java.util.List;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.DictionaryInfo;

public interface ConfigService {

	/**
	 * 获取配置类型
	 * @return
	 * @throws Exception
	 */
	public List<DictionaryInfo> fetchConfigType() throws Exception;
	
	/**
	 * 新增配置
	 * @param dictionary
	 * @return
	 * @throws Exception
	 */
	public boolean addSystemConfig(Long type, 
			String name, String code, String value) throws Exception;
	
	/**
	 * 修改配置信息
	 * @param dictionary
	 * @return
	 * @throws Exception
	 */
	public boolean updateSystemConfig(Long idNo, 
			String name, 
			String value, int status, String remark) throws Exception;
	
	/**
	 * 获取系统配置值
	 * @param configCode
	 * @return
	 * @throws Exception
	 */
	public String fetchSystemConfigValue(String configCode) throws Exception;
	
	/**
	 * 获取系统配置信息
	 * @param configCode
	 * @return
	 * @throws Exception
	 */
	public DictionaryInfo fetchSystemConfigInfo(String configCode) throws Exception;
	
	/**
	 * 获取业务配置列表
	 * @param type 类型（1 系统 2 业务 3 交易区域）
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<DictionaryInfo> fetchConfigList(Long type, int showCount, int currentPage) throws Exception;
	
}
