/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.account;

import java.util.List;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.UserBank;
import cn.waleychain.exchange.model.UserInfo;

public interface UserService {

	/**
	 * 获取系统用户userId
	 * @return
	 */
	public long fetchAdminUserId();
	
	/**
	 * 新增用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public UserInfo addUserInfo(UserInfo user) throws Exception;
	
	/**
	 * 用户有效注册
	 * @param email
	 * @param password
	 * @param invitionCode
	 * @return
	 * @throws Exception
	 */
	public UserInfo emailRgstry(String email, String password, String invitionCode) throws Exception;
	
	/**
	 * 邮箱注册验证
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public boolean activateAccount(String code) throws Exception;
	
	/**
	 * 分页搜索获取用户列表
	 * @param userId
	 * @param phoneNum
	 * @param keyword
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<UserInfo> fetchUserPageList(Long userId, String phoneNum, String keyword, Integer status, int showCount, int currentPage) throws Exception;
	
	/**
	 * 获取用户信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public UserInfo fetchUserInfo(Long userId) throws Exception;
	
	/**
	 * 编辑用户信息
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public boolean modifyUserInfo(UserInfo user) throws Exception;
	
	/**
	 * 验证手机号是否已注册
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public boolean checkMobileIsExist(String mobile) throws Exception;
	
	/**
	 * 验证邮箱是否已注册
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public boolean checkEmailIsExist(String email) throws Exception;
	
	/**
	 * 用户登录
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public UserInfo login(String mobile, String password) throws Exception;
	
	/**
	 * 通过手机号设置登录密码
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public boolean modifyPassByMobile(String mobile, String password) throws Exception;
	
	/**
	 * 修改密码
	 * @param userId
	 * @param oldPassword
	 * @return
	 * @throws Exception
	 */
	public boolean modifyPassword(Long userId, String password, String oldPassword) throws Exception;
	
	/**
	 * 修改资金密码
	 * @param userId
	 * @param isSetPayPassword
	 * @param password
	 * @param oldPassword
	 * @return
	 * @throws Exception
	 */
	public boolean modifyPayPassword(Long userId, Integer isSetPayPassword, String password, String oldPassword) throws Exception;
	
	/**
	 * 通过手机号设置资金密码
	 * @param mobile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public boolean modifyPayPassByMobile(String mobile, String password) throws Exception;
	
	/**
	 * 用户绑定银行卡
	 * @param userBank
	 * @return
	 * @throws Exception
	 */
	public boolean addUserBankCard(UserBank userBank) throws Exception;
	
	/**
	 * 判断用户是否实名认证
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public boolean checkUserIsRealNameAuth(Long userId) throws Exception;
	
	/**
	 * 用户身份认证
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public boolean userIdentifyAuth(UserInfo user) throws Exception;
	
	/**
	 * 获取用户的银行卡列表
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public List<UserBank> fetchBankListByUserId(Long userId) throws Exception;
	
	/**
	 * 验证用户支付密码
	 * @param userId
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	public boolean checkUserPayPassword(Long userId, String payPassword) throws Exception;
}
