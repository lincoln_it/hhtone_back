/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.trade;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.waleychain.exchange.model.DealOrder;
import cn.waleychain.exchange.model.EntrustOrder;

/**
 * 缓存数据存储
 * @author chenx
 * @date 2018年4月24日 上午10:30:18
 * @version 1.0 
 */
public interface CacheDataService {
	
	/**
	 * 根据交易对ID获取最新50笔成交记录，并保存到内存中
	 */
	public void resetTurnoverOrderList() throws Exception;
	
	/**
	 * 将最新委托档位保存到内存中
	 */
	public void resetNewestEntrustOrderList() throws Exception;
	
	/**
	 * 将最新交易单放入缓存
	 * @param marketId
	 * @param dealOrderList
	 * @throws Exception
	 */
	public void setTurnoverOrder2Cache(long marketId, List<DealOrder> dealOrderList) throws Exception;
	
	/**
	 * 将最新的委托买单放入缓存
	 * @param marketId
	 * @param buyOrderList
	 * @throws Exception
	 */
	public void setEntrustBuyOrder2Cache(long marketId, List<EntrustOrder> buyOrderList) throws Exception;
	
	/**
	 * 将最新的委托卖单放入缓存
	 * @param marketId
	 * @param sellOrderList
	 * @throws Exception
	 */
	public void setEntrustSellOrder2Cache(long marketId, List<EntrustOrder> sellOrderList) throws Exception;
	
	/**
	 * 取最新交易单
	 * @param marketId
	 * @param dealOrderList
	 * @throws Exception
	 */
	public List<DealOrder> getTurnoverOrder2Cache(long marketId) throws Exception;
	
	/**
	 * 取最新的委托买单
	 * @param marketId
	 * @param buyOrderList
	 * @throws Exception
	 */
	public List<EntrustOrder> getEntrustBuyOrder2Cache(long marketId) throws Exception;
	
	/**
	 * 取最新的委托卖单
	 * @param marketId
	 * @param sellOrderList
	 * @throws Exception
	 */
	public List<EntrustOrder> getEntrustSellOrder2Cache(long marketId) throws Exception;
	
	/**
	 * 缓存最新价
	 * @throws Exception
	 */
	public void setNewPrice2Cache() throws Exception;
	
	/**
	 * 获取最新价
	 * @throws Exception
	 */
	public BigDecimal getNewPrice2Cache(long marketId) throws Exception;
	
	/**
	 * 将k线数据放入缓存
	 * @param kLines
	 * @throws Exception
	 */
	public void setKLines2Cache(long marketId, Map<String, List<String>> kLines) throws Exception;
	
	/**
	 * 获取k线数据
	 * @param marketId
	 * @return
	 * @throws Exception
	 */
	public Map<String, List<String>> getKLines2Cache(long marketId) throws Exception;
	
}
