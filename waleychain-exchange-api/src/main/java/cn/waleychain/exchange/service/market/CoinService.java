/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.market;

import java.util.List;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.CoinInfo;

public interface CoinService {

	/**
	 * 获取btcx币标识ID
	 * @return
	 */
	public long fetchBtcxCoinId();
	
	/**
	 * 分页获取币种列表
	 * @param name
	 * @param title
	 * @param coinType
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<CoinInfo> fetchCoinPageList(String name, String title, Integer coinType, Integer status, int showCount, int currentPage) throws Exception;
	
	/**
	 * 新增币种
	 * @param coin
	 * @return
	 * @throws Exception
	 */
	public boolean addCoinInfo(CoinInfo coin) throws Exception;
	
	/**
	 * 获取币种信息
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	public CoinInfo fetchCoinById(Long coinId) throws Exception;
	
	/**
	 * 设置币种状态
	 * @param coinId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public boolean setCoinStatus(Long coinId, Integer status) throws Exception;
	
	/**
	 * 编辑修改币种信息
	 * @param coin
	 * @return
	 * @throws Exception
	 */
	public boolean updateCoinInfo(CoinInfo coin) throws Exception;
	
	/**
	 * 重置刷新币种到缓存中
	 * @param isForce
	 * @throws Exception
	 */
	public void resetCacheCoinList(boolean isForce) throws Exception;
	
	/**
	 * 从缓存中获取所有币种
	 * @return
	 * @throws Exception
	 */
	public List<CoinInfo> fetchCacheCoinAllList() throws Exception;
	
	/**
	 * 从缓存中获取验证后币种（可用）
	 * @return
	 * @throws Exception
	 */
	public List<CoinInfo> fetchCacheCoinVaildList() throws Exception;
	
	/**
	 * 获取币种列表
	 * @param coinType 币种类型
	 * @param walletType 钱包类型
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public List<CoinInfo> fetchCoinList(Integer coinType, Integer walletType, Integer status) throws Exception;
	
}
