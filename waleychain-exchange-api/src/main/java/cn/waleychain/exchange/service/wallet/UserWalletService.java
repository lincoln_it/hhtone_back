/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.wallet;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.UserWallet;

public interface UserWalletService {

	/**
	 * 添加用户个人钱包
	 * @param userId
	 * @param coinId
	 * @param name
	 * @param address
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	public boolean addUserWallet(Long userId, Long coinId, String name, String address, String payPassword) throws Exception;
	
	/**
	 * 获取钱包信息
	 * @param userWalletId
	 * @return
	 * @throws Exception
	 */
	public UserWallet fetchUserWalletInfo(Long userWalletId) throws Exception;
	
	/**
	 * 分页获取用户个人钱包列表
	 * @param name
	 * @param coinId
	 * @param userId
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<UserWallet> fetchUserWalletPageList(String name, Long coinId, Long userId, int showCount, int currentPage) throws Exception;
	
	/**
	 * 编辑钱包信息
	 * @param userWalletId
	 * @param userId
	 * @param name
	 * @param address
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	public boolean updateUserWallet(Long userWalletId, Long userId, String name, String address, String payPassword) throws Exception;
	
	/**
	 * 删除用户个人钱包
	 * @param userWalletId
	 * @param userId
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	public boolean delete(Long userWalletId, Long userId, String payPassword) throws Exception;
}
