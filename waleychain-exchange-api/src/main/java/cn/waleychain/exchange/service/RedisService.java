/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis缓存操作服务接口
 * 
 * @author chenx
 * @date 2018年2月6日 上午11:41:08
 * @version 1.0
 */
public interface RedisService {

	/**
	 * 查看缓存中是否存在对应KEY
	 * @param cacheName
	 * @param key
	 * @return
	 */
	public Boolean hasKey(String cacheName, String key);

	/**
	 * 往redis中存值，使用默认过期时间
	 * @param key
	 * @param value
	 * @return
	 */
    public void set(String cacheName, String key, Object val);

    /**
	 * 往redis中存值，自定义过期时间
	 * @param key
	 * @param value
	 * @return
	 */
    public void set(String cacheName, String key, Object val, Long expires, TimeUnit timeUnit);

    /**
     * 删除缓存数据
     * @param cacheName
     * @param key
     */
    public void delete(String cacheName, String key);

    /**
	 * 从redis中取值
	 * @param key
	 * @return
	 */
    public Object get(String cacheName, String key);
    
    /**
     * 获取分布式锁
     * @param prefix 锁前缀
     * @param key 锁名
     * @param value 锁值
     * @param waitLock 是否等待锁
     * @return
     */
    public boolean lock(String prefix, String key, String value, boolean waitLock);
    
    /**
     * 获取分布式锁，key为锁值
     * @param prefix 锁前缀
     * @param key 锁名
     * @param waitLock 是否等待锁
     * @return
     */
    public boolean lock(String prefix, String key, boolean waitLock);
	
    /**
     * 释放分布式锁
     * @param prefix 锁前缀
     * @param key 锁名
     * @return
     */
    public void unlock(String prefix, String key);
    
    /**
     * 在key 对应 list 的尾部添加字符串元素
     * @param cacheName
     * @param key
     * @param val
     */
    public void rpush(String cacheName, String key, Object val);
    
    /**
     * 在key 对应 list的头部添加字符串元素
     * @param cacheName
     * @param key
     * @param val
     * @param expires
     * @param timeUnit
     */
    public void lpush(String cacheName, String key, Object val);
    
    /**
     * 移除并返回列表 key 的头元素
     * @param cacheName
     * @param key
     * @return
     */
    public String lpop(String cacheName, String key);
    
    public String rpop(String cacheName, String key);
    
    /**
     * 获取哈希的keys
     * @param cacheName
     * @return
     */
    public Set<String> hkey(String cacheName);
    
    /**
     * 哈希获取数据
     * @param cacheName
     * @param key
     * @return
     */
    public String hget(String cacheName, String key);
    
    /**
     * 哈希 添加
     * @param cacheName
     * @param key
     * @param val
     */
    public void hset(String cacheName, String key, Object val);
    
    public List<String> lrange(String cacheName, String key, long start, long end);
    
    public long llen(String cacheName, String key);
    
    public void lset(String cacheName, String key, Object val);
    
    public Boolean expire(String cacheName, String key, final long timeout, final TimeUnit unit);

	void hdel(String cacheName, String key);
}
