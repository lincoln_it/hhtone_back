/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.trade;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import cn.waleychain.exchange.core.constant.KLineDataType;
import cn.waleychain.exchange.core.entity.KLineUser;
import cn.waleychain.exchange.core.entity.MarketResponseDTO;
import cn.waleychain.exchange.core.entity.TradeDataDesc;
import cn.waleychain.exchange.core.entity.TradeNewest;
import cn.waleychain.exchange.model.TradeData;

public interface TradeDataService {

	/**
	 * 获取缓存数据
	 * @throws Exception
	 */
	public void syncGetData() throws Exception;
	
	/**
	 * 数据同步
	 */
	public void syncPutData() throws Exception;
	
	/**
	 * 加入数据
	 * @param tradeData
	 */
	public void addData(TradeData tradeData);
	
	/**
	 * 获取数据
	 * @param marketId
	 * @param dataType
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<TradeDataDesc> getData(long marketId, KLineDataType dataType, int start, int limit);

	/**
	 * 重置数据列表
	 * @param marketId
	 * @param dataType
	 */
	public void resetDataList(long marketId, KLineDataType dataType);

	/**
	 * 根据市场Id 和 日期类型获取图表数据
	 * @param marketId
	 * @param dataType
	 * @return
	 * @throws Exception
	 */
	public String getDataDesc(long marketId, KLineDataType dataType) throws Exception;

	/**
	 * 获取最新委托单
	 * @param tradeDesc
	 * @return
	 * @throws Exception
	 */
	public TradeNewest getNewest(String tradeDesc) throws Exception;
	
	public List<TradeNewest> getNewData(long marketId, KLineDataType dataType, int limit) throws Exception ;
	
	/**
	 * 放入访问k线数据用户到缓存
	 * @param sessionId
	 * @param marketId
	 * @param dataType
	 * @return
	 * @throws Exception
	 */
	public KLineUser resetKLineUser(String sessionId, Long marketId, KLineDataType dataType, int kLimit) throws Exception;
	
	
	public List<KLineUser> fetchKLineUserList() throws Exception;
	
	/**
	 * 获取首页交易市场数据
	 * @param areaId
	 * @return
	 * @throws Exception
	 */
	public List<MarketResponseDTO> fetchTradeMarketByAreaId(@RequestParam String areaId) throws Exception;
}
