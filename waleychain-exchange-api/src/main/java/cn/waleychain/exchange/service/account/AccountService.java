/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.account;

import java.math.BigDecimal;
import java.util.Date;

import cn.waleychain.exchange.core.constant.OperatePaymentType;
import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.AccountRecharge;
import cn.waleychain.exchange.model.AccountWithdraw;
import cn.waleychain.exchange.model.UserAccount;

public interface AccountService {

	/**
	 * 分页获取用户资金账号列表
	 * @param userName
	 * @param mobile
	 * @param status
	 * @param begin
	 * @param end
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<UserAccount> fetchAccountPageList(String userName, String realName, String mobile, Integer status, 
			BigDecimal begin, BigDecimal end, int showCount, int currentPage) throws Exception;
	
	/**
	 * 获取资金账号信息
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	public UserAccount fetchAccountInfo(Long accountId) throws Exception;
	
	/**
	 * 分页获取资金账号提现记录
	 * @param userName
	 * @param realName
	 * @param beginDate
	 * @param endDate
	 * @param status
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<AccountWithdraw> fetchAccountWithdrowPageList(String userName, String realName, Date beginDate, Date endDate, Integer status, int showCount, int currentPage) throws Exception;
	
	/**
	 * 分页获取资金账号充值记录
	 * @param userName
	 * @param realName
	 * @param beginDate
	 * @param endDate
	 * @param status
	 * @param tradeNo
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<AccountRecharge> fetchAccountRechargePageList(String userName, String realName, Date beginDate, Date endDate, Integer status, String tradeNo, int showCount, int currentPage) throws Exception;
	
	/**
	 * 设置提现记录状态
	 * @param idNo
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public boolean setAccountWithdrawStatus(Long idNo, Integer status, String remark) throws Exception;
	
	/**
	 * 设置充值记录状态
	 * @param idNo
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public boolean setAccountRechargeStatus(Long idNo, Integer status, String remark) throws Exception;
	
	/**
	 * 资金账户提现
	 * @param accountWithdraw
	 * @return
	 * @throws Exception
	 */
	public boolean addAccountWithdraw(AccountWithdraw accountWithdraw, Long userId, 
			String payPassword) throws Exception;
	
	/**
	 * 资金账户充值
	 * @param accountRecharge
	 * @return
	 * @throws Exception
	 */
	public AccountRecharge addAccountRecharge(Long userId, BigDecimal amount, Integer payType) throws Exception;
	
	public boolean freezeAmount(Long accountId, BigDecimal freezeAmount, OperatePaymentType paymentType, Long orderId) throws Exception;
	
	public boolean unfreezeAmount(Long accountId, BigDecimal freezeAmount, OperatePaymentType paymentType, Long orderId) throws Exception;
	
	public boolean transferAmount(long reduceAccountId, long addAccountId, BigDecimal increment, OperatePaymentType paymentType, long orderId) throws Exception;
	
	public boolean withdrawalsAmount(long accountId, BigDecimal increment, OperatePaymentType paymentType) throws Exception;
	
	/**
	 * 通过用户ID获取用户资金账号信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public UserAccount fetchAccountByUserId(Long userId) throws Exception;
}
