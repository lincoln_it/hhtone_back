/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.sys;

import cn.waleychain.exchange.model.Sms;

public interface CommonService {
	
	public Sms sendSms(String mobile, String content) throws Exception;
	
	/**
	 * 添加短信验证码
	 * @param sms
	 * @return
	 * @throws Exception
	 */
	public boolean addMobileCode(Sms sms, String code) throws Exception;
	
	/**
	 * 验证手机短信验证码
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public boolean checkSmsVaildateCode(String mobile, String code) throws Exception;
	
}
