/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.service.wallet;

import java.math.BigDecimal;
import java.util.Date;

import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.model.CoinInfo;
import cn.waleychain.exchange.model.CoinWallet;
import cn.waleychain.exchange.model.CoinWalletChange;
import cn.waleychain.exchange.model.WalletCollectTask;
import cn.waleychain.exchange.model.WalletRecharge;
import cn.waleychain.exchange.model.WalletWithdraw;

public interface CoinWalletService {

	public boolean insertUserCoinWallet(CoinWallet wallet) throws Exception;
	
	/**
	 * 批量添加用户的币种钱包
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	public boolean batchInsertUserCoinWallet(Long coinId) throws Exception;
	
	/**
	 * 刷新初始化钱包币
	 * @param coin
	 * @throws Exception
	 */
	public void refreshAcceptor(CoinInfo coin) throws Exception;
	
	/**
	 * 获取平台钱包账号列表
	 * @param userId
	 * @param keyword
	 * @return
	 * @throws Exception
	 */
	public PageInfo<CoinWallet> fetchCoinWalletList(Long userId, 
			Long coinId,
			String mobile,
			String address,
			Integer status,
			Integer orderColumn,
			Integer orderType,
			Integer showCount,
			Integer currentPage) throws Exception;
	
	/**
	 * 获取平台钱包账号信息
	 * @param userId
	 * @param coinId
	 * @return
	 * @throws Exception
	 */
	public CoinWallet fetchCoinWalletInfo(Long userId, Long coinId) throws Exception;
	
	/**
	 * 用户平台钱包提现申请
	 * @param userId
	 * @param coinWalletId 平台钱包
	 * @param userWalletId 用户个人钱包
	 * @param amount 提币个数
	 * @param mobileCode
	 * @param payPassword
	 * @return
	 * @throws Exception
	 */
	public boolean userWithdrawApply(Long userId, Long coinWalletId, Long userWalletId, BigDecimal amount, String mobileCode, String payPassword) throws Exception;
	
	/**
	 * 设置钱包提现状态
	 * @param idNo
	 * @param status
	 * @param remark
	 * @return
	 * @throws Exception
	 */
	public boolean setWelletWithdrawStatus(Long idNo, Integer status, String remark) throws Exception;
	
	/**
	 * 钱包账户资金冻结
	 * @param coinWalletId
	 * @param freezeAmount
	 * @param paymentType 业务类型
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	public boolean walletFreezeAmount(Long userId, Long coinId, BigDecimal freezeAmount, String paymentType, Long orderId) throws Exception;
	
	/**
	 * 钱包账户资金解冻
	 * @param coinWalletId
	 * @param freezeAmount
	 * @param paymentType 业务类型
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	public boolean walletUnfreezeAmount(Long userId, Long coinId, BigDecimal freezeAmount, String paymentType, Long orderId) throws Exception;
	
	/**
	 * 更新钱包账户余额
	 * @param reduceWalletId 支出账户的用户ID
	 * @param addWalletId 收入账户的用户ID
	 * @param coinId 币种
	 * @param increment 金额/数量
	 * @param paymentType 业务类型
	 * @param orderId 关联订单
	 * @return
	 * @throws Exception
	 */
	public boolean transferAmount(long reduceWalletId, long addWalletId, long coinId, BigDecimal increment,
			String paymentType, long orderId) throws Exception;
	
	/**
	 * 给用户钱包账号提现
	 * @param coinWalletId 账号ID
	 * @param coinId 币种ID
	 * @param increment 提现金额
	 * @param paymentType 业务类型
	 * @return
	 * @throws Exception
	 */
	public boolean withdrawalsAmount(long coinWalletId, long coinId, BigDecimal increment, String paymentType) throws Exception;
	
	/**
	 * 用户撤销提现
	 * @param idNo
	 * @return
	 * @throws Exception
	 */
	public boolean userCancelWithdraw(Long idNo) throws Exception;
	
	/**
	 * 给用户打币成功，更新用户提币状态
	 * @param idNo
	 * @param txId
	 * @return
	 * @throws Exception
	 */
	public boolean sendCoinSuccess(Long idNo, String txId) throws Exception;
	
	/**
	 * 给用户钱包账号充值
	 * @param coinWalletId 账号ID
	 * @param increment 提现金额
	 * @param paymentType 业务类型
	 * @return
	 * @throws Exception
	 */
	public boolean rechargeAmount(long userId, long coinId, BigDecimal increment, String paymentType) throws Exception;
	
	
	/**
	 * 分页获取钱包提币记录
	 * @param userName
	 * @param status
	 * @param coinId
	 * @param mobile
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<WalletWithdraw> fetchWalletWithdrawPageList(Long coinWalletId, String userName, Integer status, Long coinId, String mobile, Date beginDate, Date endDate, int showCount, int currentPage) throws Exception;
	
	/**
	 * 分页获取钱包充币记录
	 * @param userName
	 * @param status
	 * @param coinId
	 * @param mobile
	 * @param showCount
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	public PageInfo<WalletRecharge> fetchWalletRechargePageList(Long coinWalletId, String userName, Integer status, Long coinId, String mobile, Date beginDate, Date endDate, int showCount, int currentPage) throws Exception;
	
	/**
	 * 通过交易ID和地址获取充值信息
	 * @param txId
	 * @param address
	 * @return
	 * @throws Exception
	 */
	public WalletRecharge fetchWalletRechargeByTxIdAndAddress(String txId, String address) throws Exception;
	
	/**
	 * 创建钱包任务
	 * @param task
	 * @return
	 * @throws Exception
	 */
	public boolean createWalletCollectTask(WalletCollectTask task) throws Exception;
	
	/**
	 * 通过币种和地址获取钱包信息
	 * @param coinId
	 * @param addr
	 * @return
	 * @throws Exception
	 */
	public CoinWallet fetchWalletByCoinIdAndAddr(Long coinId, String addr) throws Exception;
	
	/**
	 * 添加充值记录
	 * @param recharge
	 * @param change
	 * @return
	 * @throws Exception
	 */
	public boolean addWalletRecharge(WalletRecharge recharge, CoinWalletChange change) throws Exception;
	
	/**
	 * 冻结解冻账号
	 * @param coinWalletId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public boolean freezeAndUnfreezeWallet(Long coinWalletId, Integer status) throws Exception;
	
}
