package cn.waleychain.exchange.server.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author chenx
 * @date 2018年4月10日 下午2:46:08
 * @version 1.0
 */
@SpringBootApplication
@EnableEurekaServer
public class RegistryApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(RegistryApplication.class, args);
	}
}
