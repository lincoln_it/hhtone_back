/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.consumer.controller.sys;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.consumer.controller.BaseController;
import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.result.DefaultResult;
import cn.waleychain.exchange.core.result.IBaseResult;
import cn.waleychain.exchange.model.Announcement;
import cn.waleychain.exchange.model.DictionaryInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(description = "系统公共相关接口")
@RestController
@RequestMapping("api/v1/sys")
public class SystemController extends BaseController {

	private static final Logger mLog = LoggerFactory.getLogger(SystemController.class);
	
	
	@RequestMapping(value = "/fetchBannerList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取系统中banner", notes = "获取系统中banner，URL: http://{ip}:{port}/{projectName}/api/v1/sys/fetchBannerList", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchBannerList (
			@RequestHeader String Authorization) throws Exception {

		try {
			
			List<DictionaryInfo> list = new ArrayList<>();

			return DefaultResult.buildSuccessResult(list);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取系统中banner");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/fetchNoticeList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取公告列表", notes = "获取公告列表，URL: http://{ip}:{port}/{projectName}/api/v1/sys/fetchNoticeList", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchNoticeList (
			@RequestHeader String Authorization) throws Exception {

		try {

			List<Announcement> list = new ArrayList<>();
			
			return DefaultResult.buildSuccessResult(list);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取公告列表");
		}

		return DefaultResult.buildFailedResult();
	}
	
}
