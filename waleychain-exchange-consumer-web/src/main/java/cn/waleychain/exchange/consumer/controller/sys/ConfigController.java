/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.consumer.controller.sys;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.consumer.controller.BaseController;
import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.result.DefaultResult;
import cn.waleychain.exchange.core.result.IBaseResult;
import cn.waleychain.exchange.core.utils.StringUtils;
import cn.waleychain.exchange.feign.ConfigServiceFeign;
import cn.waleychain.exchange.model.DictionaryInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(description = "系统配置相关接口")
@RestController
@RequestMapping("api/v1/cfg")
public class ConfigController extends BaseController {

	private static final Logger mLog = LoggerFactory.getLogger(ConfigController.class);
	
	@Autowired
	private ConfigServiceFeign configFeign;
	
	@RequestMapping(value = "/fetchConfigType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取系统配置类型", notes = "获取系统配置类型，URL: http://{ip}:{port}/{projectName}/api/v1/cfg/fetchConfigType", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchConfigType (
			@RequestHeader String Authorization) throws Exception {

		try {
			
			List<DictionaryInfo> list = configFeign.fetchConfigType();

			return DefaultResult.buildSuccessResult(list);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取系统配置类型");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/fetchConfigList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取系统业务配置列表", notes = "获取系统业务配置列表，URL: http://{ip}:{port}/{projectName}/api/v1/cfg/fetchConfigList", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchConfigList (
			@ApiParam(value = "类型id", required = false) @RequestParam(required = false) Long typeId,
			@ApiParam(value = "配置码(sys 系统 biz 业务 transArea 交易区域)", required = false) @RequestParam(required = false) String configCode,
			@ApiParam(value = "每次查询多少条数据", required = true) @RequestParam(required = true, defaultValue = "-1") Integer showCount, 
			@ApiParam(value = "当前查询页", required = true) @RequestParam(required = true, defaultValue = "1") Integer currentPage, 
			@RequestHeader String Authorization) throws Exception {

		try {
			
			if ((typeId == null || typeId == 0L) && StringUtils.isNotBlank(configCode)) {
				List<DictionaryInfo> list = configFeign.fetchConfigType();
				for (DictionaryInfo d : list) {
					if (configCode.equalsIgnoreCase(d.getCode())) {
						typeId = d.getIdNo();
						break;
					}
				}
			}
			
			PageInfo<DictionaryInfo> page = configFeign.fetchConfigList(typeId, showCount, currentPage);

			return DefaultResult.buildSuccessResult(page);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取系统业务配置列表");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取配置信息内容", notes = "获取配置信息内容，URL: http://{ip}:{port}/{projectName}/api/v1/cfg/info", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchConfigInfo (
			@ApiParam(value = "配置码", required = true) @RequestParam String configCode,
			@RequestHeader String Authorization) throws Exception {

		try {

			DictionaryInfo info = configFeign.fetchConfigInfo(configCode);
			
			return DefaultResult.buildSuccessResult(info);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取配置信息内容");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@ApiOperation(value = "新增配置", notes = "新增配置，URL: http://{ip}:{port}/{projectName}/api/v1/cfg/new", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult addConfig (
			@ApiParam(value = "配置名称", required = true) @RequestParam String name,
			@ApiParam(value = "配置配型", required = true) @RequestParam Long typeId,
			@ApiParam(value = "配置码", required = true) @RequestParam String code,
			@ApiParam(value = "配置值", required = true, defaultValue = "1") @RequestParam(defaultValue = "1") String value,
			@RequestHeader String Authorization) throws Exception {

		try {

			boolean result = configFeign.addConfig(typeId, name, code, value);
			
			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "新增配置");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/modify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@ApiOperation(value = "编辑配置信息", notes = "编辑配置信息，URL: http://{ip}:{port}/{projectName}/api/v1/cfg/modify", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult addConfig (
			@ApiParam(value = "标识ID", required = true) @RequestParam Long idNo,
			@ApiParam(value = "配置名称", required = true) @RequestParam String name,
			@ApiParam(value = "配置值", required = true) @RequestParam String value,
			@ApiParam(value = "配置描述", required = true) @RequestParam String remark,
			@RequestHeader String Authorization) throws Exception {

		try {
			
			boolean result = configFeign.modify(idNo, name, value, 1, remark);
			
			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "编辑配置信息");
		}

		return DefaultResult.buildFailedResult();
	}
}
