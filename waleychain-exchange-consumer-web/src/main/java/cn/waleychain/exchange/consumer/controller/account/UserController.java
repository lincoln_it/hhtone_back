/**
 * Copyright (c) 2018, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.consumer.controller.account;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.consumer.controller.BaseController;
import cn.waleychain.exchange.core.constant.DDIC;
import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.result.DefaultResult;
import cn.waleychain.exchange.core.result.IBaseResult;
import cn.waleychain.exchange.core.result.RetResultCode;
import cn.waleychain.exchange.core.utils.SequenceUtils;
import cn.waleychain.exchange.core.vaildate.VaildateHelper;
import cn.waleychain.exchange.feign.CommonServiceFeign;
import cn.waleychain.exchange.feign.UserServiceFeign;
import cn.waleychain.exchange.model.UserBank;
import cn.waleychain.exchange.model.UserInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(description = "用户相关接口")
@RestController
@RequestMapping("api/v1/user")
public class UserController extends BaseController {
	
	private static final Logger mLog = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserServiceFeign userFeign;
	
	@Autowired
	private CommonServiceFeign commonFeign;
	
	@RequestMapping(value = "/checkMailIsRegistry", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "验证邮箱是否已注册", notes = "验证邮箱是否已注册，URL: http://{ip}:{port}/{projectName}/api/v1/user/checkMailIsRegistry", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult checkMailIsRegistry (
			@ApiParam(value = "邮箱地址", required = true) @RequestParam String email) throws Exception {

		try {

			boolean result = userFeign.checkEmailIsExist(email);
			if (result) {
				return DefaultResult.buildFailedResult(RetResultCode.E20007);
			}

			return DefaultResult.buildSuccessResult();
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "验证邮箱是否已注册");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/emailRgs", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "用户邮箱注册", notes = "用户邮箱注册，URL: http://{ip}:{port}/{projectName}/api/v1/user/emailRgs", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult emailRgs (
			@ApiParam(value = "邮箱地址", required = true) @RequestParam String email,
			@ApiParam(value = "登录密码", required = true) @RequestParam String password,
			@ApiParam(value = "邀请码", required = false) @RequestParam(required = false) String invitionCode) throws Exception {

		try {

			UserInfo result = userFeign.emailRgstry(email, password, invitionCode);
			if (result != null) {
				return DefaultResult.buildSuccessResult(result);
			}
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "用户邮箱注册");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/activateAccount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "用户邮箱注册验证", notes = "用户邮箱注册验证，URL: http://{ip}:{port}/{projectName}/api/v1/user/activateAccount", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult activateAccount (
			@ApiParam(value = "邮箱地址", required = true) @RequestParam String code) throws Exception {

		try {

			boolean result = userFeign.activateAccount(code);
			
			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "用户邮箱注册验证");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/checkMobileIsRegistry", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "验证手机号是否已注册", notes = "验证手机号是否已注册，URL: http://{ip}:{port}/{projectName}/api/v1/user/checkMobileIsRegistry", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult checkMobileIsRegistry (
			@ApiParam(value = "手机号码", required = true) @RequestParam String mobile) throws Exception {

		try {

			VaildateHelper.vaildatePhone(mobile);
			
			boolean result = userFeign.checkMobileIsExist(mobile);
			if (result) {
				return DefaultResult.buildFailedResult(RetResultCode.E20007);
			}

			return DefaultResult.buildSuccessResult();
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "验证手机号是否已注册");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/registry", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "用户手机号注册", notes = "用户手机号注册，URL: http://{ip}:{port}/{projectName}/api/v1/user/registry", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult registry (
			@ApiParam(value = "手机号码", required = true) @RequestParam String mobile,
			@ApiParam(value = "登录密码", required = true) @RequestParam String password,
			@ApiParam(value = "动态验证码", required = true) @RequestParam String mobileCode,
			@ApiParam(value = "邀请码", required = false) @RequestParam(required = false) String invitionCode) throws Exception {

		try {

			VaildateHelper.vaildatePhone(mobile);
			
			// 验证验证码
			commonFeign.vaildateSmsCode(mobile, mobileCode);
			
			UserInfo user = new UserInfo();
			user.setUserId(SequenceUtils.generateDefaultSerialNum());
			user.setUserName(mobile);
			user.setMobile(mobile);
			user.setPassword(password);
			user.setIsSetPayPassword(DDIC.Boolean.BOOL_FALSE_0.id);
			user.setStatus(DDIC.UserAccountStatus.ENABLED.id);
			user.setAuthLevel(DDIC.UserAuthLevel.AUTH_LEVEL_1.id);
			user.setInvitionCode(invitionCode);
			user.setIsDeductible(DDIC.Boolean.BOOL_FALSE_0.id);
			user.setCreateTime(new Date());
			user.setModifiedTime(new Date());
			
			UserInfo result = userFeign.addUserInfo(user);
			if (result != null) {
				return DefaultResult.buildSuccessResult(result);
			}
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "用户手机号注册");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/modify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "编辑用户信息", notes = "编辑用户信息，URL: http://{ip}:{port}/{projectName}/api/v1/user/modify", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult modify (
			@ApiParam(value = "用户ID", required = true) @RequestParam Long userId,
			@ApiParam(value = "用户名", required = false) @RequestParam(required = false) String userName,
			@ApiParam(value = "真实姓名", required = false) @RequestParam(required = false) String realName,
			/*@ApiParam(value = "证件类型（1 身份证 2 军官证 3 护照）", required = false) @RequestParam(required = false) Integer idCardType,
			@ApiParam(value = "证件号码", required = false) @RequestParam(required = false) String idCard,*/
			@ApiParam(value = "邮箱", required = false) @RequestParam(required = false) String email,
			@ApiParam(value = "状态（0 禁用 1 启用）", required = false) @RequestParam(required = false) Integer status,
			@RequestHeader String Authorization) throws Exception {

		try {

			UserInfo user = new UserInfo();
			user.setUserId(userId);
			user.setUserName(userName);
			user.setRealName(realName);
//			user.setIdCardType(idCardType);
//			user.setIdCard(idCard);
			user.setEmail(email);
			user.setStatus(status);
			user.setModifiedTime(new Date());
			
			boolean result = userFeign.modifyUserInfo(user);

			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "编辑用户信息");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取用户信息", notes = "获取用户信息，URL: http://{ip}:{port}/{projectName}/api/v1/user/info", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchUserInfo(
			@ApiParam(value = "标识ID", required = true) @RequestParam Long userId, 
			@RequestHeader String Authorization) throws Exception {

		try {

			UserInfo user = userFeign.fetchUserInfo(userId);

			return DefaultResult.buildSuccessResult(user);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取用户信息");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "用户登录", notes = "用户登录，URL: http://{ip}:{port}/{projectName}/api/v1/user/login", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult login (
			@ApiParam(value = "手机号/邮箱") @RequestParam String loginName,
			@ApiParam(value = "密码") @RequestParam String password,
			@RequestHeader String Authorization) throws Exception {

		try {
			
			UserInfo info = userFeign.login(loginName, password);
			if (info != null) {
				return DefaultResult.buildSuccessResult(info);
			}
			
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "用户登录");
		}

		return DefaultResult.buildFailedResult();
	}

	@RequestMapping(value = "/fetchPageList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "分页获取用户列表", notes = "分页获取用户列表，URL: http://{ip}:{port}/{projectName}/api/v1/user/fetchPageList", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchPageList(
			@ApiParam(value = "用户标识ID", required = false) @RequestParam(required = false) String userId,
			@ApiParam(value = "手机号码", required = false) @RequestParam(required = false) String phoneNum,
			@ApiParam(value = "搜索开始时间", required = false) @RequestParam(required = false) Date beginDate,
			@ApiParam(value = "搜索结束时间", required = false) @RequestParam(required = false) Date endDate,
			@ApiParam(value = "每次查询多少条数据", required = true) @RequestParam(required = true, defaultValue = "-1") Integer showCount, 
			@ApiParam(value = "当前查询页", required = true) @RequestParam(required = true, defaultValue = "1") Integer currentPage, 
			@ApiParam(value = "搜索关键字", required = false) @RequestParam(required = false) String keyword,
			@RequestHeader String Authorization) throws Exception {

		try {

			PageInfo<UserInfo> page = userFeign.fetchUserPageList(userId, phoneNum, keyword, beginDate, endDate, showCount, currentPage);

			return DefaultResult.buildSuccessResult(page);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "分页获取用户列表");
		}

		return DefaultResult.buildSuccessResult();
	}
	
	@RequestMapping(value = "/modifyPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "用户修改密码", notes = "用户修改密码，URL: http://{ip}:{port}/{projectName}/api/v1/user/modifyPassword", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult modifyPassword(
			@ApiParam(value = "用户标识ID") @RequestParam Long userId,
			@ApiParam(value = "新密码") @RequestParam String password,
			@ApiParam(value = "原密码") @RequestParam String oldPassword,
			@RequestHeader String Authorization) throws Exception {

		try {

			// 获取用户数据
			UserInfo user = userFeign.fetchUserInfo(userId);
			VaildateHelper.vaildateEntityIsNull(user);
			
			boolean result = userFeign.modifyPassword(userId, password, oldPassword);

			return DefaultResult.buildSuccessResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "用户修改密码");
		}

		return DefaultResult.buildSuccessResult();
	}
	
	@RequestMapping(value = "/findPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "用户找回密码", notes = "用户找回密码，URL: http://{ip}:{port}/{projectName}/api/v1/user/findPassword", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult findPassword2 (
			@ApiParam(value = "手机号") @RequestParam String mobile,
			@ApiParam(value = "新密码") @RequestParam String password,
			@ApiParam(value = "动态验证码", required = true) @RequestParam String mobileCode,
			@RequestHeader String Authorization) throws Exception {

		try {

			commonFeign.vaildateSmsCode(mobile, mobileCode);
			
			boolean result = userFeign.modifyPassByMobile(mobile, password);

			return DefaultResult.buildSuccessResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "用户找回密码");
		}

		return DefaultResult.buildSuccessResult();
	}
	
	@RequestMapping(value = "/modifyPayPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "修改资金密码", notes = "修改资金密码，URL: http://{ip}:{port}/{projectName}/api/v1/user/modifyPayPassword", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult modifyPayPassword(
			@ApiParam(value = "用户标识ID") @RequestParam Long userId,
			@ApiParam(value = "是否已设置资金密码（0 否 1 是）") @RequestParam Integer isSetPayPassword,
			@ApiParam(value = "新密码") @RequestParam String password,
			@ApiParam(value = "原密码，若isSetPayPassword为1必填", required = false) @RequestParam(required = false) String oldPassword,
			@RequestHeader String Authorization) throws Exception {

		try {
			
			VaildateHelper.vaildateIntParams(isSetPayPassword, "是否已设置资金密码", DDIC.Boolean.BOOL_FALSE_0.id, DDIC.Boolean.BOOL_TRUE_1.id);
			if (DDIC.Boolean.BOOL_TRUE_1.id == isSetPayPassword) {
				VaildateHelper.vaildateStrIsNull(oldPassword, "原密码");
			}
			
			boolean result = userFeign.modifyPayPassword(userId, isSetPayPassword, password, oldPassword);

			return DefaultResult.buildSuccessResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "修改资金密码");
		}

		return DefaultResult.buildSuccessResult();
	}
	
	@RequestMapping(value = "/findPayPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "用户找回资金密码", notes = "用户找回资金密码，URL: http://{ip}:{port}/{projectName}/api/v1/user/findPayPassword", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult findPayPassword (
			@ApiParam(value = "手机号") @RequestParam String mobile,
			@ApiParam(value = "新密码") @RequestParam String password,
			@ApiParam(value = "动态验证码", required = true) @RequestParam String mobileCode,
			@RequestHeader String Authorization) throws Exception {

		try {

			commonFeign.vaildateSmsCode(mobile, mobileCode);
			
			boolean result = userFeign.modifyPayPassByMobile(mobile, password);

			return DefaultResult.buildSuccessResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "用户找回资金密码");
		}

		return DefaultResult.buildSuccessResult();
	}
	
	@RequestMapping(value = "/userIdentifyAuth", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "用户身份认证", notes = "用户身份认证，URL: http://{ip}:{port}/{projectName}/api/v1/user/userIdentifyAuth", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult userIdentifyAuth (
			@ApiParam(value = "用户ID", required = true) @RequestParam Long userId,
			@ApiParam(value = "姓名", required = true) @RequestParam String realName,
			@ApiParam(value = "证件类型（1 身份证 2 军官证 3 护照）", required = true) @RequestParam Integer idCardType,
			@ApiParam(value = "证件号码", required = true) @RequestParam String idCard,
			@ApiParam(value = "认证等级（1 初级 2 中级 3 高级）", required = true) @RequestParam Integer authLevel,
			@ApiParam(value = "身份证正面照", required = false) @RequestParam(required = false) String cardPicture1,
			@ApiParam(value = "身份证反面照", required = false) @RequestParam(required = false) String cardPicture2,
			@RequestHeader String Authorization) throws Exception {

		try {
			
			VaildateHelper.vaildateIntParams(idCardType, "证件类型", 1, 2, 3);
			VaildateHelper.vaildateIntParams(authLevel, "认证等级", 1, 2, 3);
			
			UserInfo updateUser = new UserInfo();
			updateUser.setUserId(userId);
			updateUser.setRealName(realName);
			updateUser.setIdCardType(idCardType);
			updateUser.setIdCard(idCard);
			updateUser.setAuthLevel(authLevel);
			updateUser.setAuthTime(new Date());
			updateUser.setModifiedTime(new Date());
			
			boolean result = userFeign.userIdentifyAuth(updateUser);
			
			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "用户身份认证");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/bindBankCard", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "绑定银行卡", notes = "绑定银行卡，URL: http://{ip}:{port}/{projectName}/api/v1/user/bindBankCard", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult bindBankCard (
			@ApiParam(value = "用户ID", required = true) @RequestParam Long userId,
			@ApiParam(value = "开户名", required = true) @RequestParam String name,
			@ApiParam(value = "开户银行", required = true) @RequestParam String bankName,
			@ApiParam(value = "银行卡号", required = true) @RequestParam String bankCard,
			@ApiParam(value = "备注名", required = true) @RequestParam String remark,
			@ApiParam(value = "交易密码", required = true) @RequestParam String payPassword,
			@ApiParam(value = "开户银行所在省", required = false) @RequestParam(required = false) String privince,
			@ApiParam(value = "开户银行所在市", required = false) @RequestParam(required = false) String city,
			@ApiParam(value = "开户支行名称", required = false) @RequestParam(required = false) String branchName,
			@RequestHeader String Authorization) throws Exception {

		try {
			
			// 判断是否实名认证
			boolean isAuth = userFeign.checkUserIsRealNameAuth(userId);
			VaildateHelper.vaildateBooleanResult(!isAuth, RetResultCode.E20014);
			
			// 获取用户数据，只能添加用户相同名称下的银行卡
			UserInfo user = userFeign.fetchUserInfo(userId);
			VaildateHelper.vaildateEntityIsNull(user);
			
			VaildateHelper.vaildateBooleanResult(!name.equals(user.getRealName()), RetResultCode.E11001, "只可以添加本人名下的银行卡");
			
			// 验证交易密码
			boolean checkPaypassword = userFeign.checkUserPayPassword(userId, payPassword);
			VaildateHelper.vaildateBooleanResult(!checkPaypassword, RetResultCode.E20024);
			
			UserBank bank = new UserBank();
			bank.setUserId(userId);
			bank.setName(name);
			bank.setBankName(bankName);
			bank.setPrivince(privince);
			bank.setCity(city);
			bank.setBranchName(branchName);
			bank.setBankCard(bankCard);
			bank.setStatus(DDIC.UserBankStatus.STATUS_1.id);
			bank.setRemark(remark);
			bank.setCreateTime(new Date());
			bank.setModifiedTime(new Date());
			
			boolean result = userFeign.bindBankCard(bank);

			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "绑定银行卡");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/fetchUserBankList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取用户银行卡列表", notes = "获取用户银行卡列表，URL: http://{ip}:{port}/{projectName}/api/v1/user/fetchUserBankList", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchUserBankList(
			@ApiParam(value = "用户标识ID", required = true) @RequestParam Long userId,
			@RequestHeader String Authorization) throws Exception {

		try {

			List<UserBank> list = userFeign.fetchUserBankList(userId);

			return DefaultResult.buildSuccessResult(list);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取用户银行卡列表");
		}

		return DefaultResult.buildSuccessResult();
	}
	
}
