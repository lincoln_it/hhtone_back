/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.consumer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.result.DefaultResult;
import cn.waleychain.exchange.core.result.IBaseResult;
import cn.waleychain.exchange.core.result.RetResultCode;
import cn.waleychain.exchange.core.utils.StringUtils;
import cn.waleychain.exchange.core.vaildate.SecurityCodeUtil;
import cn.waleychain.exchange.core.vaildate.VaildateHelper;
import cn.waleychain.exchange.feign.CommonServiceFeign;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 公共通用
 * @author chenx
 * @date 2017年10月24日 下午5:04:08
 * @version 1.0 
 */
@Api(description = "公共通用")
@RestController
@RequestMapping("api/v1/common")
public class CommonController extends BaseController {

	private static final Logger mLog = LoggerFactory.getLogger(CommonController.class);
	
	@Autowired
	private CommonServiceFeign commonFeign;
	
	@RequestMapping(value = "/sendVaildateCode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "发送手机验证码", notes = "发送手机验证码，URL: http://{ip}:{port}/{projectName}/api/v1/common/sendVaildateCode", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult sendVaildateCode (
			@ApiParam(value = "手机号码", required = true) @RequestParam String mobile,
			@ApiParam(value = "图片验证码", required = false) @RequestParam(required = false) String verifyCode) throws Exception {

		try {

			VaildateHelper.vaildatePhone(mobile);
			// 验证验证码
			if (StringUtils.isNotBlank(verifyCode)) {
				boolean bool = SecurityCodeUtil.whetherImgCaptchaMatch(verifyCode, request);
				VaildateHelper.vaildateBooleanResult(bool, RetResultCode.E12005);
			}
			
			boolean result = commonFeign.sendSmsCode(mobile);	

			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "发送手机验证码");
		}

		return DefaultResult.buildFailedResult();
	}
	
}
