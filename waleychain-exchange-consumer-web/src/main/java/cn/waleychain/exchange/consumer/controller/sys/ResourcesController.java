/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.consumer.controller.sys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.consumer.controller.BaseController;
import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.result.DefaultResult;
import cn.waleychain.exchange.core.result.IBaseResult;
import cn.waleychain.exchange.feign.DfsServiceFeign;
import cn.waleychain.exchange.model.FileResource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(description = "文件资源相关接口")
@RestController
@RequestMapping("api/v1/res")
public class ResourcesController extends BaseController {

	private static final Logger mLog = LoggerFactory.getLogger(ResourcesController.class);
	
	@Autowired
	private DfsServiceFeign dfsFeign;
	
	@RequestMapping(value = "/fetchResourcesList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取系统资源列表", notes = "获取系统资源列表，URL: http://{ip}:{port}/{projectName}/api/v1/res/fetchResourcesList", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchResourcesList (
			@ApiParam(value = "名称", required = false) @RequestParam(required = false) String name,
			@ApiParam(value = "类型（1 图片 2 文件）", required = false) @RequestParam(required = false) Integer type,
			@ApiParam(value = "每次查询多少条数据", required = true) @RequestParam(required = true, defaultValue = "-1") Integer showCount, 
			@ApiParam(value = "当前查询页", required = true) @RequestParam(required = true, defaultValue = "1") Integer currentPage, 
			@RequestHeader String Authorization) throws Exception {

		try {
			
			PageInfo<FileResource> page = dfsFeign.fetchResourcesList(name, type, showCount, currentPage);

			return DefaultResult.buildSuccessResult(page);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取系统资源列表");
		}

		return DefaultResult.buildFailedResult();
	}
	
}
