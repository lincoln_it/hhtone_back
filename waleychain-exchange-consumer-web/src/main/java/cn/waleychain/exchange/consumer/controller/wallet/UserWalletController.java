/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.consumer.controller.wallet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.waleychain.exchange.consumer.controller.BaseController;
import cn.waleychain.exchange.core.entity.PageInfo;
import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.result.DefaultResult;
import cn.waleychain.exchange.core.result.IBaseResult;
import cn.waleychain.exchange.feign.UserWalletServiceFeign;
import cn.waleychain.exchange.model.UserWallet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(description = "用户个人钱包相关接口")
@RestController
@RequestMapping("api/v1/userWallet")
public class UserWalletController extends BaseController {

	private static final Logger mLog = LoggerFactory.getLogger(UserWalletController.class);
	
	@Autowired
	private UserWalletServiceFeign userWalletFeign;
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "添加用户个人钱包", notes = "添加用户个人钱包，URL: http://{ip}:{port}/{projectName}/api/v1/userWallet/new", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult addUserWallet (
			@ApiParam(value = "用户标识ID", required = true) @RequestParam Long userId,
			@ApiParam(value = "币种标识ID", required = true) @RequestParam Long coinId,
			@ApiParam(value = "钱包名称", required = true) @RequestParam String name,
			@ApiParam(value = "钱包地址", required = true) @RequestParam String address,
			@ApiParam(value = "交易密码", required = true) @RequestParam String payPassword,
			@RequestHeader String Authorization) throws Exception {

		try {
			
			boolean result = userWalletFeign.addUserWallet(userId, coinId, name, address, payPassword);

			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "添加用户个人钱包");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取钱包信息", notes = "获取钱包信息，URL: http://{ip}:{port}/{projectName}/api/v1/userWallet/info", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchUserWalletInfo(
			@ApiParam(value = "标识ID", required = true) @RequestParam Long userWalletId, 
			@RequestHeader String Authorization) throws Exception {

		try {

			UserWallet userWallet = userWalletFeign.fetchUserWalletById(userWalletId);
			
			return DefaultResult.buildSuccessResult(userWallet);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取钱包信息");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/fetchPageList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "获取用户个人钱包列表", notes = "获取用户个人钱包列表，URL: http://{ip}:{port}/{projectName}/api/v1/userWallet/fetchPageList", httpMethod = "GET", response = DefaultResult.class)
	public IBaseResult fetchPageList(
			@ApiParam(value = "用户标识ID", required = true) @RequestParam Long userId,
			@ApiParam(value = "币种标识ID", required = false) @RequestParam(required = false) Long coinId, 
			@ApiParam(value = "名称", required = false) @RequestParam(required = false) String name,
			@ApiParam(value = "每次查询多少条数据", required = true) @RequestParam(required = true, defaultValue = "-1") Integer showCount, 
			@ApiParam(value = "当前查询页", required = true) @RequestParam(required = true, defaultValue = "1") Integer currentPage,  
			@RequestHeader String Authorization) throws Exception {

		try {

			PageInfo<UserWallet> page = userWalletFeign.fetchUserWalletPageList(name, coinId, userId, showCount, currentPage);

			return DefaultResult.buildSuccessResult(page);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取用户个人钱包列表");
		}

		return DefaultResult.buildFailedResult();
	}
	
	@RequestMapping(value = "/modify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "编辑用户个人钱包信息", notes = "编辑用户个人钱包信息，URL: http://{ip}:{port}/{projectName}/api/v1/userWallet/modify", httpMethod = "POST", response = DefaultResult.class)
	public IBaseResult modify (
			@ApiParam(value = "标识ID", required = true) @RequestParam Long userWalletId,
			@ApiParam(value = "用户标识ID", required = true) @RequestParam Long userId,
			@ApiParam(value = "钱包名称", required = true) @RequestParam String name,
			@ApiParam(value = "钱包地址", required = true) @RequestParam String address,
			@ApiParam(value = "交易密码", required = true) @RequestParam String payPassword,
			@RequestHeader String Authorization) throws Exception {

		try {

			boolean result = userWalletFeign.modifyUserWallet(userWalletId, userId, name, address, payPassword);

			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "编辑用户个人钱包信息");
		}

		return DefaultResult.buildFailedResult();
	}
	

	@RequestMapping(value = "/del", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "删除用户个人钱包", notes = "删除用户个人钱包，URL: http://{ip}:{port}/{projectName}/api/v1/userWallet/del", httpMethod = "DELETE", response = DefaultResult.class)
	public IBaseResult delete (
			@ApiParam(value = "标识ID", required = true) @RequestParam Long userWalletId,
			@ApiParam(value = "用户标识ID", required = true) @RequestParam Long userId,
			@ApiParam(value = "交易密码", required = true) @RequestParam String payPassword,
			@RequestHeader String Authorization) throws Exception {

		try {

			boolean result = userWalletFeign.delete(userWalletId, userId, payPassword);

			return DefaultResult.buildResult(result);
		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "删除用户个人钱包");
		}

		return DefaultResult.buildFailedResult();
	}
	
}
