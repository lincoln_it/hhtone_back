/**
 * Copyright (c) 2017, 西安星沙网络科技-版权所有
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.waleychain.exchange.consumer.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.bingoohuang.patchca.custom.ConfigurableCaptchaService;
import com.github.bingoohuang.patchca.filter.FilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.CurvesRippleFilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.DiffuseRippleFilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.DoubleRippleFilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.MarbleRippleFilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.WobbleRippleFilterFactory;
import com.github.bingoohuang.patchca.utils.encoder.EncoderHelper;

import cn.waleychain.exchange.core.Global;
import cn.waleychain.exchange.core.constant.ImageType;
import cn.waleychain.exchange.core.logger.LoggerHelper;
import cn.waleychain.exchange.core.vaildate.CaptchaFactory;

@Controller
@RequestMapping("frw")
public class ForwordController extends BaseController {

	private static final Logger mLog = LoggerFactory.getLogger(CommonController.class);
	
	private static ConfigurableCaptchaService cs = CaptchaFactory.getInstance();

    private static List<FilterFactory> factories;

    static {
        factories = new ArrayList<FilterFactory>();
        factories.add(new CurvesRippleFilterFactory(cs.getColorFactory()));
        factories.add(new MarbleRippleFilterFactory());
        factories.add(new DoubleRippleFilterFactory());
        factories.add(new WobbleRippleFilterFactory());
        factories.add(new DiffuseRippleFilterFactory());
    }
    
    @RequestMapping("/getImage.htm")
	public void getImagehtm () throws Exception {

		try {
			
			cs.setFilterFactory(factories.get(new Random().nextInt(5)));
            setResponseHeaders(response);

            Session session = SecurityUtils.getSubject().getSession();
            String token = EncoderHelper.getChallangeAndWriteImage(cs, ImageType.PNG.name(),
                response.getOutputStream());
            
            session.setAttribute(Global.IMAGE_VAILDATE_CODE_NAME, token);
            
            LoggerHelper.printLogInfo(mLog, "当前的SessionID = " + session.getId() + "，  验证码 = " + token);

		} catch (Exception e) {
			LoggerHelper.printLogError(mLog, e, "获取图片验证码");
		}

	}
	
	private void setResponseHeaders(HttpServletResponse response) {
        response.setContentType("image/png");
        response.setHeader("Cache-Control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        long time = System.currentTimeMillis();
        response.setDateHeader("Last-Modified", time);
        response.setDateHeader("Date", time);
        response.setDateHeader("Expires", time);
    }
}
