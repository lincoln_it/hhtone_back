package cn.waleychain.exchange.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 服务消费者，对外提供接口
 * 
 * @author chenx
 * @date 2018年4月11日 上午11:48:56
 * @version 1.0
 */
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"cn.waleychain.exchange.feign"})
public class Application {
	
	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}
}
