# waleychain-exchange
H网云未来数字货币交易平台

API
---
项目开发使用说明
    项目采用spring cloud体系的微服务架构，数据使用mysql及redis缓存。

# 模块说明

采用 maven 多模块的方式开发,有一个 **parent** 父级模块,用来控制整个模块版本.

## 模块结构

```
├── waleychain-exchange-core 公共核心模块
├── waleychain-exchange-model 项目model
├── waleychain-exchange-dao 数据访问层，mybatis实现
├── waleychain-exchange-api 业务接口模块
├── waleychain-exchange-service 业务接口实现
├── waleychain-exchange-server-registry 服务注册中心
├── waleychain-exchange-server-gateway 微服务，路由网关
├── waleychain-exchange-server-config 配置中心（暂未使用）
├── waleychain-exchange-provider-account 服务提供者，用户账号相关
├── waleychain-exchange-provider-authority 服务提供者，授权相关
├── waleychain-exchange-provider-cms 服务提供者，内容管理相关
├── waleychain-exchange-provider-market 服务提供者，市场相关
├── waleychain-exchange-provider-sys 服务提供者，平台系统相关
├── waleychain-exchange-provider-trade 服务提供者，交易相关
├── waleychain-exchange-provider-wallet 服务提供者，钱包相关
└── waleychain-exchange-consumer-web 服务消费者，对外提供接口
```

### 服务部署

项目启动顺序：waleychain-exchange-server-registry -> waleychain-exchange-server-gateway -> 各服务提供者 -> 各服务消费者

