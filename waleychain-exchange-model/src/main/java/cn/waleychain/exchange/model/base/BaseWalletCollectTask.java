package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 钱包转账定时任务
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-28 16:53:47
 */
public class BaseWalletCollectTask extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "wallet_collect_task";
	
	/**
	 * 标识ID
	 */
	private Long idNo;
	/**
	 * 币种ID
	 */
	private Long coinId;
	/**
	 * 来自哪个用户
	 */
	private Long fromUser;
	/**
	 * 转到哪里
	 */
	private String toAddr;
	/**
	 * 归集数量
	 */
	private BigDecimal amount;
	/**
	 * 定时执行
	 */
	private Date execTime;
	/**
	 * 是否处理
	 */
	private Integer status;

	/**
	 * 设置：标识ID
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：标识ID
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：币种ID
	 */
	public void setCoinId(Long coinId) {
		this.coinId = coinId;
	}
	/**
	 * 获取：币种ID
	 */
	public Long getCoinId() {
		return coinId;
	}
	/**
	 * 设置：来自哪个用户
	 */
	public void setFromUser(Long fromUser) {
		this.fromUser = fromUser;
	}
	/**
	 * 获取：来自哪个用户
	 */
	public Long getFromUser() {
		return fromUser;
	}
	/**
	 * 设置：转到哪里
	 */
	public void setToAddr(String toAddr) {
		this.toAddr = toAddr;
	}
	/**
	 * 获取：转到哪里
	 */
	public String getToAddr() {
		return toAddr;
	}
	/**
	 * 设置：归集数量
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：归集数量
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：定时执行
	 */
	public void setExecTime(Date execTime) {
		this.execTime = execTime;
	}
	/**
	 * 获取：定时执行
	 */
	public Date getExecTime() {
		return execTime;
	}
	/**
	 * 设置：是否处理
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：是否处理
	 */
	public Integer getStatus() {
		return status;
	}
}
