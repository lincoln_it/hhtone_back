package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseFileResource;

/**
 * 文件资源
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:40
 */
public class FileResource extends BaseFileResource {

	private static final long serialVersionUID = 1L;

}
