package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 委托订单
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:23
 */
public class BaseEntrustOrder extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "entrust_order";
	
	/**
	 * 主键
	 */
	private Long orderId;
	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 交易对ID
	 */
	private Long marketId;
	/**
	 * 委托价格
	 */
	private BigDecimal price;
	/**
	 * 委托数量
	 */
	private BigDecimal number;
	/**
	 * 预计成交总额
	 */
	private BigDecimal amount;
	/**
	 * 手续费费率
	 */
	private BigDecimal feeRate;
	/**
	 * 手续费
	 */
	private BigDecimal fee;
	/**
	 * 已成交量
	 */
	private BigDecimal dealNumber;
	/**
	 * 冻结量
	 */
	private BigDecimal lockNumber;
	/**
	 * 委托单类型
                        1-买单；
                        2-卖单；
	 */
	private Integer type;
	/**
	 * 状态
                        0-未成交；
                        1-已成交；
                        2-已撤单；
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：主键
	 */
	public Long getOrderId() {
		return orderId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：交易对ID
	 */
	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
	/**
	 * 获取：交易对ID
	 */
	public Long getMarketId() {
		return marketId;
	}
	/**
	 * 设置：委托价格
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * 获取：委托价格
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * 设置：委托数量
	 */
	public void setNumber(BigDecimal number) {
		this.number = number;
	}
	/**
	 * 获取：委托数量
	 */
	public BigDecimal getNumber() {
		return number;
	}
	/**
	 * 设置：预计成交总额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：预计成交总额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：手续费费率
	 */
	public void setFeeRate(BigDecimal feeRate) {
		this.feeRate = feeRate;
	}
	/**
	 * 获取：手续费费率
	 */
	public BigDecimal getFeeRate() {
		return feeRate;
	}
	/**
	 * 设置：手续费
	 */
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	/**
	 * 获取：手续费
	 */
	public BigDecimal getFee() {
		return fee;
	}
	/**
	 * 设置：已成交量
	 */
	public void setDealNumber(BigDecimal dealNumber) {
		this.dealNumber = dealNumber;
	}
	/**
	 * 获取：已成交量
	 */
	public BigDecimal getDealNumber() {
		return dealNumber;
	}
	/**
	 * 设置：冻结量
	 */
	public void setLockNumber(BigDecimal lockNumber) {
		this.lockNumber = lockNumber;
	}
	/**
	 * 获取：冻结量
	 */
	public BigDecimal getLockNumber() {
		return lockNumber;
	}
	/**
	 * 设置：委托单类型
                        1-买单；
                        2-卖单；
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：委托单类型
                        1-买单；
                        2-卖单；
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：状态
                        0-未成交；
                        1-已成交；
                        2-已撤单；
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-未成交；
                        1-已成交；
                        2-已撤单；
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
