package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 资金账户流水记录（?该表只保留核心字段，用视图实现统计流水功能）
通过biz_type和biz_type_i
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:20
 */
public class BaseAccountChange extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "account_change";
	
	/**
	 * 主键
	 */
	private Long idNo;
	/**
	 * 用户账号ID
	 */
	private Long accountId;
	/**
	 * 业务类别， 分别对应：1 entrust_order 2 deal_order  3 account_withdraw  4 account_recharge
	 */
	private Integer bizType;
	/**
	 * 业务表id
	 */
	private Long bizTypeId;
	/**
	 * 收支类型
                        1-收入；
                        2-支出；
	 */
	private Integer type;
	/**
	 * 业务类型
                        10-委托冻结；
                        11-委托解冻；
                        12-委托成交；
                        13-委托成交手续费；
                        14-委托撤单；
                        30-提现待审核；
                        31-提现拒绝；
                        32-提现完成；
                        40-充值处理中；
                        41-充值拒绝；
                        42-充值到账
	 */
	private Integer status;
	/**
	 * 备注
                        充值；
                        提现；
                        冻结；
                        解冻；
                        转出；
                        转入；
	 */
	private String remark;
	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 设置：主键
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：用户账号ID
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	/**
	 * 获取：用户账号ID
	 */
	public Long getAccountId() {
		return accountId;
	}
	/**
	 * 设置：业务类别， 分别对应：1 entrust_order 2 deal_order  3 account_withdraw  4 account_recharge
	 */
	public void setBizType(Integer bizType) {
		this.bizType = bizType;
	}
	/**
	 * 获取：业务类别， 分别对应：1 entrust_order 2 deal_order  3 account_withdraw  4 account_recharge
	 */
	public Integer getBizType() {
		return bizType;
	}
	/**
	 * 设置：业务表id
	 */
	public void setBizTypeId(Long bizTypeId) {
		this.bizTypeId = bizTypeId;
	}
	/**
	 * 获取：业务表id
	 */
	public Long getBizTypeId() {
		return bizTypeId;
	}
	/**
	 * 设置：收支类型
                        1-收入；
                        2-支出；
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：收支类型
                        1-收入；
                        2-支出；
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：业务类型
                        10-委托冻结；
                        11-委托解冻；
                        12-委托成交；
                        13-委托成交手续费；
                        14-委托撤单；
                        30-提现待审核；
                        31-提现拒绝；
                        32-提现完成；
                        40-充值处理中；
                        41-充值拒绝；
                        42-充值到账
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：业务类型
                        10-委托冻结；
                        11-委托解冻；
                        12-委托成交；
                        13-委托成交手续费；
                        14-委托撤单；
                        30-提现待审核；
                        31-提现拒绝；
                        32-提现完成；
                        40-充值处理中；
                        41-充值拒绝；
                        42-充值到账
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：备注
                        充值；
                        提现；
                        冻结；
                        解冻；
                        转出；
                        转入；
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
                        充值；
                        提现；
                        冻结；
                        解冻；
                        转出；
                        转入；
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
