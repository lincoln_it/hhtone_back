package cn.waleychain.exchange.model;

import java.math.BigDecimal;

import cn.waleychain.exchange.model.base.BaseMarket;

/**
 * 
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:41
 */
public class Market extends BaseMarket {

	private static final long serialVersionUID = 1L;

	private BigDecimal buyLimitMax;
	
	private BigDecimal sellLimitMin;
	
	private BigDecimal closePrice;
	
	/**
	 * 资源图片url
	 */
	private String iconResUrl;

	public BigDecimal getBuyLimitMax() {
		return buyLimitMax;
	}

	public void setBuyLimitMax(BigDecimal buyLimitMax) {
		this.buyLimitMax = buyLimitMax;
	}

	public BigDecimal getSellLimitMin() {
		return sellLimitMin;
	}

	public void setSellLimitMin(BigDecimal sellLimitMin) {
		this.sellLimitMin = sellLimitMin;
	}

	public BigDecimal getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(BigDecimal closePrice) {
		this.closePrice = closePrice;
	}

	public String getIconResUrl() {
		return iconResUrl;
	}

	public void setIconResUrl(String iconResUrl) {
		this.iconResUrl = iconResUrl;
	}
	
}
