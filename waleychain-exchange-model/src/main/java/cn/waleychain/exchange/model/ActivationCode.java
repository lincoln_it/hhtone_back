package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseActivationCode;

/**
 * 用户邮箱注册激活码
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-05-02 12:06:55
 */
public class ActivationCode extends BaseActivationCode {

	private static final long serialVersionUID = 1L;

}
