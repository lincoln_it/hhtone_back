package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseUserInfo;

/**
 * 用户信息表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:42
 */
public class UserInfo extends BaseUserInfo {

	private static final long serialVersionUID = 1L;

}
