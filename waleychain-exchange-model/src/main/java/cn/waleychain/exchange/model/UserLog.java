package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseUserLog;

/**
 * 用户操作日志
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:42
 */
public class UserLog extends BaseUserLog {

	private static final long serialVersionUID = 1L;

}
