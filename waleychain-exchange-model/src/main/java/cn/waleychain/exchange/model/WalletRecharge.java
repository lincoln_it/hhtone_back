package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseWalletRecharge;

/**
 * 钱包充值
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:43
 */
public class WalletRecharge extends BaseWalletRecharge {

	private static final long serialVersionUID = 1L;

	/**
	 * 操作流水标识ID
	 */
	private Long walletChangeId;
	
	private String coinTitle;
	
	private Long userId;
	
	private String userName;
	
	private String realName;
	
	private String mobile;
	
	private Integer status;

	public Long getWalletChangeId() {
		return walletChangeId;
	}

	public void setWalletChangeId(Long walletChangeId) {
		this.walletChangeId = walletChangeId;
	}

	public String getCoinTitle() {
		return coinTitle;
	}

	public void setCoinTitle(String coinTitle) {
		this.coinTitle = coinTitle;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	} 
	
	
}
