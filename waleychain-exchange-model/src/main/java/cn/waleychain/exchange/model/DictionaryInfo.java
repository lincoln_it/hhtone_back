package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseDictionaryInfo;

/**
 * 系统码表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:40
 */
public class DictionaryInfo extends BaseDictionaryInfo {

	private static final long serialVersionUID = 1L;

}
