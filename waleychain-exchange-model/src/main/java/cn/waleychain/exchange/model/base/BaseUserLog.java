package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 用户操作日志
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:25
 */
public class BaseUserLog extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "user_log";
	
	/**
	 * 主键
	 */
	private Long idNo;
	/**
	 * 用户Id
	 */
	private Long userId;
	/**
	 * 日志类型
	 */
	private Integer type;
	/**
	 * 方法
	 */
	private String method;
	/**
	 * 参数
	 */
	private String params;
	/**
	 * 花费时间
	 */
	private Long time;
	/**
	 * ip地址
	 */
	private String ip;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建时间
	 */
	private Date created;

	/**
	 * 设置：主键
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：用户Id
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户Id
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：日志类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：日志类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：方法
	 */
	public void setMethod(String method) {
		this.method = method;
	}
	/**
	 * 获取：方法
	 */
	public String getMethod() {
		return method;
	}
	/**
	 * 设置：参数
	 */
	public void setParams(String params) {
		this.params = params;
	}
	/**
	 * 获取：参数
	 */
	public String getParams() {
		return params;
	}
	/**
	 * 设置：花费时间
	 */
	public void setTime(Long time) {
		this.time = time;
	}
	/**
	 * 获取：花费时间
	 */
	public Long getTime() {
		return time;
	}
	/**
	 * 设置：ip地址
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * 获取：ip地址
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * 设置：描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 获取：描述
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreated(Date created) {
		this.created = created;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreated() {
		return created;
	}
}
