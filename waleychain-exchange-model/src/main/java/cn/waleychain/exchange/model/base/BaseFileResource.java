package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 文件资源
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:23
 */
public class BaseFileResource extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "file_resource";
	
	/**
	 * 主键
	 */
	private Long resId;
	/**
	 * 
	 */
	private Integer type;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String url;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setResId(Long resId) {
		this.resId = resId;
	}
	/**
	 * 获取：主键
	 */
	public Long getResId() {
		return resId;
	}
	/**
	 * 设置：
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 获取：
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * 获取：
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
