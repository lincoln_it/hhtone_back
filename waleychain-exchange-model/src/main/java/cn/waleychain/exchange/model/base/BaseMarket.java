package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:24
 */
public class BaseMarket extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "market";
	
	/**
	 * 市场ID
	 */
	private Long marketId;
	/**
	 * 买方币种ID
	 */
	private Long buyCoinId;
	/**
	 * 卖方市场ID
	 */
	private Long sellCoinId;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 市场ICON
	 */
	private Long iconResId;
	/**
	 * 发行价格
	 */
	private BigDecimal openPrice;
	/**
	 * 最小买入量
	 */
	private BigDecimal buyMin;
	/**
	 * 最大买入量
	 */
	private BigDecimal buyMax;
	/**
	 * 最小卖出量
	 */
	private BigDecimal sellMin;
	/**
	 * 最大卖出量
	 */
	private BigDecimal sellMax;
	/**
	 * 买入手续费率
	 */
	private BigDecimal feeBuy;
	/**
	 * 卖出手续费率
	 */
	private BigDecimal feeSell;
	/**
	 * 单笔最小数量
	 */
	private BigDecimal tradeMin;
	/**
	 * 单笔最大数量
	 */
	private BigDecimal tradeMax;
	/**
	 * 涨幅
	 */
	private BigDecimal zhang;
	/**
	 * 跌幅
	 */
	private BigDecimal die;
	/**
	 * 告警价格比例
	 */
	private BigDecimal alertPriceRate;
	/**
	 * 交易时间
	 */
	private String tradeTime;
	/**
	 * 交易周期
	 */
	private String tradeWeek;
	/**
	 * 状态
            0禁用
            1启用
	 */
	private Integer status;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 排序列
	 */
	private Integer sorting;
	/**
	 * 交易区域
	 */
	private String transactionAreaId;

	/**
	 * 设置：市场ID
	 */
	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
	/**
	 * 获取：市场ID
	 */
	public Long getMarketId() {
		return marketId;
	}
	/**
	 * 设置：买方币种ID
	 */
	public void setBuyCoinId(Long buyCoinId) {
		this.buyCoinId = buyCoinId;
	}
	/**
	 * 获取：买方币种ID
	 */
	public Long getBuyCoinId() {
		return buyCoinId;
	}
	/**
	 * 设置：卖方市场ID
	 */
	public void setSellCoinId(Long sellCoinId) {
		this.sellCoinId = sellCoinId;
	}
	/**
	 * 获取：卖方市场ID
	 */
	public Long getSellCoinId() {
		return sellCoinId;
	}
	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：标题
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：市场ICON
	 */
	public void setIconResId(Long iconResId) {
		this.iconResId = iconResId;
	}
	/**
	 * 获取：市场ICON
	 */
	public Long getIconResId() {
		return iconResId;
	}
	/**
	 * 设置：发行价格
	 */
	public void setOpenPrice(BigDecimal openPrice) {
		this.openPrice = openPrice;
	}
	/**
	 * 获取：发行价格
	 */
	public BigDecimal getOpenPrice() {
		return openPrice;
	}
	/**
	 * 设置：最小买入量
	 */
	public void setBuyMin(BigDecimal buyMin) {
		this.buyMin = buyMin;
	}
	/**
	 * 获取：最小买入量
	 */
	public BigDecimal getBuyMin() {
		return buyMin;
	}
	/**
	 * 设置：最大买入量
	 */
	public void setBuyMax(BigDecimal buyMax) {
		this.buyMax = buyMax;
	}
	/**
	 * 获取：最大买入量
	 */
	public BigDecimal getBuyMax() {
		return buyMax;
	}
	/**
	 * 设置：最小卖出量
	 */
	public void setSellMin(BigDecimal sellMin) {
		this.sellMin = sellMin;
	}
	/**
	 * 获取：最小卖出量
	 */
	public BigDecimal getSellMin() {
		return sellMin;
	}
	/**
	 * 设置：最大卖出量
	 */
	public void setSellMax(BigDecimal sellMax) {
		this.sellMax = sellMax;
	}
	/**
	 * 获取：最大卖出量
	 */
	public BigDecimal getSellMax() {
		return sellMax;
	}
	/**
	 * 设置：买入手续费率
	 */
	public void setFeeBuy(BigDecimal feeBuy) {
		this.feeBuy = feeBuy;
	}
	/**
	 * 获取：买入手续费率
	 */
	public BigDecimal getFeeBuy() {
		return feeBuy;
	}
	/**
	 * 设置：卖出手续费率
	 */
	public void setFeeSell(BigDecimal feeSell) {
		this.feeSell = feeSell;
	}
	/**
	 * 获取：卖出手续费率
	 */
	public BigDecimal getFeeSell() {
		return feeSell;
	}
	/**
	 * 设置：单笔最小数量
	 */
	public void setTradeMin(BigDecimal tradeMin) {
		this.tradeMin = tradeMin;
	}
	/**
	 * 获取：单笔最小数量
	 */
	public BigDecimal getTradeMin() {
		return tradeMin;
	}
	/**
	 * 设置：单笔最大数量
	 */
	public void setTradeMax(BigDecimal tradeMax) {
		this.tradeMax = tradeMax;
	}
	/**
	 * 获取：单笔最大数量
	 */
	public BigDecimal getTradeMax() {
		return tradeMax;
	}
	/**
	 * 设置：涨幅
	 */
	public void setZhang(BigDecimal zhang) {
		this.zhang = zhang;
	}
	/**
	 * 获取：涨幅
	 */
	public BigDecimal getZhang() {
		return zhang;
	}
	/**
	 * 设置：跌幅
	 */
	public void setDie(BigDecimal die) {
		this.die = die;
	}
	/**
	 * 获取：跌幅
	 */
	public BigDecimal getDie() {
		return die;
	}
	/**
	 * 设置：告警价格比例
	 */
	public void setAlertPriceRate(BigDecimal alertPriceRate) {
		this.alertPriceRate = alertPriceRate;
	}
	/**
	 * 获取：告警价格比例
	 */
	public BigDecimal getAlertPriceRate() {
		return alertPriceRate;
	}
	/**
	 * 设置：交易时间
	 */
	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}
	/**
	 * 获取：交易时间
	 */
	public String getTradeTime() {
		return tradeTime;
	}
	/**
	 * 设置：交易周期
	 */
	public void setTradeWeek(String tradeWeek) {
		this.tradeWeek = tradeWeek;
	}
	/**
	 * 获取：交易周期
	 */
	public String getTradeWeek() {
		return tradeWeek;
	}
	/**
	 * 设置：状态
            0禁用
            1启用
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
            0禁用
            1启用
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：排序列
	 */
	public void setSorting(Integer sorting) {
		this.sorting = sorting;
	}
	/**
	 * 获取：排序列
	 */
	public Integer getSorting() {
		return sorting;
	}
	/**
	 * 设置：交易区域
	 */
	public void setTransactionAreaId(String transactionAreaId) {
		this.transactionAreaId = transactionAreaId;
	}
	/**
	 * 获取：交易区域
	 */
	public String getTransactionAreaId() {
		return transactionAreaId;
	}
}
