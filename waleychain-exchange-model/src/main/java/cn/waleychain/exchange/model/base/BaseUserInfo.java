package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 用户信息表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:25
 */
public class BaseUserInfo extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "user_info";
	
	/**
	 * 主键
	 */
	private Long userId;
	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 头像地址
	 */
	private String imageUrl;
	/**
	 * 是否设置支付密码
                        0-未设置；
                        1-已设置；
	 */
	private Integer isSetPayPassword;
	/**
	 * 支付密码
	 */
	private String payPassword;
	/**
	 * 身份认证级别
                        0-未认证；
                        1-初级认证；
                        2-高级认证；
	 */
	private Integer authLevel;
	/**
	 * 姓名
	 */
	private String realName;
	/**
	 * 证件类型
                        1-身份证；
                        2-军官证；
                        3-护照；
                        4-台湾居民通行证；
                        5-港澳居民通行证；
                        9-其他；
	 */
	private Integer idCardType;
	/**
	 * 证件号码
	 */
	private String idCard;
	/**
	 * 认证时间
	 */
	private Date authTime;
	/**
	 * 状态
                        0-锁定
                        1-有效
	 */
	private Integer status;
	/**
	 * 邀请码
	 */
	private String invitionCode;
	/**
	 * 邀请关系
	 */
	private String inviteRelationship;
	/**
	 * 邀请人ID
	 */
	private Long inviterId;
	/**
	 * 是否开通HHT抵扣手续费
                        0-未开通；
                        1-已开通；
	 */
	private Integer isDeductible;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：主键
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * 获取：用户名
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * 获取：手机号
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * 设置：邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * 获取：邮箱
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * 设置：密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * 获取：密码
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * 设置：头像地址
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	/**
	 * 获取：头像地址
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * 设置：是否设置支付密码
                        0-未设置；
                        1-已设置；
	 */
	public void setIsSetPayPassword(Integer isSetPayPassword) {
		this.isSetPayPassword = isSetPayPassword;
	}
	/**
	 * 获取：是否设置支付密码
                        0-未设置；
                        1-已设置；
	 */
	public Integer getIsSetPayPassword() {
		return isSetPayPassword;
	}
	/**
	 * 设置：支付密码
	 */
	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}
	/**
	 * 获取：支付密码
	 */
	public String getPayPassword() {
		return payPassword;
	}
	/**
	 * 设置：身份认证级别
                        0-未认证；
                        1-初级认证；
                        2-高级认证；
	 */
	public void setAuthLevel(Integer authLevel) {
		this.authLevel = authLevel;
	}
	/**
	 * 获取：身份认证级别
                        0-未认证；
                        1-初级认证；
                        2-高级认证；
	 */
	public Integer getAuthLevel() {
		return authLevel;
	}
	/**
	 * 设置：姓名
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	/**
	 * 获取：姓名
	 */
	public String getRealName() {
		return realName;
	}
	/**
	 * 设置：证件类型
                        1-身份证；
                        2-军官证；
                        3-护照；
                        4-台湾居民通行证；
                        5-港澳居民通行证；
                        9-其他；
	 */
	public void setIdCardType(Integer idCardType) {
		this.idCardType = idCardType;
	}
	/**
	 * 获取：证件类型
                        1-身份证；
                        2-军官证；
                        3-护照；
                        4-台湾居民通行证；
                        5-港澳居民通行证；
                        9-其他；
	 */
	public Integer getIdCardType() {
		return idCardType;
	}
	/**
	 * 设置：证件号码
	 */
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	/**
	 * 获取：证件号码
	 */
	public String getIdCard() {
		return idCard;
	}
	/**
	 * 设置：认证时间
	 */
	public void setAuthTime(Date authTime) {
		this.authTime = authTime;
	}
	/**
	 * 获取：认证时间
	 */
	public Date getAuthTime() {
		return authTime;
	}
	/**
	 * 设置：状态
                        0-锁定
                        1-有效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-锁定
                        1-有效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：邀请码
	 */
	public void setInvitionCode(String invitionCode) {
		this.invitionCode = invitionCode;
	}
	/**
	 * 获取：邀请码
	 */
	public String getInvitionCode() {
		return invitionCode;
	}
	/**
	 * 设置：邀请关系
	 */
	public void setInviteRelationship(String inviteRelationship) {
		this.inviteRelationship = inviteRelationship;
	}
	/**
	 * 获取：邀请关系
	 */
	public String getInviteRelationship() {
		return inviteRelationship;
	}
	/**
	 * 设置：邀请人ID
	 */
	public void setInviterId(Long inviterId) {
		this.inviterId = inviterId;
	}
	/**
	 * 获取：邀请人ID
	 */
	public Long getInviterId() {
		return inviterId;
	}
	/**
	 * 设置：是否开通HHT抵扣手续费
                        0-未开通；
                        1-已开通；
	 */
	public void setIsDeductible(Integer isDeductible) {
		this.isDeductible = isDeductible;
	}
	/**
	 * 获取：是否开通HHT抵扣手续费
                        0-未开通；
                        1-已开通；
	 */
	public Integer getIsDeductible() {
		return isDeductible;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
