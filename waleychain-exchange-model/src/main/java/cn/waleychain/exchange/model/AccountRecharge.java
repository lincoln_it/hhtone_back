package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseAccountRecharge;

/**
 * 人民币充值记录
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:38
 */
public class AccountRecharge extends BaseAccountRecharge {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户名
	 */
	private String userName;
	
	/**
	 * 真实姓名
	 */
	private String realName;
	
	/**
	 * 资金账户流水标识ID
	 */
	private Long changeId;
	
	/**
	 * 用户资金账号ID
	 */
	private Long accountId;
	
	/**
	 * 状态
	 */
	private Integer status;
	
	/**
	 * 备注
	 */
	private String remark;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Long getChangeId() {
		return changeId;
	}

	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
