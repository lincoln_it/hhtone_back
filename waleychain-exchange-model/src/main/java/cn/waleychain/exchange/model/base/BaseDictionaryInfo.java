package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 系统码表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-05-03 09:52:51
 */
public class BaseDictionaryInfo extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "dictionary_info";
	
	/**
	 * 主键
	 */
	private Long idNo;
	/**
	 * 配置类型
1 系统配置 2 业务配置 3 交易区域
	 */
	private Long typeId;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 码
	 */
	private String code;
	/**
	 * 码值
	 */
	private String value;
	/**
	 * 值类型（1 string 2 int 3 boolean）
	 */
	private Integer valueType;
	/**
	 * 状态
                        0-无效；
                        1-有效；
	 */
	private Integer status;
	/**
	 * 描述
	 */
	private String remark;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：配置类型
1 系统配置 2 业务配置 3 交易区域
	 */
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	/**
	 * 获取：配置类型
1 系统配置 2 业务配置 3 交易区域
	 */
	public Long getTypeId() {
		return typeId;
	}
	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：码
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 获取：码
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：码值
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * 获取：码值
	 */
	public String getValue() {
		return value;
	}
	/**
	 * 设置：值类型（1 string 2 int 3 boolean）
	 */
	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}
	/**
	 * 获取：值类型（1 string 2 int 3 boolean）
	 */
	public Integer getValueType() {
		return valueType;
	}
	/**
	 * 设置：状态
                        0-无效；
                        1-有效；
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-无效；
                        1-有效；
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：描述
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：描述
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
