package cn.waleychain.exchange.model;

import java.math.BigDecimal;

import cn.waleychain.exchange.model.base.BaseEntrustOrder;

/**
 * 委托订单
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:40
 */
public class EntrustOrder extends BaseEntrustOrder {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户名
	 */
	private String userName;
	
	/**
	 * 手机号
	 */
	private String mobile;
	
	/**
	 * 市场title
	 */
	private String marketTitle;
	
	/**
	 * 市场名
	 */
	private String marketName;
	
	/**
	 * 市场买方币种ID
	 */
	private Long buyCoinId;
	
	/**
	 * 市场卖方市场ID
	 */
	private Long sellCoinId;
	
	/**
	 * 未成交量
	 */
	private BigDecimal notNum;
	
	/**
	 * 未成交总额
	 */
	private BigDecimal notTotal;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMarketTitle() {
		return marketTitle;
	}

	public void setMarketTitle(String marketTitle) {
		this.marketTitle = marketTitle;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public Long getBuyCoinId() {
		return buyCoinId;
	}

	public void setBuyCoinId(Long buyCoinId) {
		this.buyCoinId = buyCoinId;
	}

	public Long getSellCoinId() {
		return sellCoinId;
	}

	public void setSellCoinId(Long sellCoinId) {
		this.sellCoinId = sellCoinId;
	}

	public BigDecimal getNotNum() {
		return notNum;
	}

	public void setNotNum(BigDecimal notNum) {
		this.notNum = notNum;
	}

	public BigDecimal getNotTotal() {
		return notTotal;
	}

	public void setNotTotal(BigDecimal notTotal) {
		this.notTotal = notTotal;
	}
}
