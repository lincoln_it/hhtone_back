package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseDealOrder;

/**
 * 成交订单
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:39
 */
public class DealOrder extends BaseDealOrder {

	private static final long serialVersionUID = 1L;

}
