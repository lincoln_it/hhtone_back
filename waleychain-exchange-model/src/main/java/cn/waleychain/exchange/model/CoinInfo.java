package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseCoinInfo;

/**
 * 币种信息
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:39
 */
public class CoinInfo extends BaseCoinInfo {

	private static final long serialVersionUID = 1L;

	private String iconResUrl;

	public String getIconResUrl() {
		return iconResUrl;
	}

	public void setIconResUrl(String iconResUrl) {
		this.iconResUrl = iconResUrl;
	}
	
	
}
