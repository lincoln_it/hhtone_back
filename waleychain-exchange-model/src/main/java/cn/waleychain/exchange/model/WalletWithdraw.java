package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseWalletWithdraw;

/**
 * 钱包提现
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:43
 */
public class WalletWithdraw extends BaseWalletWithdraw {

	private static final long serialVersionUID = 1L;

	/**
	 * 操作流水标识ID
	 */
	private Long walletChangeId;
	
	/**
	 * 提现状态
	 */
	private Integer status;
	
	/**
	 * 说明
	 */
	private String remark;
	
	private String userName;
	
	private String realName;
	
	private String mobile;
	
	private String coinTitle;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getWalletChangeId() {
		return walletChangeId;
	}

	public void setWalletChangeId(Long walletChangeId) {
		this.walletChangeId = walletChangeId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCoinTitle() {
		return coinTitle;
	}

	public void setCoinTitle(String coinTitle) {
		this.coinTitle = coinTitle;
	}
	
}
