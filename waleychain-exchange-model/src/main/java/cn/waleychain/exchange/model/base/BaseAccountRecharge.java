package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 人民币充值记录
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:20
 */
public class BaseAccountRecharge extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "account_recharge";
	
	/**
	 * 主键
	 */
	private Long idNo;
	/**
	 * 用户账户ID
	 */
	private Long accountId;
	/**
	 * 充值金额
	 */
	private BigDecimal amount;
	/**
	 * 实际到账金额
	 */
	private BigDecimal amountReal;
	/**
	 * 手续费
	 */
	private BigDecimal fee;
	/**
	 * 类型
                        1-银联支付
                        2-支付宝支付; 3-微信支付; 4-线下支付
	 */
	private Integer type;
	/**
	 * 序列号
	 */
	private String tradeNo;
	/**
	 * 第三方流水号
	 */
	private String tradeNoOut;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：用户账户ID
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	/**
	 * 获取：用户账户ID
	 */
	public Long getAccountId() {
		return accountId;
	}
	/**
	 * 设置：充值金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：充值金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：实际到账金额
	 */
	public void setAmountReal(BigDecimal amountReal) {
		this.amountReal = amountReal;
	}
	/**
	 * 获取：实际到账金额
	 */
	public BigDecimal getAmountReal() {
		return amountReal;
	}
	/**
	 * 设置：手续费
	 */
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	/**
	 * 获取：手续费
	 */
	public BigDecimal getFee() {
		return fee;
	}
	/**
	 * 设置：类型
                        1-银联支付
                        2-支付宝支付; 3-微信支付; 4-线下支付
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：类型
                        1-银联支付
                        2-支付宝支付; 3-微信支付; 4-线下支付
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：序列号
	 */
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	/**
	 * 获取：序列号
	 */
	public String getTradeNo() {
		return tradeNo;
	}
	/**
	 * 设置：第三方流水号
	 */
	public void setTradeNoOut(String tradeNoOut) {
		this.tradeNoOut = tradeNoOut;
	}
	/**
	 * 获取：第三方流水号
	 */
	public String getTradeNoOut() {
		return tradeNoOut;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
