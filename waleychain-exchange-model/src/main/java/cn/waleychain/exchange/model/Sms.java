package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseSms;

/**
 * 用户短信记录
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:41
 */
public class Sms extends BaseSms {

	private static final long serialVersionUID = 1L;

}
