package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseCoinWallet;

/**
 * 用户平台钱包
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:39
 */
public class CoinWallet extends BaseCoinWallet {

	private static final long serialVersionUID = 1L;

	private String userName;
	
	private String realName;
	
	private String mobile;
	
	/**
	 * 币种标题
	 */
	private String coinTitle;
	
	/**
	 * 是否允许提现
            0-禁止提现；
            1-可提现；
            2-提现缓慢；
	 */
	private Integer isAllowWithdraw;
	
	/**
	 * 是否允许充值
            0-禁止充值；
            1-可充值；
            2-充值缓慢；
	 */
	private Integer isAllowRecharge;

	public String getCoinTitle() {
		return coinTitle;
	}

	public void setCoinTitle(String coinTitle) {
		this.coinTitle = coinTitle;
	}

	public Integer getIsAllowWithdraw() {
		return isAllowWithdraw;
	}

	public void setIsAllowWithdraw(Integer isAllowWithdraw) {
		this.isAllowWithdraw = isAllowWithdraw;
	}

	public Integer getIsAllowRecharge() {
		return isAllowRecharge;
	}

	public void setIsAllowRecharge(Integer isAllowRecharge) {
		this.isAllowRecharge = isAllowRecharge;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
}
