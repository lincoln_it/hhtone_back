package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 钱包提现
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:26
 */
public class BaseWalletWithdraw extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "wallet_withdraw";
	
	/**
	 * 主键
	 */
	private Long idNo;
	/**
	 * 提现钱包地址
	 */
	private String address;
	/**
	 * 类型
                        1-站外
                        2-站内
	 */
	private Integer type;
	/**
	 * 钱包标识ID
	 */
	private Long coinWalletId;
	/**
	 * 交易金额
	 */
	private BigDecimal amount;
	/**
	 * 提现手续费费率
	 */
	private BigDecimal feeRate;
	/**
	 * 提现手续费
	 */
	private BigDecimal fee;
	/**
	 * 实际到账金额
	 */
	private BigDecimal realAmount;
	/**
	 * 区块高度
	 */
	private Integer blockHeight;
	/**
	 * 交易id
	 */
	private String txId;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：提现钱包地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：提现钱包地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：类型
                        1-站外
                        2-站内
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：类型
                        1-站外
                        2-站内
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：钱包标识ID
	 */
	public void setCoinWalletId(Long coinWalletId) {
		this.coinWalletId = coinWalletId;
	}
	/**
	 * 获取：钱包标识ID
	 */
	public Long getCoinWalletId() {
		return coinWalletId;
	}
	/**
	 * 设置：交易金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：提现手续费费率
	 */
	public void setFeeRate(BigDecimal feeRate) {
		this.feeRate = feeRate;
	}
	/**
	 * 获取：提现手续费费率
	 */
	public BigDecimal getFeeRate() {
		return feeRate;
	}
	/**
	 * 设置：提现手续费
	 */
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	/**
	 * 获取：提现手续费
	 */
	public BigDecimal getFee() {
		return fee;
	}
	/**
	 * 设置：实际到账金额
	 */
	public void setRealAmount(BigDecimal realAmount) {
		this.realAmount = realAmount;
	}
	/**
	 * 获取：实际到账金额
	 */
	public BigDecimal getRealAmount() {
		return realAmount;
	}
	/**
	 * 设置：区块高度
	 */
	public void setBlockHeight(Integer blockHeight) {
		this.blockHeight = blockHeight;
	}
	/**
	 * 获取：区块高度
	 */
	public Integer getBlockHeight() {
		return blockHeight;
	}
	/**
	 * 设置：交易id
	 */
	public void setTxId(String txId) {
		this.txId = txId;
	}
	/**
	 * 获取：交易id
	 */
	public String getTxId() {
		return txId;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
