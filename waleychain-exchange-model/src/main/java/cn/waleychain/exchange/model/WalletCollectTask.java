package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseWalletCollectTask;

/**
 * 钱包转账定时任务
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-28 16:53:47
 */
public class WalletCollectTask extends BaseWalletCollectTask {

	private static final long serialVersionUID = 1L;

}
