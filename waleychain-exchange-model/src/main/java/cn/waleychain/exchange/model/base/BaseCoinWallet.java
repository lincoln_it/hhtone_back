package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 用户平台钱包
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-28 16:53:47
 */
public class BaseCoinWallet extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "coin_wallet";
	
	/**
	 * 主键
	 */
	private Long coinWalletId;
	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 币种ID
	 */
	private Long coinId;
	/**
	 * 钱包地址
	 */
	private String address;
	/**
	 * 账户余额
	 */
	private BigDecimal balance;
	/**
	 * 币种冻结金额
	 */
	private BigDecimal freeze;
	/**
	 * 累计充值金额
	 */
	private BigDecimal recharge;
	/**
	 * 累计提现金额
	 */
	private BigDecimal withdraw;
	/**
	 * keystore
	 */
	private String keystore;
	/**
	 * password
	 */
	private String password;
	/**
	 * 钱包状态
                        0-锁定
                        1-有效
	 */
	private Integer status;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setCoinWalletId(Long coinWalletId) {
		this.coinWalletId = coinWalletId;
	}
	/**
	 * 获取：主键
	 */
	public Long getCoinWalletId() {
		return coinWalletId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：币种ID
	 */
	public void setCoinId(Long coinId) {
		this.coinId = coinId;
	}
	/**
	 * 获取：币种ID
	 */
	public Long getCoinId() {
		return coinId;
	}
	/**
	 * 设置：钱包地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：钱包地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：账户余额
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * 获取：账户余额
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * 设置：币种冻结金额
	 */
	public void setFreeze(BigDecimal freeze) {
		this.freeze = freeze;
	}
	/**
	 * 获取：币种冻结金额
	 */
	public BigDecimal getFreeze() {
		return freeze;
	}
	/**
	 * 设置：累计充值金额
	 */
	public void setRecharge(BigDecimal recharge) {
		this.recharge = recharge;
	}
	/**
	 * 获取：累计充值金额
	 */
	public BigDecimal getRecharge() {
		return recharge;
	}
	/**
	 * 设置：累计提现金额
	 */
	public void setWithdraw(BigDecimal withdraw) {
		this.withdraw = withdraw;
	}
	/**
	 * 获取：累计提现金额
	 */
	public BigDecimal getWithdraw() {
		return withdraw;
	}
	/**
	 * 设置：keystore
	 */
	public void setKeystore(String keystore) {
		this.keystore = keystore;
	}
	/**
	 * 获取：keystore
	 */
	public String getKeystore() {
		return keystore;
	}
	/**
	 * 设置：password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * 获取：password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * 设置：钱包状态
                        0-锁定
                        1-有效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：钱包状态
                        0-锁定
                        1-有效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
