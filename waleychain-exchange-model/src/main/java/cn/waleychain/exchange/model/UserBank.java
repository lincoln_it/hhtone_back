package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseUserBank;

/**
 * 用户绑卡信息表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:42
 */
public class UserBank extends BaseUserBank {

	private static final long serialVersionUID = 1L;

}
