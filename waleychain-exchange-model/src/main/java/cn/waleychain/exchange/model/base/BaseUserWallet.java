package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 用户提现钱包
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:26
 */
public class BaseUserWallet extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "user_wallet";
	
	/**
	 * 主键
	 */
	private Long userWalletId;
	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 币种名称
	 */
	private Long coinId;
	/**
	 * 钱包名称
	 */
	private String name;
	/**
	 * 钱包地址
	 */
	private String address;
	/**
	 * 备注
	 */
	private String memo;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 状态
                        0-无效；
                        1-有效
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setUserWalletId(Long userWalletId) {
		this.userWalletId = userWalletId;
	}
	/**
	 * 获取：主键
	 */
	public Long getUserWalletId() {
		return userWalletId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：币种名称
	 */
	public void setCoinId(Long coinId) {
		this.coinId = coinId;
	}
	/**
	 * 获取：币种名称
	 */
	public Long getCoinId() {
		return coinId;
	}
	/**
	 * 设置：钱包名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：钱包名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：钱包地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：钱包地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：备注
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	/**
	 * 获取：备注
	 */
	public String getMemo() {
		return memo;
	}
	/**
	 * 设置：排序
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSort() {
		return sort;
	}
	/**
	 * 设置：状态
                        0-无效；
                        1-有效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-无效；
                        1-有效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
