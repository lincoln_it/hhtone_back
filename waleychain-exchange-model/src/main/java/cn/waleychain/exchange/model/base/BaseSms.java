package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 用户短信记录
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:25
 */
public class BaseSms extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "sms";
	
	/**
	 * 主键
	 */
	private Long idNo;
	/**
	 * 短信模板ID
	 */
	private Long templateCode;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 短信内容
	 */
	private String content;
	/**
	 * 状态
                        0-失败；
                        1-成功
	 */
	private Integer status;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 短信发送时间
	 */
	private Date created;

	/**
	 * 设置：主键
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：短信模板ID
	 */
	public void setTemplateCode(Long templateCode) {
		this.templateCode = templateCode;
	}
	/**
	 * 获取：短信模板ID
	 */
	public Long getTemplateCode() {
		return templateCode;
	}
	/**
	 * 设置：手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * 获取：手机号
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * 设置：短信内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：短信内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：状态
                        0-失败；
                        1-成功
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-失败；
                        1-成功
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：短信发送时间
	 */
	public void setCreated(Date created) {
		this.created = created;
	}
	/**
	 * 获取：短信发送时间
	 */
	public Date getCreated() {
		return created;
	}
}
