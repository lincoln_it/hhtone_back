package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 钱包数据流水记录（?该表只保留核心字段，用视图实现统计流水功能）
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:21
 */
public class BaseCoinWalletChange extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "coin_wallet_change";
	
	/**
	 * 主键
	 */
	private Long idNo;
	/**
	 * 
	 */
	private Long coinWalletId;
	/**
	 * 业务类型， 分别对应1:entrust_order 2.deal_order 3. coin_withdraw 4 coin_recharge
	 */
	private Integer bizType;
	/**
	 * 业务表id
	 */
	private Long bizTypeId;
	/**
	 * 收入、支出状态：1表示收入  2表示支出
	 */
	private Integer type;
	/**
	 * 业务类型
                        30-提现审核中；
                        31-提现通过；
                        32-提现拒绝；
                        40-充值失败；
                        41-充值成功
	 */
	private Integer status;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 
	 */
	private Date createTime;

	/**
	 * 设置：主键
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：
	 */
	public void setCoinWalletId(Long coinWalletId) {
		this.coinWalletId = coinWalletId;
	}
	/**
	 * 获取：
	 */
	public Long getCoinWalletId() {
		return coinWalletId;
	}
	/**
	 * 设置：业务类型， 分别对应1:entrust_order 2.deal_order 3. coin_withdraw 4 coin_recharge
	 */
	public void setBizType(Integer bizType) {
		this.bizType = bizType;
	}
	/**
	 * 获取：业务类型， 分别对应1:entrust_order 2.deal_order 3. coin_withdraw 4 coin_recharge
	 */
	public Integer getBizType() {
		return bizType;
	}
	/**
	 * 设置：业务表id
	 */
	public void setBizTypeId(Long bizTypeId) {
		this.bizTypeId = bizTypeId;
	}
	/**
	 * 获取：业务表id
	 */
	public Long getBizTypeId() {
		return bizTypeId;
	}
	/**
	 * 设置：收入、支出状态：1表示收入  2表示支出
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：收入、支出状态：1表示收入  2表示支出
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：业务类型
                        30-提现审核中；
                        31-提现通过；
                        32-提现拒绝；
                        40-充值失败；
                        41-充值成功
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：业务类型
                        30-提现审核中；
                        31-提现通过；
                        32-提现拒绝；
                        40-充值失败；
                        41-充值成功
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
