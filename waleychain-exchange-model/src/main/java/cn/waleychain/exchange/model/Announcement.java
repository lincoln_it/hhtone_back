package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseAnnouncement;

/**
 * 系统公告
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:38
 */
public class Announcement extends BaseAnnouncement {

	private static final long serialVersionUID = 1L;

}
