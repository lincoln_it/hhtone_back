package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseAccountChange;

/**
 * 资金账户流水记录（?该表只保留核心字段，用视图实现统计流水功能）
通过biz_type和biz_type_i
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:38
 */
public class AccountChange extends BaseAccountChange {

	private static final long serialVersionUID = 1L;

}
