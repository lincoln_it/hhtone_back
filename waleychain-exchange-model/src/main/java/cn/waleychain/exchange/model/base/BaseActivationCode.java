package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 用户邮箱注册激活码
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-05-02 12:06:55
 */
public class BaseActivationCode extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "activation_code";
	
	/**
	 * 用户标识ID
	 */
	private Long userId;
	/**
	 * 激活码
	 */
	private String code;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 状态：
            0 未验证
            1 已验证
	 */
	private Integer status;

	/**
	 * 设置：用户标识ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户标识ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：激活码
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 获取：激活码
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：状态：
            0 未验证
            1 已验证
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态：
            0 未验证
            1 已验证
	 */
	public Integer getStatus() {
		return status;
	}
}
