package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 成交订单
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:22
 */
public class BaseDealOrder extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "deal_order";
	
	/**
	 * 主键
	 */
	private Long dealId;
	/**
	 * 市场ID
	 */
	private Long marketId;
	/**
	 * 委托买单订单ID
	 */
	private Long buyOrderId;
	/**
	 * 委托卖单订单ID
	 */
	private Long sellOrderId;
	/**
	 * 成交价格
	 */
	private BigDecimal dealPrice;
	/**
	 * 成交数量
	 */
	private BigDecimal dealNumber;
	/**
	 * 成交总额
	 */
	private BigDecimal dealAmount;
	/**
	 * 买手续费费率
	 */
	private BigDecimal dealBuyFeeRate;
	/**
	 * 买手续费
	 */
	private BigDecimal dealBuyFee;
	/**
	 * 卖手续费费率
	 */
	private BigDecimal dealSellFeeRate;
	/**
	 * 卖手续费
	 */
	private BigDecimal dealSellFee;
	/**
	 * 状态
                        0-未成交;
                        1-已成交;
                        2-已取消;
                        3-异常单;
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setDealId(Long dealId) {
		this.dealId = dealId;
	}
	/**
	 * 获取：主键
	 */
	public Long getDealId() {
		return dealId;
	}
	/**
	 * 设置：市场ID
	 */
	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
	/**
	 * 获取：市场ID
	 */
	public Long getMarketId() {
		return marketId;
	}
	/**
	 * 设置：委托买单订单ID
	 */
	public void setBuyOrderId(Long buyOrderId) {
		this.buyOrderId = buyOrderId;
	}
	/**
	 * 获取：委托买单订单ID
	 */
	public Long getBuyOrderId() {
		return buyOrderId;
	}
	/**
	 * 设置：委托卖单订单ID
	 */
	public void setSellOrderId(Long sellOrderId) {
		this.sellOrderId = sellOrderId;
	}
	/**
	 * 获取：委托卖单订单ID
	 */
	public Long getSellOrderId() {
		return sellOrderId;
	}
	/**
	 * 设置：成交价格
	 */
	public void setDealPrice(BigDecimal dealPrice) {
		this.dealPrice = dealPrice;
	}
	/**
	 * 获取：成交价格
	 */
	public BigDecimal getDealPrice() {
		return dealPrice;
	}
	/**
	 * 设置：成交数量
	 */
	public void setDealNumber(BigDecimal dealNumber) {
		this.dealNumber = dealNumber;
	}
	/**
	 * 获取：成交数量
	 */
	public BigDecimal getDealNumber() {
		return dealNumber;
	}
	/**
	 * 设置：成交总额
	 */
	public void setDealAmount(BigDecimal dealAmount) {
		this.dealAmount = dealAmount;
	}
	/**
	 * 获取：成交总额
	 */
	public BigDecimal getDealAmount() {
		return dealAmount;
	}
	/**
	 * 设置：买手续费费率
	 */
	public void setDealBuyFeeRate(BigDecimal dealBuyFeeRate) {
		this.dealBuyFeeRate = dealBuyFeeRate;
	}
	/**
	 * 获取：买手续费费率
	 */
	public BigDecimal getDealBuyFeeRate() {
		return dealBuyFeeRate;
	}
	/**
	 * 设置：买手续费
	 */
	public void setDealBuyFee(BigDecimal dealBuyFee) {
		this.dealBuyFee = dealBuyFee;
	}
	/**
	 * 获取：买手续费
	 */
	public BigDecimal getDealBuyFee() {
		return dealBuyFee;
	}
	/**
	 * 设置：卖手续费费率
	 */
	public void setDealSellFeeRate(BigDecimal dealSellFeeRate) {
		this.dealSellFeeRate = dealSellFeeRate;
	}
	/**
	 * 获取：卖手续费费率
	 */
	public BigDecimal getDealSellFeeRate() {
		return dealSellFeeRate;
	}
	/**
	 * 设置：卖手续费
	 */
	public void setDealSellFee(BigDecimal dealSellFee) {
		this.dealSellFee = dealSellFee;
	}
	/**
	 * 获取：卖手续费
	 */
	public BigDecimal getDealSellFee() {
		return dealSellFee;
	}
	/**
	 * 设置：状态
                        0-未成交;
                        1-已成交;
                        2-已取消;
                        3-异常单;
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-未成交;
                        1-已成交;
                        2-已取消;
                        3-异常单;
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
