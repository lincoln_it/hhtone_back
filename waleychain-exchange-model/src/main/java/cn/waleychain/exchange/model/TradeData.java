package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseTradeData;

/**
 * 市场行情
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-23 16:18:55
 */
public class TradeData extends BaseTradeData {

	private static final long serialVersionUID = 1L;

}
