package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 市场行情
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-23 16:18:55
 */
public class BaseTradeData extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "trade_data";
	
	/**
	 * 标识ID
	 */
	private Long idNo;
	/**
	 * 市场ID
	 */
	private Long marketId;
	/**
	 * 成交价
	 */
	private BigDecimal tradePrice;
	/**
	 * 成交数量
	 */
	private BigDecimal tradeNum;
	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 设置：标识ID
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：标识ID
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：市场ID
	 */
	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
	/**
	 * 获取：市场ID
	 */
	public Long getMarketId() {
		return marketId;
	}
	/**
	 * 设置：成交价
	 */
	public void setTradePrice(BigDecimal tradePrice) {
		this.tradePrice = tradePrice;
	}
	/**
	 * 获取：成交价
	 */
	public BigDecimal getTradePrice() {
		return tradePrice;
	}
	/**
	 * 设置：成交数量
	 */
	public void setTradeNum(BigDecimal tradeNum) {
		this.tradeNum = tradeNum;
	}
	/**
	 * 获取：成交数量
	 */
	public BigDecimal getTradeNum() {
		return tradeNum;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
