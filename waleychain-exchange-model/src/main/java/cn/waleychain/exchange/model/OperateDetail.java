package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseOperateDetail;

/**
 * 相关操作流水
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-18 14:23:13
 */
public class OperateDetail extends BaseOperateDetail {

	private static final long serialVersionUID = 1L;

}
