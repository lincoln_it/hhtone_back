package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 币种信息
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:21
 */
public class BaseCoinInfo extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "coin_info";
	
	/**
	 * 主键
	 */
	private Long coinId;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 币种图标
	 */
	private Long iconResId;
	/**
	 * 币种类型
                        1-钱包币；
                        2-认购币；
	 */
	private Integer coinType;
	/**
	 * 钱包类型
                        0-其他；
                        1-比特币系列；
                        2-以太坊系列；
                        3-以太坊代币；
	 */
	private Integer walletType;
	/**
	 * 小数位
	 */
	private Integer scale;
	/**
	 * 充值当前扫描区块高度
	 */
	private Integer scanBlockHeight;
	/**
	 * 钱包服务器IP
	 */
	private String rpcIp;
	/**
	 * 钱包服务器端口
	 */
	private Integer rpcPort;
	/**
	 * 钱包服务器用户名
	 */
	private String rpcUser;
	/**
	 * 钱包服务器密码
	 */
	private String rpcPassword;
	/**
	 * 合约地址
	 */
	private String contractAddress;
	/**
	 * 单位
	 */
	private Integer amountUnit;
	/**
	 * ERC20 ABI
	 */
	private String abi;
	/**
	 * 最低归账个数
	 */
	private Integer debtMinMun;
	/**
	 * 提现最低手续费
	 */
	private BigDecimal minFee;
	/**
	 * 提现手续费率
	 */
	private BigDecimal feeRate;
	/**
	 * 自动提现额度
	 */
	private BigDecimal autoWithdraw;
	/**
	 * 单笔提现最大额度
	 */
	private BigDecimal singleLimit;
	/**
	 * 当日提现最大额度
	 */
	private BigDecimal dayLimit;
	/**
	 * 是否允许提现
                        0-禁止提现；
                        1-可提现；
                        2-提现缓慢；
	 */
	private Integer isAllowWithdraw;
	/**
	 * 是否允许充值
                        0-禁止充值；
                        1-可充值；
                        2-充值缓慢；
	 */
	private Integer isAllowRecharge;
	/**
	 * 是否允许交易
             0-不可交易；
             1-可交易；

	 */
	private Integer isAllowTrade;
	/**
	 * 最后一个块
	 */
	private String lastblock;
	/**
	 * 状态
                        0-无效；
                        1-有效；
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setCoinId(Long coinId) {
		this.coinId = coinId;
	}
	/**
	 * 获取：主键
	 */
	public Long getCoinId() {
		return coinId;
	}
	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：标题
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：币种图标
	 */
	public void setIconResId(Long iconResId) {
		this.iconResId = iconResId;
	}
	/**
	 * 获取：币种图标
	 */
	public Long getIconResId() {
		return iconResId;
	}
	/**
	 * 设置：币种类型
                        1-钱包币；
                        2-认购币；
	 */
	public void setCoinType(Integer coinType) {
		this.coinType = coinType;
	}
	/**
	 * 获取：币种类型
                        1-钱包币；
                        2-认购币；
	 */
	public Integer getCoinType() {
		return coinType;
	}
	/**
	 * 设置：钱包类型
                        0-其他；
                        1-比特币系列；
                        2-以太坊系列；
                        3-以太坊代币；
	 */
	public void setWalletType(Integer walletType) {
		this.walletType = walletType;
	}
	/**
	 * 获取：钱包类型
                        0-其他；
                        1-比特币系列；
                        2-以太坊系列；
                        3-以太坊代币；
	 */
	public Integer getWalletType() {
		return walletType;
	}
	/**
	 * 设置：小数位
	 */
	public void setScale(Integer scale) {
		this.scale = scale;
	}
	/**
	 * 获取：小数位
	 */
	public Integer getScale() {
		return scale;
	}
	/**
	 * 设置：充值当前扫描区块高度
	 */
	public void setScanBlockHeight(Integer scanBlockHeight) {
		this.scanBlockHeight = scanBlockHeight;
	}
	/**
	 * 获取：充值当前扫描区块高度
	 */
	public Integer getScanBlockHeight() {
		return scanBlockHeight;
	}
	/**
	 * 设置：钱包服务器IP
	 */
	public void setRpcIp(String rpcIp) {
		this.rpcIp = rpcIp;
	}
	/**
	 * 获取：钱包服务器IP
	 */
	public String getRpcIp() {
		return rpcIp;
	}
	/**
	 * 设置：钱包服务器端口
	 */
	public void setRpcPort(Integer rpcPort) {
		this.rpcPort = rpcPort;
	}
	/**
	 * 获取：钱包服务器端口
	 */
	public Integer getRpcPort() {
		return rpcPort;
	}
	/**
	 * 设置：钱包服务器用户名
	 */
	public void setRpcUser(String rpcUser) {
		this.rpcUser = rpcUser;
	}
	/**
	 * 获取：钱包服务器用户名
	 */
	public String getRpcUser() {
		return rpcUser;
	}
	/**
	 * 设置：钱包服务器密码
	 */
	public void setRpcPassword(String rpcPassword) {
		this.rpcPassword = rpcPassword;
	}
	/**
	 * 获取：钱包服务器密码
	 */
	public String getRpcPassword() {
		return rpcPassword;
	}
	/**
	 * 设置：合约地址
	 */
	public void setContractAddress(String contractAddress) {
		this.contractAddress = contractAddress;
	}
	/**
	 * 获取：合约地址
	 */
	public String getContractAddress() {
		return contractAddress;
	}
	/**
	 * 设置：单位
	 */
	public void setAmountUnit(Integer amountUnit) {
		this.amountUnit = amountUnit;
	}
	/**
	 * 获取：单位
	 */
	public Integer getAmountUnit() {
		return amountUnit;
	}
	/**
	 * 设置：ERC20 ABI
	 */
	public void setAbi(String abi) {
		this.abi = abi;
	}
	/**
	 * 获取：ERC20 ABI
	 */
	public String getAbi() {
		return abi;
	}
	/**
	 * 设置：最低归账个数
	 */
	public void setDebtMinMun(Integer debtMinMun) {
		this.debtMinMun = debtMinMun;
	}
	/**
	 * 获取：最低归账个数
	 */
	public Integer getDebtMinMun() {
		return debtMinMun;
	}
	/**
	 * 设置：提现最低手续费
	 */
	public void setMinFee(BigDecimal minFee) {
		this.minFee = minFee;
	}
	/**
	 * 获取：提现最低手续费
	 */
	public BigDecimal getMinFee() {
		return minFee;
	}
	/**
	 * 设置：提现手续费率
	 */
	public void setFeeRate(BigDecimal feeRate) {
		this.feeRate = feeRate;
	}
	/**
	 * 获取：提现手续费率
	 */
	public BigDecimal getFeeRate() {
		return feeRate;
	}
	/**
	 * 设置：自动提现额度
	 */
	public void setAutoWithdraw(BigDecimal autoWithdraw) {
		this.autoWithdraw = autoWithdraw;
	}
	/**
	 * 获取：自动提现额度
	 */
	public BigDecimal getAutoWithdraw() {
		return autoWithdraw;
	}
	/**
	 * 设置：单笔提现最大额度
	 */
	public void setSingleLimit(BigDecimal singleLimit) {
		this.singleLimit = singleLimit;
	}
	/**
	 * 获取：单笔提现最大额度
	 */
	public BigDecimal getSingleLimit() {
		return singleLimit;
	}
	/**
	 * 设置：当日提现最大额度
	 */
	public void setDayLimit(BigDecimal dayLimit) {
		this.dayLimit = dayLimit;
	}
	/**
	 * 获取：当日提现最大额度
	 */
	public BigDecimal getDayLimit() {
		return dayLimit;
	}
	/**
	 * 设置：是否允许提现
                        0-禁止提现；
                        1-可提现；
                        2-提现缓慢；
	 */
	public void setIsAllowWithdraw(Integer isAllowWithdraw) {
		this.isAllowWithdraw = isAllowWithdraw;
	}
	/**
	 * 获取：是否允许提现
                        0-禁止提现；
                        1-可提现；
                        2-提现缓慢；
	 */
	public Integer getIsAllowWithdraw() {
		return isAllowWithdraw;
	}
	/**
	 * 设置：是否允许充值
                        0-禁止充值；
                        1-可充值；
                        2-充值缓慢；
	 */
	public void setIsAllowRecharge(Integer isAllowRecharge) {
		this.isAllowRecharge = isAllowRecharge;
	}
	/**
	 * 获取：是否允许充值
                        0-禁止充值；
                        1-可充值；
                        2-充值缓慢；
	 */
	public Integer getIsAllowRecharge() {
		return isAllowRecharge;
	}
	/**
	 * 设置：是否允许交易
             0-不可交易；
             1-可交易；

	 */
	public void setIsAllowTrade(Integer isAllowTrade) {
		this.isAllowTrade = isAllowTrade;
	}
	/**
	 * 获取：是否允许交易
             0-不可交易；
             1-可交易；

	 */
	public Integer getIsAllowTrade() {
		return isAllowTrade;
	}
	/**
	 * 设置：最后一个块
	 */
	public void setLastblock(String lastblock) {
		this.lastblock = lastblock;
	}
	/**
	 * 获取：最后一个块
	 */
	public String getLastblock() {
		return lastblock;
	}
	/**
	 * 设置：状态
                        0-无效；
                        1-有效；
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-无效；
                        1-有效；
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
