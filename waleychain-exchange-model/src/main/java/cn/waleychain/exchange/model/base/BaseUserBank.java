package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;

/**
 * 用户绑卡信息表
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:25
 */
public class BaseUserBank extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "user_bank";
	
	/**
	 * 主键
	 */
	private Long userBankId;
	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 开户行名称
	 */
	private String bankName;
	/**
	 * 卡号
	 */
	private String bankCard;
	/**
	 * 开户行省
	 */
	private String privince;
	/**
	 * 开户行市
	 */
	private String city;
	/**
	 * 开户行地址
	 */
	private String address;
	/**
	 * 支行名称
	 */
	private String branchName;
	/**
	 * 状态
                        0-失效；
                        1-有效
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;
	
	/**
	 * 备注名
	 */
	private String remark;

	/**
	 * 设置：主键
	 */
	public void setUserBankId(Long userBankId) {
		this.userBankId = userBankId;
	}
	/**
	 * 获取：主键
	 */
	public Long getUserBankId() {
		return userBankId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：姓名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：姓名
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：开户行名称
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * 获取：开户行名称
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * 设置：卡号
	 */
	public void setBankCard(String bankCard) {
		this.bankCard = bankCard;
	}
	/**
	 * 获取：卡号
	 */
	public String getBankCard() {
		return bankCard;
	}
	/**
	 * 设置：开户行省
	 */
	public void setPrivince(String privince) {
		this.privince = privince;
	}
	/**
	 * 获取：开户行省
	 */
	public String getPrivince() {
		return privince;
	}
	/**
	 * 设置：开户行市
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：开户行市
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 设置：开户行地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：开户行地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：支行名称
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	/**
	 * 获取：支行名称
	 */
	public String getBranchName() {
		return branchName;
	}
	/**
	 * 设置：状态
                        0-失效；
                        1-有效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-失效；
                        1-有效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
