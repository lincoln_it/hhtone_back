package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseUserWallet;

/**
 * 用户提现钱包
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:42
 */
public class UserWallet extends BaseUserWallet {

	private static final long serialVersionUID = 1L;

	/**
	 * 币种标题
	 */
	private String coinTitle;

	public String getCoinTitle() {
		return coinTitle;
	}

	public void setCoinTitle(String coinTitle) {
		this.coinTitle = coinTitle;
	}
	
}
