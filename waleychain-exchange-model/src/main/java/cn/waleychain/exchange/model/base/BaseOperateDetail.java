package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 相关操作流水
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:24
 */
public class BaseOperateDetail extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "operate_detail";
	
	/**
	 * 主键标识ID
	 */
	private Long idNo;
	/**
	 * 用户标识ID
	 */
	private Long userId;
	/**
	 * 币种ID
	 */
	private Long coinId;
	/**
	 * 账户id
	 */
	private Long unionId;
	/**
	 * 该笔流水资金关联方的账户id
	 */
	private Long refUnionId;
	/**
	 * 订单ID
	 */
	private Long orderId;
	/**
	 * 入账为1，出账为2
	 */
	private Integer detailType;
	/**
	 * 业务类型:
            充值(recharge_into) 
            提现审核通过(withdrawals_out) 
            下单(order_create) 
            成交(order_turnover)
            成交手续费(order_turnover_poundage)  
            撤单(order_cancel)  
            注册奖励(bonus_register)
            提币冻结解冻(withdrawals)
            充人民币(recharge)
            提币手续费(withdrawals_poundage)   
            兑换(cny_btcx_exchange)
            奖励充值(bonus_into)
            奖励冻结(bonus_freeze)
	 */
	private String paymentType;
	/**
	 * 资产数量
	 */
	private BigDecimal paymentAmount;
	/**
	 * 流水状态：
            充值
            提现
            冻结
            解冻
            转出
            转入
	 */
	private String paymentRemark;
	/**
	 * 日期
	 */
	private Date paymentDate;

	/**
	 * 设置：主键标识ID
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键标识ID
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：用户标识ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户标识ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：币种ID
	 */
	public void setCoinId(Long coinId) {
		this.coinId = coinId;
	}
	/**
	 * 获取：币种ID
	 */
	public Long getCoinId() {
		return coinId;
	}
	/**
	 * 设置：账户id
	 */
	public void setUnionId(Long unionId) {
		this.unionId = unionId;
	}
	/**
	 * 获取：账户id
	 */
	public Long getUnionId() {
		return unionId;
	}
	/**
	 * 设置：该笔流水资金关联方的账户id
	 */
	public void setRefUnionId(Long refUnionId) {
		this.refUnionId = refUnionId;
	}
	/**
	 * 获取：该笔流水资金关联方的账户id
	 */
	public Long getRefUnionId() {
		return refUnionId;
	}
	/**
	 * 设置：订单ID
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单ID
	 */
	public Long getOrderId() {
		return orderId;
	}
	/**
	 * 设置：入账为1，出账为2
	 */
	public void setDetailType(Integer detailType) {
		this.detailType = detailType;
	}
	/**
	 * 获取：入账为1，出账为2
	 */
	public Integer getDetailType() {
		return detailType;
	}
	/**
	 * 设置：业务类型:
            充值(recharge_into) 
            提现审核通过(withdrawals_out) 
            下单(order_create) 
            成交(order_turnover)
            成交手续费(order_turnover_poundage)  
            撤单(order_cancel)  
            注册奖励(bonus_register)
            提币冻结解冻(withdrawals)
            充人民币(recharge)
            提币手续费(withdrawals_poundage)   
            兑换(cny_btcx_exchange)
            奖励充值(bonus_into)
            奖励冻结(bonus_freeze)
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * 获取：业务类型:
            充值(recharge_into) 
            提现审核通过(withdrawals_out) 
            下单(order_create) 
            成交(order_turnover)
            成交手续费(order_turnover_poundage)  
            撤单(order_cancel)  
            注册奖励(bonus_register)
            提币冻结解冻(withdrawals)
            充人民币(recharge)
            提币手续费(withdrawals_poundage)   
            兑换(cny_btcx_exchange)
            奖励充值(bonus_into)
            奖励冻结(bonus_freeze)
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * 设置：资产数量
	 */
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	/**
	 * 获取：资产数量
	 */
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}
	/**
	 * 设置：流水状态：
            充值
            提现
            冻结
            解冻
            转出
            转入
	 */
	public void setPaymentRemark(String paymentRemark) {
		this.paymentRemark = paymentRemark;
	}
	/**
	 * 获取：流水状态：
            充值
            提现
            冻结
            解冻
            转出
            转入
	 */
	public String getPaymentRemark() {
		return paymentRemark;
	}
	/**
	 * 设置：日期
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	/**
	 * 获取：日期
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}
}
