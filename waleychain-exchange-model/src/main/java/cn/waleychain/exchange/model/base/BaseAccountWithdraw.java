package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 人民币提现
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:20
 */
public class BaseAccountWithdraw extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "account_withdraw";
	
	/**
	 * 主键
	 */
	private Long idNo;
	/**
	 * 用户账户ID
	 */
	private Long accountId;
	/**
	 * 提现金额
	 */
	private BigDecimal amount;
	/**
	 * 到账金额
	 */
	private BigDecimal amountReal;
	/**
	 * 提现手续费
	 */
	private BigDecimal fee;
	/**
	 * 用户银行卡绑定id
	 */
	private Long userBankId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getIdNo() {
		return idNo;
	}
	/**
	 * 设置：用户账户ID
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	/**
	 * 获取：用户账户ID
	 */
	public Long getAccountId() {
		return accountId;
	}
	/**
	 * 设置：提现金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：提现金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：到账金额
	 */
	public void setAmountReal(BigDecimal amountReal) {
		this.amountReal = amountReal;
	}
	/**
	 * 获取：到账金额
	 */
	public BigDecimal getAmountReal() {
		return amountReal;
	}
	/**
	 * 设置：提现手续费
	 */
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	/**
	 * 获取：提现手续费
	 */
	public BigDecimal getFee() {
		return fee;
	}
	/**
	 * 设置：用户银行卡绑定id
	 */
	public void setUserBankId(Long userBankId) {
		this.userBankId = userBankId;
	}
	/**
	 * 获取：用户银行卡绑定id
	 */
	public Long getUserBankId() {
		return userBankId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
