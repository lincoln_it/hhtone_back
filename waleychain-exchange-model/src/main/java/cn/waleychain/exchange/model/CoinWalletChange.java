package cn.waleychain.exchange.model;

import cn.waleychain.exchange.model.base.BaseCoinWalletChange;

/**
 * 钱包数据流水记录（?该表只保留核心字段，用视图实现统计流水功能）
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-11 17:28:39
 */
public class CoinWalletChange extends BaseCoinWalletChange {

	private static final long serialVersionUID = 1L;

}
