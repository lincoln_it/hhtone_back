package cn.waleychain.exchange.model.base;
import java.util.Date;
import cn.waleychain.exchange.model.BaseModel;
import java.math.BigDecimal;

/**
 * 用户资金账户
 * @author chenx
 * @email chenxun@starsand.cn
 * @date 2018-04-19 11:08:25
 */
public class BaseUserAccount extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 缓存名称
	 */
	public static final String CACHE_NAME = "user_account";
	
	/**
	 * 主键
	 */
	private Long accountId;
	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 可用余额
	 */
	private BigDecimal balance;
	/**
	 * 冻结余额
	 */
	private BigDecimal freeze;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 状态
                        0-锁定
                        1-有效
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date modifiedTime;

	/**
	 * 设置：主键
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	/**
	 * 获取：主键
	 */
	public Long getAccountId() {
		return accountId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：可用余额
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * 获取：可用余额
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * 设置：冻结余额
	 */
	public void setFreeze(BigDecimal freeze) {
		this.freeze = freeze;
	}
	/**
	 * 获取：冻结余额
	 */
	public BigDecimal getFreeze() {
		return freeze;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：状态
                        0-锁定
                        1-有效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态
                        0-锁定
                        1-有效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
}
